﻿using LocalDropshipping.Web.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LocalDropshipping.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAdministrativeAccountsAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                var user = (User)context.HttpContext.Items["CurrentUser"];

                if (user.IsSuperAdmin || user.IsAdmin)
                {
                    if(!user.EmailConfirmed)
                    {
                        context.Result = new RedirectToActionResult("Login", "Admin", new { returnUrl = context.HttpContext.Request.Path });
                        return;
                    }

                    return;
                }
            }
           
            context.Result = new RedirectToActionResult("Login", "Admin", new { returnUrl = context.HttpContext.Request.Path });
        }
    }
}
