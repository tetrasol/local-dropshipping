﻿using LocalDropshipping.Web.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LocalDropshipping.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class OnlySellerWithCompleteProfile : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                var user = (User)context.HttpContext.Items["CurrentUser"];

                if (!user.EmailConfirmed)
                {
                    context.Result = new RedirectToActionResult("VerificationEmailSent", "Seller", new { returnUrl = context.HttpContext.Request.Path});
                    return;
                }

                if (!user.IsProfileCompleted)
                {
                    context.Result = new RedirectToActionResult("ProfileVerification", "Seller", new { returnUrl = context.HttpContext.Request.Path });
                    return;
                }

            }
            else
            {
                context.Result = new RedirectToActionResult("Login", "Seller", new { returnUrl = context.HttpContext.Request.Path });
            }
        }
    }
}
