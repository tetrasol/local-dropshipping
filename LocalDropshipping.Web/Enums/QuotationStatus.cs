﻿namespace LocalDropshipping.Web.Enums
{
    public enum QuotationStatus
    {
        Pending,
        Processed,
        All
    }
}
