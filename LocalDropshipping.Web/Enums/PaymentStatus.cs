﻿namespace LocalDropshipping.Web.Enums
{
    public enum PaymentStatus
    {
        Paid = 0,
        UnPaid = 1,
        Processing = 2
    }
}