﻿using System.ComponentModel;

namespace LocalDropshipping.Web.Enums
{
    public enum OrderStatus
    {
        [Description("Pickup Request Not Send")]
        PickupRequestNotSend,

        [Description("48 Hours Auto Canceled")]
        FortyEightHoursAutoCanceled,

        [Description("Pickup Request Sent")]
        PickupRequestSent,

        [Description("Cancelled")]
        Cancelled,

        [Description("Consignment Booked")]
        ConsignmentBooked,

        [Description("Assign to Courier")]
        AssignToCourier,

        [Description("Arrived at Station")]
        ArrivedAtStation,

        [Description("Returned to Shipper")]
        ReturnedToShipper,

        [Description("Missroute")]
        Missroute,

        [Description("Pending")]
        Pending,

        [Description("Delivered")]
        Delivered,

        [Description("Dispatched")]
        Dispatched,

        [Description("Ready for Return")]
        ReadyForReturn,

        [Description("Being Return")]
        BeingReturn,

        [Description("Shipment Picked")]
        ShipmentPicked,

        [Description("Drop Off at Express Center")]
        DropOffAtExpressCenter,

        [Description("InProcess")]
        InProcess,

        [Description("Shipment - Booked")]
        ShipmentBooked,

        [Description("Shipment - Arrived at Origin")]
        ShipmentArrivedAtOrigin,

        [Description("Shipment - In Transit")]
        ShipmentInTransit,

        [Description("Shipment - Arrived at Destination")]
        ShipmentArrivedAtDestination,

        [Description("Shipment - Out for Delivery")]
        ShipmentOutForDelivery,

        [Description("Shipment - Rider Exchange")]
        ShipmentRiderExchange,

        [Description("Shipment - Not Attempt")]
        ShipmentNotAttempt,

        [Description("Shipment - Delivery Unsuccessfull")]
        ShipmentDeliveryUnsuccessfull,

        [Description("Shipment - On Hold")]
        ShipmentOnHold,

        [Description("Shipment - Delivered")]
        ShipmentDelivered,

        [Description("Shipment - Non - Service Area")]
        ShipmentNonServiceArea,

        [Description("Shipment - Misrouted")]
        ShipmentMisrouted,

        [Description("Shipment - Return Confirmation Pending")]
        ShipmentReturnConfirmationPending,

        [Description("Shipment - Re - Attempt")]
        ShipmentReAttempt,

        [Description("Shipment - Case Closed")]
        ShipmentCaseClosed,

        [Description("Shipment - Re-Attempt Requested")]
        ShipmentReAttemptRequested,

        [Description("Shipment - Rider Picked")]
        ShipmentRiderPicked,

        [Description("Shipment - On Hold for Self Collection")]
        ShipmentOnHoldForSelfCollection,

        [Description("Shipment - Cancelled")]
        ShipmentCancelled,

        [Description("Shipment - Misroute Forwarded")]
        ShipmentMissrouteForwarded,

        [Description("Shipment - Arrival Service Center")]
        ShipmentArrivalServiceCenter,

        [Description("Shipment - Multiple Pieces Hold")]
        ShipmentMultiplePiecesHold,

        [Description("Shipment - Dispatched From Warehouse")]
        ShipmentDispatchedFromWarehouse,

        [Description("Shipment - Received From Shipper")]
        ShipmentReceivedFromShipper,

        [Description("Shipment - Recalled")]
        ShipmentRecalled,

        [Description("Shipment - Lost")]
        ShipmentLost,

        [Description("Shipment - Re - Booked")]
        ShipmentReBooked,

        [Description("Shipment - Waiting For Consolidation")]
        ShipmentWaitingForConsolidation,

        [Description("Shipment - Consolidated")]
        ShipmentConsolidated,

        [Description("Return - Confirm")]
        ReturnConfirm,

        [Description("Return - In Transit")]
        ReturnInTransit,

        [Description("Return - Arrived at Origin")]
        ReturnArrivedAtOrigin,

        [Description("Return - Dispatched")]
        ReturnDispatched,

        [Description("Return - Delivery Unsuccessfull")]
        ReturnDeliveryUnsuccessfull,

        [Description("Return - Delivered to Shipper")]
        ReturnDeliveredToShipper,

        [Description("Return - Not Attempted")]
        ReturnNotAttempted,

        [Description("Return - On Hold")]
        ReturnOnHold,

        [Description("Return - Dispatched")]
        ReturnedDispatched,

        [Description("Return - Unable to Return")]
        ReturnedUnableToReturn,

        [Description("Return - Note Shifted")]
        ReturnNotShifted,

        [Description("Return - Rider Exchange")]
        ReturnRiderExchange,

        [Description("Replacement - In Transit")]
        ReplacementInTransit,

        [Description("Replacement - Arrived at Origin")]
        ReplacementArrivedAtOrigin,

        [Description("Replacement - Dispatched")]
        ReplacementDispatched,

        [Description("Replacement - Delivery Unsuccessfull")]
        ReplacementDeliveryUnsuccessfull,

        [Description("Replacement - Delivered to Shipper")]
        ReplacementDeliveredToShipper,

        [Description("Replacement - Collected")]
        ReplacementCollected,

        [Description("Replacement - Rider Exchange")]
        ReplacementRiderExchange,

        [Description("Replacement - Not Collected")]
        ReplacementNotCollected,

        [Description("Intercept - Requested")]
        InterceptRequested,

        [Description("Intercept - Approved")]
        InterceptApproved,

        [Description("Booked")]
        Booked,

        [Description("Booking Cancelled")]
        BookingCancelled,

        [Description("Dispatached")]
        Dispatached,

        [Description("Dispatached Partially")]
        DispatachedPartially,

        [Description("Inward")]
        Inward,

        [Description("Dispatached for Home Delivery")]
        DispatachedForHomeDelivery,

        [Description("Return Undelivered Home Delievry")]
        ReturnUndeliveredHomeDelivery,

        [Description("Dlivered at Home")]
        DliveredAtHome,

        [Description("Refused Delivery")]
        RefusedDelivery,

        [Description("Return To Shipper")]
        ReturnToShipper,

        [Description("Return to Origin")]
        ReturnToOrigin,

        [Description("Hold For Collection")]
        HoldForCollection,

        [Description("Partial Delivered")]
        PartialDelivered,

        [Description("Lost Cash")]
        LostCash,

        [Description("Lost Transit")]
        LostTransit,

        [Description("Snatched Cash")]
        SnatchedCash,

        [Description("Snatched Shipment")]
        SnatchedShipment,

        [Description("Lost Shipment")]
        LostShipment,

        [Description("Damaged")]
        Damaged,

        [Description("Marked For Disposal")]
        MarkedForDisposal,

        [Description("Disposed Off")]
        DisposedOff,

        [Description("Claim Paid")]
        ClaimPaid,







        [Description("Order Booked")]
        OrderBooked,

        [Description("Rider on its way")]
        RiderOnItsWay,

        [Description("Collected")]
        Collected,

        [Description("QC Rejected-Hub")]
        QCRejectedHub,

        [Description("Awaiting Dispatch")]
        AwaitingDispatch,

        [Description("Rider is out for Delivery")]
        RiderIsOutForDelivery,

        [Description("Awaiting Return")]
        AwaitingReturn,

        [Description("Returned")]
        Returned,

        [Description("Cancelled")]
        CancelledByClient,

        [Description("Delivery Attempt Failed")]
        DeliveryAttemptFailed,

        [Description("Return in Progress")]
        ReturnInProgress,

        [Description("Return Attempt Failed")]
        ReturnAttemptFailed,

        [Description("Delivery in progress")]
        DeliveryInProgress,

        [Description("In Transit")]
        InTransit,

        [Description("Return In Transit")]
        ReturnInTransitRider,

        [Description("Lost")]
        Lost,

        [Description("Awaiting Linehaul Dispatch (Sort)")]
        AwaitingLinehaulDispatch,

        [Description("Transit to Hub")]
        TransitToHub,

        [Description("Transit to Hub Return")]
        TransitToHubReturn,

        [Description("Awaiting Linehaul Return")]
        AwaitingLinehaulReturn,
    }
}


