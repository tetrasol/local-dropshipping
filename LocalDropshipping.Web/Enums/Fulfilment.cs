﻿namespace LocalDropshipping.Web.Enums
{
    public enum Fulfilment
    {
        OWN,
        FOC
    }
}
