﻿using System.ComponentModel;

namespace LocalDropshipping.Web.Enums
{
    public enum CourierServiceType
    {
        [Description("Leopard")]
        Leopard,
        [Description("Trax")]
        Trax,
        [Description("Daewoo")]
        Daewoo,
        [Description("Rider")]
        Rider,
        [Description("Not Selected")]
        NotSelected
    }
}
