﻿using LocalDropshipping.Web.Attributes;
using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Dtos;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Exceptions;
using LocalDropshipping.Web.Extensions;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Helpers.Constants;
using LocalDropshipping.Web.Models;
using LocalDropshipping.Web.Models.Inventory;
using LocalDropshipping.Web.Models.ProductViewModels;
using LocalDropshipping.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace LocalDropshipping.Web.Controllers
{
    public class AdminController : Controller
    {
        public ICategoryService CategoryService { get; }
        private readonly IAdminService _service;
        private readonly IProductsService _productsService;
        private readonly IProductVariantService _productVariantService;
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly LocalDropshippingContext _context;
        private readonly ICategoryService _categoryService;
        private readonly IAccountService _accountService;
        private readonly IOrderService _orderService;
        private readonly IConsumerService _consumerService;
        private readonly IWithdrawalService _withdrawlsService;
        private readonly IProfilesService _profilesService;
        private readonly ICourierService _courierService;
        private readonly IReportService _reportService;
        private readonly IOrderItemService _orderItemService;
        private readonly IWalletService _walletService;
        private readonly IWholesaleService _wholesaleService;
        private readonly IWebHostEnvironment _env;
        private readonly IInventoryService _inventoryService;

        private User _currentUser { get => (User)HttpContext.Items["CurrentUser"]; }
        private bool _isLoggedIn { get => HttpContext.User.Identity.IsAuthenticated; }

        public AdminController(IAdminService service, IProductsService productsService, IProductVariantService productVariantService, IUserService userService, UserManager<User> userManager, SignInManager<User> signInManager, LocalDropshippingContext context, ICategoryService categoryService, IAccountService accountService, IOrderService orderService, IConsumerService consumerService, IWithdrawalService withdrawlsService, IProfilesService profilesService, ICourierService courierService, IReportService reportService, IOrderItemService orderItemService, IWalletService walletService, IWholesaleService wholesaleService, IWebHostEnvironment env, IInventoryService inventoryService)
        {
            _service = service;
            _productsService = productsService;
            this._productVariantService = productVariantService;
            _userService = userService;
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _categoryService = categoryService;
            _orderService = orderService;
            _consumerService = consumerService;
            _withdrawlsService = withdrawlsService;
            _profilesService = profilesService;
            _courierService = courierService;
            _reportService = reportService;
            _orderItemService = orderItemService;
            _walletService = walletService;
            CategoryService = _categoryService;
            _accountService = accountService;
            _orderService = orderService;
            _wholesaleService = wholesaleService;
            _env = env;
            _inventoryService = inventoryService;
        }

        #region Accounts  
        public IActionResult Login()
        {
            if (_isLoggedIn)
            {
                if (_currentUser.IsAdmin || _currentUser.IsSuperAdmin)
                {
                    //if (!_currentUser.EmailConfirmed)
                    //    return RedirectToAction("AdminSideVerificationEmailSent", "Admin");
                    if (!_currentUser.EmailConfirmed)
                    {
                        // Show an error message on the login page
                        TempData[Constants.AdminTempData.NotificationMessage] = "Your email is not confirmed. Please confirm your email to login.";
                        return View(); // Return to the login page
                    }

                    return RedirectToAction("Dashboard", "Admin");
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(AdminLoginViewModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _accountService.AdminLoginAsync(model.Email, model.Password, model.RememberMe);
                    if (user != null)
                    {
                        HttpContext.Session.SetString("CurrentUser", JsonConvert.SerializeObject(user));
                        if (!returnUrl.IsNullOrEmpty())
                        {
                            return LocalRedirect(returnUrl);
                        }
                        if (model.RememberMe)
                        {
                            var rememberMeCookieOptions = new CookieOptions
                            {
                                Expires = DateTime.UtcNow.AddDays(7),
                                IsEssential = true,
                            };
                            Response.Cookies.Append("RememberMeEmail", model.Email, rememberMeCookieOptions);
                            Response.Cookies.Append("RememberMePassword", model.Password, rememberMeCookieOptions);
                        }
                        return RedirectToAction("Dashboard", "Admin");
                    }
                }
                return View(model);
            }
            catch (IdentityException ex)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = ex.Message;
                return View(model);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLogin;
                return RedirectToAction(Constants.AdminActionMethods.ProductionErrorView, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult AdminSideVerificationEmailSent()
        {
            if (!_isLoggedIn && _currentUser.EmailConfirmed)
            {
                return RedirectToAction("Dashboard", "Admin");
            }
            return View();
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult AddNewUser()
        {
            try
            {
                bool isAdmin = _currentUser.IsAdmin;
                bool isSuperAdmin = _currentUser.IsSuperAdmin;
                UserViewModel model = new UserViewModel();
                if (isAdmin)
                {
                    model.IsSeller = true;
                }
                TempData["IsAdmin"] = isAdmin;
                TempData["IsSuperAdmin"] = isSuperAdmin;
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> AddNewUser(UserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User user = new User
                    {
                        Fullname = model.FullName,
                        UserName = model.Email.Split('@')[0],
                        Email = model.Email,
                        PhoneNumber = model.PhoneNumber,
                        IsAdmin = model.IsAdmin,
                        IsSeller = model.IsSeller,
                        IsActive = true,
                        CreatedDate = DateTime.Now
                    };
                    // Use the AccountService to register the user and send the email
                    var result = await _accountService.RegisterAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        TempData[Constants.AdminTempData.NotificationMessage] = "User has been added successfully!";
                        //TempData["RegistrationConfirmation"] = "Registration was successful. You can now log in.";
                        // HttpContext.Session.SetString("AdminSideVerificationEmailSent", "True");
                        return RedirectToAction("AddNewUser", "Admin");
                    }
                    else
                    {
                        foreach (IdentityError error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                SetRoleByCurrentUser();
                return View(model);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToAddUser;
                return RedirectToAction(Constants.AdminActionMethods.AddNewUser, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult StaffMember([FromQuery] Pagination pagination, string searchString, string sortByName, string currentFilter)
        {
            try
            {
                SetRoleByCurrentUser();
                ViewBag.CurrentSort = sortByName;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sortByName) ? "name_asc" : (sortByName == "name_asc" ? "name_desc" : "name_asc");

                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                var staffMembers = _userService.GetAllStaffMember();
                var sellers = _userService.GetAll();
                if (!string.IsNullOrEmpty(searchString))
                {
                    staffMembers = staffMembers.Where(x => x.Fullname.ToLower().Contains(searchString.ToLower())
                    || x.Email.ToLower().Contains(searchString.ToLower())
                    || (x.PhoneNumber != null && x.PhoneNumber.Contains(searchString))).ToList();
                }
                switch (sortByName)
                {
                    case "name_asc":
                        staffMembers = staffMembers.OrderBy(s => s.Fullname).ToList();
                        break;
                    case "name_desc":
                        staffMembers = staffMembers.OrderByDescending(s => s.CreatedDate).ToList();
                        break;

                    default:
                        staffMembers = staffMembers.OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                }
                var count = staffMembers.Count();
                staffMembers = staffMembers.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View(new PageResponse<List<User>>(staffMembers, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult EditUser()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult DeleteUser(string Id)
        {
            try
            {
                _userService.Delete(Id);
                SetRoleByCurrentUser();
                var users = _userService.GetAll();
                var pageResponse = new PageResponse<List<User>>(
                          data: users,
                          pageNumber: 1,
                          pageSize: users.Count,
                          totalCount: users.Count
                      );
                return View("GetAllSellers", pageResponse);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToDeleteUser;
                return RedirectToAction(Constants.AdminActionMethods.GetAllSellers, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult ActivateUser(string userId, [FromQuery] Pagination pagination)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    _userService.ActivateUser(userId);
                    var user = _userService.GetById(userId);
                    TempData["notificationMessage"] = user.IsActive ? $"User {user.Fullname} has been enabled." : $"User {user.Fullname} has been disabled.";
                }
                var sellers = _userService.GetAll();
                SetRoleByCurrentUser();
                var count = sellers.Count();
                sellers = sellers.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).OrderByDescending(x => x.CreatedDate).ToList();
                return View("GetAllSellers", new PageResponse<List<User>>(sellers, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToActivateUser;
                return RedirectToAction(Constants.AdminActionMethods.GetAllSellers, Constants.AdminActionMethods.Admin);
            }
        }


        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult AdminActivate(string userId, [FromQuery] Pagination pagination)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    _userService.ActivateUser(userId);
                    var user = _userService.GetById(userId);
                    TempData["notificationMessage"] = user.IsActive ? $"User {user.Fullname} has been enabled." : $"User {user.Fullname} has been disabled.";
                    Console.WriteLine("Notification message: " + TempData["notificationMessage"]);
                }
                var staffMembers = _userService.GetAllStaffMember();
                SetRoleByCurrentUser();
                var count = staffMembers.Count();
                staffMembers = staffMembers.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).OrderByDescending(x => x.CreatedDate).ToList();
                return View("StaffMember", new PageResponse<List<User>>(staffMembers, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToActivateUser;
                return RedirectToAction(Constants.AdminActionMethods.GetAllSellers, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult IsSubscribe(string userId, [FromQuery] Pagination pagination)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    _userService.IsSubscribe(userId);
                }
                var sellers = _userService.GetAll();
                SetRoleByCurrentUser();
                var count = sellers.Count();
                sellers = sellers.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).OrderByDescending(x => x.CreatedDate).ToList();
                return View("GetAllSellers", new PageResponse<List<User>>(sellers, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToActivateUser;
                return RedirectToAction(Constants.AdminActionMethods.GetAllSellers, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Login", "Admin");
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLogout;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }
        #endregion

        #region Orders
        [AuthorizeAdministrativeAccounts]
        public IActionResult OrdersList([FromQuery] Pagination pagination, string searchString, string sortOrder, string currentFilter, int? currentPage)
        {
            try
            {
                SetRoleByCurrentUser();

                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "name_asc" : (sortOrder == "name_asc" ? "name_desc" : "name_asc");
                ViewBag.PriceSortParm = string.IsNullOrEmpty(sortOrder) ? "price_asc" : (sortOrder == "price_asc" ? "price_desc" : "price_asc");
                ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
                if (currentPage.HasValue)
                {
                    pagination.PageNumber = currentPage.Value;
                }
                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                OrderListModel orders = _orderService.GetListOfOrderWithConsumer();
                if (!string.IsNullOrEmpty(searchString))
                {
                    //if (Enum.TryParse<OrderStatus>(searchString.ApplyCase(LetterCasing.Sentence), out OrderStatus status))
                    //{

                    if (orders.Orders != null)
                    {
                        orders.Orders = orders.Orders.Where(x =>
                            x.OrderStatus.ToString().ToLower().Contains(searchString)
                            || x.Id.ToString().Contains(searchString)
                            || (x.CourierOrderId != null && x.CourierOrderId.Contains(searchString))
                            || (x.OrderTrackingId != null && x.OrderTrackingId.Contains(searchString))).ToList();
                    }
                    //}
                }

                switch (sortOrder)
                {
                    case "name_asc":
                        orders.Orders = orders.Orders.OrderBy(s => s.OrderStatus).ToList();
                        break;
                    case "name_desc":
                        orders.Orders = orders.Orders.OrderByDescending(s => s.OrderStatus).ToList();
                        break;
                    case "Date":
                        orders.Orders = orders.Orders.OrderBy(s => s.CreatedDate).ToList();
                        break;
                    case "date_desc":
                        orders.Orders = orders.Orders.OrderByDescending(s => s.CreatedDate).ToList();
                        break;
                    case "price_asc":
                        orders.Orders = orders.Orders.OrderBy(s => s.GrandTotal).ToList();
                        break;
                    case "price_desc":
                        orders.Orders = orders.Orders.OrderByDescending(s => s.GrandTotal).ToList();
                        break;
                    default:
                        orders.Orders = orders.Orders.OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                }

                var count = orders.Orders.Count();
                orders.Orders = orders.Orders.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();

                ViewBag.CurrentFilter = searchString;
                var pageResponse = new PageResponse<List<OrderListModel>>(new List<OrderListModel> { orders }, pagination.PageNumber, pagination.PageSize, count);

                return View(pageResponse);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }



        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> OrdersList(OrderViewModel orderViewModel, int? currentPage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    orderViewModel.UpdatedBy = _currentUser.Email;
                    var result = await _orderService.UpdateOrder(orderViewModel);
                    return RedirectToAction("OrdersList", new { currentPage });
                }
                else
                {
                    return RedirectToAction("OrdersList");
                }
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.OrderNotFound;
                return RedirectToAction(Constants.AdminActionMethods.OrdersList, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> UpdateOrderStatus(int id)
        {
            Order order = (from p in _context.Orders
                           where p.Id == id
                           select p).SingleOrDefault();
            if (order.CourierServiceType == CourierServiceType.Leopard)
            {
                var result = await _courierService.UpdateLeopardOrderByIdAsync(order.CourierOrderId, id);
            }
            else if (order.CourierServiceType == CourierServiceType.Trax)
            {
                var result = await _courierService.UpdateTraxOrderByIdAsync(order.CourierOrderId, id);
            }
            else if (order.CourierServiceType == CourierServiceType.Daewoo)
            {
                var result = await _courierService.UpdateDaewooOrderByIdAsync(order.CourierOrderId, id);
            }
            else if (order.CourierServiceType == CourierServiceType.Rider)
            {
                var result = await _courierService.UpdateRiderOrderByIdAsync(order.CourierOrderId, id);
            }
            return RedirectToAction("OrdersList");
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult DeleteOrder(int id)
        {
            var result = _orderService.Delete(id);
            return RedirectToAction("OrdersList");
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult ViewOrder(int id)
        {
            try
            {
                var order = _orderService.GetById(id);
                var userId = _userService.GetUserIdByEmail(order.Seller);
                var profile = _profilesService.GetProfileById(userId);
                var consumer = _consumerService.GetConsumerByOrderId(id);
                var orderDetails = new
                {
                    businessName = profile?.StoreName,
                    courierService = order.CourierServiceType == null ? CourierServiceType.NotSelected.GetDescription() : order.CourierServiceType.GetDescription(),
                    sellPrice = order?.SellPrice,
                    shippingFee = (decimal?)250,
                    total = order?.GrandTotal,
                    paymentMethod = "COD",
                    yourCustomerCity = consumer?.City == null ? "<Not Available>" : consumer.City,
                    yourCustomerAddress = consumer?.Address == null ? "<Not Available>" : consumer.Address,
                    orderId = order?.Id,
                    createdDate = order?.CreatedDate.ToString("MMM dd, yyyy"),
                    status = order?.OrderStatus == null ? OrderStatus.Pending.GetDescription() : order.OrderStatus.GetDescription(),
                    remarks = order?.SpecialInstructions == null ? "<Not Available>" : order.SpecialInstructions
                };
                if (orderDetails != null) return Json(orderDetails);
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLoadOrder;
                return RedirectToAction(Constants.AdminActionMethods.OrdersList, Constants.AdminActionMethods.Admin);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLoadOrder;
                return RedirectToAction(Constants.AdminActionMethods.OrdersList, Constants.AdminActionMethods.Admin);
            }
        }


        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public IActionResult ExportToCsv(string daterange)
        {
            var dateRangeParts = daterange.Split(" - ");
            var startDate = DateTime.Parse(dateRangeParts[0]);
            var endDate = DateTime.Parse(dateRangeParts[1]);
            var orders = _orderService.GetOrderByDateRange(startDate, endDate);

            if (orders.Count() <= 0)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.NoOrdersFound;
                return RedirectToAction(Constants.AdminActionMethods.OrdersList, Constants.AdminActionMethods.Admin);
            }

            var header = "Order #,Consumer Name,Consumer Address,Contact 1,Contact 2,Product Name,Quantity,Selling Price,Store Name,Remarks\n";
            var stream = new MemoryStream();
            var sw = new StreamWriter(stream);
            sw.Write(header);

            foreach (var order in orders)
            {
                var consumer = _consumerService.GetConsumerByOrderId(order.Id);
                var user = _userService.GetUserIdByEmail(order.Seller);
                var profiles = _profilesService.GetProfileById(user);
                var orderItems = _orderItemService.GetOrderItemsById(order.Id);
                var remarks = order.SpecialInstructions;
                if (consumer != null && profiles != null)
                {
                    var consumerName = consumer.FullName;
                    var consumerAddress = consumer.Address;
                    var contact1 = consumer.PrimaryPhoneNumber;
                    var contact2 = consumer.SecondaryPhoneNumber;
                    var storeName = profiles.StoreName;

                    foreach (var orderItem in orderItems)
                    {
                        var productName = orderItem.Name;
                        var quantity = orderItem.Quantity;
                        var sellingPrice = order.SellPrice;
                        sw.Write($"{order.Id},\"{consumerName}\",\"{consumerAddress}\",\"{contact1}\",\"{contact2}\",\"{productName}\",{quantity},{sellingPrice},\"{storeName}\",\"{remarks}\"\n");
                    }
                }
            }

            sw.Flush();
            var fileName = $"FOCFulfillment-Orders-{DateTime.Today.ToString("dd/MM/yyyy")}.csv";
            Response.Headers.Add("Content-Type", Constants.ContentTypeFiles.TextCsv);
            Response.Headers.Add("Content-Disposition", $"attachment; filename={fileName}");
            return File(stream.ToArray(), Constants.ContentTypeFiles.TextCsv);
        }

        [AuthorizeSuperAdminOnly]
        public async Task<IActionResult> UpdateOrderStatuses()
        {
            var ordersList = await _orderService.GetUnpaidAndProcessedOrder();
            if (ordersList != null && ordersList.Count > 0)
            {
                foreach (var order in ordersList)
                {
                    if (order.CourierServiceType == CourierServiceType.Leopard)
                    {
                        var result = await _courierService.UpdateLeopardOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                    else if (order.CourierServiceType == CourierServiceType.Trax)
                    {
                        var result = await _courierService.UpdateTraxOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                    else if (order.CourierServiceType == CourierServiceType.Leopard)
                    {
                        var result = await _courierService.UpdateDaewooOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                    else if (order.CourierServiceType == CourierServiceType.Rider)
                    {
                        var result = await _courierService.UpdateRiderOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                }
            }
            TempData[Constants.AdminTempData.NotificationMessage] = Constants.NotificationSuccessMessage.OrdersUpdateSuccessfully;
            return RedirectToAction(Constants.AdminActionMethods.OrdersList, Constants.AdminActionMethods.Admin);
        }
        #endregion

        #region Withdrawal
        [AuthorizeAdministrativeAccounts]
        public IActionResult Withdrawal([FromQuery] Pagination pagination, string searchString, string sorting, string currentFilter, int? currentPage)
        {
            try
            {
                ViewBag.CurrentSort = sorting;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sorting) ? "name_asc" : (sorting == "name_asc" ? "name_desc" : "name_asc");
                ViewBag.DateSortParm = sorting == "Date" ? "date_desc" : "Date";
                var withdrawals = new List<Withdrawals>();
                if (currentPage.HasValue)
                {
                    pagination.PageNumber = currentPage.Value;
                }
                if (pagination.PaymentStatus != "All" || (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue))
                {
                    withdrawals = _withdrawlsService.GetFilteredWithdrawals(pagination);
                }
                else
                {
                    withdrawals = _withdrawlsService.GetAll();
                }
                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                var profiles = _profilesService.GetAllProfiles();
                var combinedData = getProfileAndWithdrawalData(withdrawals, profiles);
                combinedData = combinedData.OrderByDescending(x => x.CreatedDate).ToList();
                if (!string.IsNullOrEmpty(searchString))
                {
                    combinedData = combinedData.Where(x => x.UserEmail.ToLower().Contains(searchString.ToLower())).ToList();
                }
                switch (sorting)
                {
                    case "name_asc":
                        combinedData = combinedData.OrderBy(s => s.UserEmail).ToList();
                        break;
                    case "name_desc":
                        combinedData = combinedData.OrderByDescending(s => s.UserEmail).ToList();
                        break;
                    case "Date":
                        combinedData = combinedData.OrderBy(s => s.CreatedDate).ToList();
                        break;
                    case "date_desc":
                        combinedData = combinedData.OrderByDescending(s => s.CreatedDate).ToList();
                        break;

                    default:
                        combinedData = combinedData.OrderByDescending(s => s.CreatedDate).ToList();
                        break;
                }
                var count = combinedData.Count();
                combinedData = combinedData.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                var pageResponse = new PageResponse<List<AddWithdrawalUserViewModel>>(combinedData.ToList(), pagination.PageNumber, pagination.PageSize, count)
                {
                    Succeeded = true,
                    Message = "Data retrieved successfully",
                };
                return View("Withdrawal", pageResponse);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult Withdrawal(PaymentViewModel model, int? currentPage)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    model.UpdatedBy = _currentUser.Email;
                    model.ProcessedBy = _currentUser.Email;
                    var result = _withdrawlsService.UpdateWithdrawal(model);
                    if (result != null) return RedirectToAction("Withdrawal");
                    return RedirectToAction("Withdrawal", new { currentPage });
                    //return RedirectToAction("Withdrawal");
                }
                else
                {
                    return View("Withdrawal");
                }
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedTowithdrawal;
                return RedirectToAction(Constants.AdminActionMethods.Withdrawal, Constants.AdminActionMethods.Admin);
            }
        }


        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> ExportToCsvWithdrawal(string daterange)
        {
            var dateRangeParts = daterange.Split(" - ");
            var startDate = DateTime.Parse(dateRangeParts[0]);
            var endDate = DateTime.Parse(dateRangeParts[1]);
            var withdrawals = await _withdrawlsService.GetWithdrawalsByDateRange(startDate, endDate);
            var profiles = _profilesService.GetAllProfiles();
            var combineData = getProfileAndWithdrawalData(withdrawals, profiles);

            if (combineData.Count() <= 0)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.NoOrdersFound;
                return RedirectToAction(Constants.AdminActionMethods.Withdrawal, Constants.AdminActionMethods.Admin);
            }

            var header = "Vendor/Seller,Account Title,Account Number,Bank Name,Created Date,Processed By,Payment Status,Amount In Pkr\n";
            var stream = new MemoryStream();
            var sw = new StreamWriter(stream);
            sw.Write(header);

            foreach (var withdrawal in combineData)
            {
                var UserEmail = withdrawal.UserEmail;
                var paymentStatus = withdrawal.paymentStatus;
                var ProcessedBy = withdrawal.ProcessedBy;
                var CreatedDate = withdrawal.CreatedDate.ToString("dd/MM/yyyy");
                var AccountTitle = withdrawal.AccountTitle;
                var BankAccountNumberOrIBAN = withdrawal.BankAccountNumberOrIBAN;
                var BankName = withdrawal.BankName;
                var sellingPrice = withdrawal.AmountInPkr;
                sw.Write($"{UserEmail},\"{AccountTitle}\",\"{BankAccountNumberOrIBAN}\",\"{BankName}\",\"{CreatedDate}\",\"{ProcessedBy}\",{paymentStatus},{sellingPrice}\n");
            }
            sw.Flush();
            var fileName = $"FOCFulfillment-Withdrawals-{DateTime.Today.ToString("dd/MM/yyyy")}.csv";
            Response.Headers.Add("Content-Type", Constants.ContentTypeFiles.TextCsv);
            Response.Headers.Add("Content-Disposition", $"attachment; filename={fileName}");
            return File(stream.ToArray(), Constants.ContentTypeFiles.TextCsv);
        }


        #endregion

        #region Product
        [AuthorizeAdministrativeAccounts]
        public IActionResult Products([FromQuery] Pagination pagination, string searchString, string sortProduct, string currentFilter)
        {
            try
            {
                var user = HttpContext.Session.Get<User>("CurrentUser");
                ViewBag.CurrentSort = sortProduct;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sortProduct) ? "name_asc" : (sortProduct == "name_asc" ? "name_desc" : "name_asc");
                ViewBag.PriceSortParm = string.IsNullOrEmpty(sortProduct) ? "price_asc" : (sortProduct == "price_asc" ? "price_desc" : "price_asc");
                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                List<Product> data = _productsService.GetAll();
                if (!string.IsNullOrEmpty(searchString))
                {
                    data = data.Where(x => x.Name.ToLower().Contains(searchString.ToLower()) ||
                    x.Description.ToLower().Contains(searchString.ToLower()) ||
                     x.Category.Name.ToLower().Contains(searchString.ToLower())).ToList();
                }
                switch (sortProduct)
                {
                    case "name_asc":
                        data = data.OrderBy(s => s.Name).ToList();
                        break;
                    case "name_desc":
                        data = data.OrderByDescending(s => s.Name).ToList();
                        break;
                    case "price_asc":
                        data = data.OrderBy(s => s.Variants.FirstOrDefault().VariantPrice).ToList();
                        break;
                    case "price_desc":
                        data = data.OrderByDescending(s => s.Variants.FirstOrDefault().VariantPrice).ToList();
                        break;
                    default:
                        data = data.OrderBy(s => s.ProductId).ToList();
                        break;
                }
                data = data.OrderByDescending(x => x.CreatedDate).ToList();
                var count = data.Count();
                data = data.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View(new PageResponse<List<Product>>(data, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult AddUpdateProduct(int id = 0)
        {
            try
            {
                SetRoleByCurrentUser();
                ViewBag.Category = _categoryService.GetDeafultCategory();
                var productVeiwModel = new ProductViewModel(_productsService.GetById(id));
                return View(productVeiwModel);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult DeleteProduct(int id)
        {
            try
            {
                SetRoleByCurrentUser();
                Product product = _productsService.Delete(id, _currentUser);
                TempData["Message"] = "Product deleted successfully.";
                return RedirectToAction("Products");
            }
            catch (Exception e)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult Product(int id)
        {
            try
            {
                Product? product = _productsService.GetById(id);
                //List<Product> bestSellingProducts = _productsService.GetAll()
                //                                                    .Where(x => x.IsBestSelling)
                //                                                    .Take(5).ToList();
                //List<Product> topRatedProducts = _productsService.GetAll()
                //                                                    .Where(x => x.IsTopRated && !bestSellingProducts.Contains(x))
                //                                                    .Take(5).ToList();
                if (product != null)
                {
                    //return View(new ProductPageViewModel { Product = product, BestSellingProducts = bestSellingProducts, TopRatedProducts = topRatedProducts });
                    return View(new ProductPageViewModel { Product = product });
                }
                TempData["Message"] = "Product does not exist.";
                return RedirectToAction("Products");
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult AddUpdateProduct(ProductViewModel model)
        {
            try
            {
                SetRoleByCurrentUser();
                if (ModelState.IsValid)
                {
                    ModelState.Remove("ProductId");
                    var form = Request.Form;
                    var formFiles = form.Files;
                    if (model.ProductId == 0)
                    {
                        if (model.HasVariants == 0)
                        {
                            //Product without variants
                            var featuredImage = formFiles["featuredImage"]!;
                            var featureImageLink = featuredImage.SaveTo("images/products", model.Name!);

                            var otherImages = formFiles.GetFiles("otherImages")!.ToArray();
                            var uniqueIdentifier = DateTime.Now.Ticks;

                            var otherImagesLinks = otherImages.Select((image, index) =>
                            {
                                var uniqueFileName = $"{model.Name}_thumbnail_{index}_{uniqueIdentifier}";

                                return image.SaveTo("images/products", uniqueFileName);
                            }).ToArray();
                            var productVideos = formFiles.GetFiles("productVideos")!.ToArray();
                            var videoLinks = productVideos.SaveTo("videos/products", model.Name);
                            var product = new Product()
                            {
                                Name = model.Name,
                                CategoryId = model.CategoryId,
                                IsBestSelling = model.IsBestSelling,
                                IsTopRated = model.IsTopRated,
                                IsNewArravial = model.IsNewArravial,
                                Description = model.Description,
                                DescriptionContents = model.DescriptionContents,
                                ShortDescription = model.ShortDescription,
                                ShortDescriptionContents = model.ShortDescriptionContents,
                                Weight = model.Weight,
                                Sourcing = model.Sourcing,
                                SKU = model.SKU,
                                Variants = new List<ProductVariant>
                        {
                            new ProductVariant
                            {
                                IsMainVariant = true,
                                VariantPrice = model.Price,
                                FeatureImageLink = featureImageLink,
                                DiscountedPrice = model.DiscountedPrice,
                                Images = otherImagesLinks.Select(imageLink => new ProductVariantImage { Link = imageLink }).ToList(),
                                Videos = videoLinks.Select(videoLink => new ProductVariantVideo { Link = videoLink }).ToList(),
                                Quantity = model.Quantity
                            }
                        },
                            };
                            _productsService.Add(product, _currentUser);
                            TempData["ProductAdded"] = "Product added successfully";
                            return RedirectToAction("Products");
                        }
                        else
                        {
                            // Product with variants
                            var product = new Product()
                            {
                                Name = model.Name,
                                CategoryId = model.CategoryId,
                                IsBestSelling = model.IsBestSelling,
                                IsTopRated = model.IsTopRated,
                                IsNewArravial = model.IsNewArravial,
                                Description = model.Description,
                                DescriptionContents = model.DescriptionContents,
                                ShortDescription = model.ShortDescription,
                                ShortDescriptionContents = model.ShortDescriptionContents,
                                Weight = model.Weight,
                                Sourcing = model.Sourcing,
                                SKU = model.SKU,
                                Variants = new List<ProductVariant>(),
                            };
                            for (int variantNo = 1; variantNo <= model.VariantCounts; variantNo++)
                            {
                                var uniqueIdentifier = DateTime.Now.Ticks;

                                product.Variants.Add(new ProductVariant
                                {
                                    VariantType = form["variant-type"],
                                    Variant = form["variant-" + variantNo + "-value"],
                                    VariantPrice = Convert.ToInt32(form["variant-" + variantNo + "-price"]),
                                    DiscountedPrice = Convert.ToInt32(form["variant-" + variantNo + "-discounted-price"]),
                                    FeatureImageLink = formFiles["variant-" + variantNo + "-feature-image"]!.SaveTo("images/products", $"{model.Name}_{form["variant-type"]}_{uniqueIdentifier}"),
                                    Images = formFiles.GetFiles("variant-" + variantNo + "-images")
                                        .Select((image, index) =>
                                        {
                                            var uniqueFileName = $"{model.Name}_{form["variant-type"]}_Image{index}_{uniqueIdentifier}";
                                            return image.SaveTo("images/products", uniqueFileName);
                                        })
                                        .Select(x => new ProductVariantImage { Link = x })
                                        .ToList(),
                                    Videos = formFiles.GetFiles("variant-" + variantNo + "-videos").ToArray().SaveTo("videos/products", $"{model.Name}_{form["variant-type"]}_{uniqueIdentifier}")
                                        .Select(x => new ProductVariantVideo { Link = x })
                                        .ToList(),
                                    Quantity = Convert.ToInt32(form["variant-" + variantNo + "-quantity"]),
                                    IsMainVariant = false
                                });
                            }
                            _productsService.Add(product, _currentUser);
                            TempData["ProductAdded"] = "Product added successfully";
                            return RedirectToAction("Products");
                        }
                    }
                    else
                    {
                        var product = new Product();
                        product.Name = model.Name;
                        product.CategoryId = model.CategoryId;
                        product.IsBestSelling = model.IsBestSelling;
                        product.IsTopRated = model.IsTopRated;
                        product.IsNewArravial = model.IsNewArravial;
                        product.Description = model.Description;
                        product.DescriptionContents = model.DescriptionContents;
                        product.ShortDescription = model.ShortDescription;
                        product.ShortDescriptionContents = model.ShortDescriptionContents;
                        product.Weight = model.Weight;
                        product.Sourcing = model.Sourcing;
                        product.SKU = model.SKU;
                        product.Variants = new List<ProductVariant>();
                        if (model.HasVariants == 1)
                        {
                            for (int variantNo = 1; variantNo <= model.VariantCounts; variantNo++)
                            {

                                int variantId = Convert.ToInt32(form["variant-" + variantNo + "-variant-id"]);
                                var newImagesUploaded = form.Files.Any(x => x.Name == $"variant-{variantNo}-updated-images");
                                var newVideosUploaded = form.Files.Any(x => x.Name == $"variant-{variantNo}-updated-videos");
                                var newFeaturedImage = form.Files.Any(x => x.Name == $"variant-{variantNo}-updated-image");

                                product.Variants.Add(new ProductVariant
                                {
                                    ProductVariantId = variantId,
                                    VariantType = form["variant-type"],
                                    Variant = form["variant-" + variantNo + "-value"],
                                    DiscountedPrice = Convert.ToInt32(form["variant-" + variantNo + "-discounted-price"]),
                                    VariantPrice = Convert.ToInt32(form["variant-" + variantNo + "-price"]),
                                    Quantity = Convert.ToInt32(form["variant-" + variantNo + "-quantity"]),
                                    IsMainVariant = false,
                                    Images = newImagesUploaded ? formFiles.GetFiles($"variant-{variantNo}-updated-images").ToArray().SaveTo("images/products", model.Name + " " + form["variant-type"]).Select(x => new ProductVariantImage { Link = x }).ToList() : new List<ProductVariantImage>(),
                                    Videos = newVideosUploaded ? formFiles.GetFiles($"variant-{variantNo}-updated-videos").ToArray()!.SaveTo("videos/products", model.Name + " " + form["variant-type"]).Select(x => new ProductVariantVideo { Link = x }).ToList() : new List<ProductVariantVideo>(),
                                    FeatureImageLink = newFeaturedImage ? formFiles[$"variant-{variantNo}-updated-image"]!.SaveTo("images/products", model.Name + " " + form["variant-type"]) : "",
                                });
                            }
                            _productsService.Update(model.ProductId, product, _currentUser, false);
                            TempData["updated"] = "Product updated successfully";
                        }
                        else
                        {
                            var newImagesUploaded = form.Files.Any(x => x.Name == $"main-variant-updated-images");
                            var newVideosUploaded = form.Files.Any(x => x.Name == $"main-variant-updated-videos");
                            var newFeaturedImage = form.Files.Any(x => x.Name == $"main-variant-updated-image");
                            product.Variants.Add(new ProductVariant
                            {
                                ProductVariantId = model.MainVariantId,
                                IsMainVariant = true,
                                VariantPrice = model.Price,
                                DiscountedPrice = model.DiscountedPrice,
                                Quantity = model.Quantity,
                                Images = newImagesUploaded ? formFiles.GetFiles($"main-variant-updated-images").ToArray().SaveTo("images/products", model.Name).Select(x => new ProductVariantImage { Link = x }).ToList() : new List<ProductVariantImage>(),
                                Videos = newVideosUploaded ? formFiles.GetFiles($"main-variant-updated-videos").ToArray()!.SaveTo("videos/products", model.Name).Select(x => new ProductVariantVideo { Link = x }).ToList() : new List<ProductVariantVideo>(),
                                FeatureImageLink = newFeaturedImage ? formFiles[$"main-variant-updated-image"]!.SaveTo("images/products", model.Name + " " + form["variant-type"]) : "",
                            });
                            _productsService.Update(model.ProductId, product, _currentUser);
                            TempData["updated"] = "Product updated successfully";
                        }
                        return RedirectToAction("Products");
                    }
                }
                var category = new Category
                {
                    CategoryId = 0,
                    Name = "Selectcategory"
                };
                ViewBag.Category = category;
                ModelState.AddModelError("Quantity", "Product Quantity is required.");
                return View(model);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToAddProduct;
                return RedirectToAction(Constants.AdminActionMethods.AddUpdateProduct, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpGet]
        public IActionResult UpdateNews()
        {
            SetRoleByCurrentUser();

            var allSettings = _context.FocSettings.FirstOrDefault(setting => setting.Name == "news");

            if (allSettings != null)
            {
                var viewModel = new FocSettingViewModel
                {
                    value = allSettings.Value
                };

                return View("UpdateNews", viewModel);
            }
            return View("UpdateNews", allSettings);
        }

        [HttpPost]
        public IActionResult UpdateNews(FocSettingViewModel viewModel)
        {
            SetRoleByCurrentUser();
            if (ModelState.IsValid)
            {
                var newsSetting = _context.FocSettings.SingleOrDefault(setting => setting.Name == viewModel.name);

                if (newsSetting != null)
                {
                    newsSetting.Value = viewModel.value;

                    _context.FocSettings.Update(newsSetting);
                    _context.SaveChanges();



                    return RedirectToAction("Dashboard");
                }
                else
                {
                    return RedirectToAction("NewsNotFound");
                }
            }

            return View(viewModel);
        }

        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> QuotationList([FromQuery] Pagination pagination, string searchString, string sortOrder, string currentFilter)
        {
            try
            {
                List<Quotations> quotationList = new List<Quotations>();
                if (pagination.QuotationStatus != "All")
                {
                    quotationList = _wholesaleService.GetFilteredWholesales(pagination);
                }
                else
                {
                    quotationList = await _wholesaleService.GetAllWholesales();
                }
                var quotationViewModelList = new List<QuotationListModel>();
                foreach (var quotation in quotationList)
                {
                    var productImages = await _context.QuotationsProductImages
                        .Where(p => p.QuotationId == quotation.QuotationId)
                        .ToListAsync();

                    var wholesaleViewModel = new QuotationListModel
                    {
                        Fullname = quotation.Fullname,
                        BudgetInUSD = quotation.BudgetInUSD,
                        SlipImage = quotation.SlipImage,
                        PhoneNumber = quotation.PhoneNumber,
                        ProductInformation = quotation.ProductInformation,
                        Email = quotation.Email,
                        WebsiteURL = quotation.WebsiteURL,
                        HomeCity = quotation.HomeCity,
                        QuotationId = quotation.QuotationId,
                        WhatsappPhoneNumber = quotation.WhatsappPhoneNumber,
                        ProductImages = productImages,
                        QuotationStatus = quotation.QuotationStatus,
                        Quantity = quotation.Quantity,
                        Role = quotation.Role

                    };
                    quotationViewModelList.Add(wholesaleViewModel);
                    quotationViewModelList = quotationViewModelList.OrderByDescending(q => q.CreatedDate).ToList();

                }
                var count = quotationViewModelList.Count();
                quotationViewModelList = quotationViewModelList.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                var pageResponse = new PageResponse<List<QuotationListModel>>(
                    quotationViewModelList, pagination.PageNumber, pagination.PageSize, count
                );
                ViewBag.SelectedFilter = pagination.QuotationStatus;
                return View(pageResponse);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }

        }

        [HttpPost]
        public async Task<IActionResult> UpdateWholesaleStatus(int quotationId, string status)
        {
            try
            {
                var result = await _wholesaleService.UpdateStatus(quotationId, status);
                if (result)
                {
                    TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.UpdateWholesale;
                    return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
                }
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToUpdateWholesale;
                return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToUpdateWholesale;
                return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
            }
        }


        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public IActionResult ExportToCsvWholeales(string daterange)
        {
            var dateRangeParts = daterange.Split(" - ");
            var startDate = DateTime.Parse(dateRangeParts[0]);
            var endDate = DateTime.Parse(dateRangeParts[1]);
            var wholesales = _wholesaleService.GetWholesaleByDaterange(startDate, endDate);

            if (wholesales.Count() <= 0)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.NoWholesaleFound;
                return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
            }

            var header = "Fullname,Email,PhoneNumber,Whatsapp PhoneNumber,Home City,Product Information,BudgetInPKR,Store Name,Sourcing,Fulfilment,Role,QuotationStatus\n";
            var stream = new MemoryStream();
            var sw = new StreamWriter(stream);
            sw.Write(header);
            foreach (var wholesale in wholesales)
            {
                sw.Write($"{wholesale.Fullname},\"{wholesale.Email}\",\"{wholesale.PhoneNumber}\",\"{wholesale.WhatsappPhoneNumber}\",\"{wholesale.HomeCity}\",{wholesale.ProductInformation},{wholesale.BudgetInUSD},\"{wholesale.WebsiteURL}\",\"{wholesale.Sourcing}\",\"{wholesale.Fulfilment}\",\"{wholesale.Role}\",\"{wholesale.QuotationStatus}\"\n");
            }

            sw.Flush();
            var fileName = $"FOCFulfillment-Quotations-{DateTime.Today.ToString("dd/MM/yyyy")}.csv";
            Response.Headers.Add("Content-Type", Constants.ContentTypeFiles.TextCsv);
            Response.Headers.Add("Content-Disposition", $"attachment; filename={fileName}");
            return File(stream.ToArray(), Constants.ContentTypeFiles.TextCsv);
        }

        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> WholesaleDetail(int quotationId)
        {
            var quotation = await _wholesaleService.GetById(quotationId);
            return PartialView("_WholesaleViewModel", quotation);
        }

        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> ShowProductImages(int QuotationId)
        {
            var productImages = await _context.QuotationsProductImages.Where(x => x.QuotationId == QuotationId).ToListAsync();
            return PartialView("_WholesaleProductImagesPartial", productImages);
        }

        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> DeleteWholesale(int QuotationId)
        {
            var imagesToDelete = _context.QuotationsProductImages.Where(x => x.QuotationId == QuotationId).ToList();
            var wholesale = await _wholesaleService.GetById(QuotationId);
            string slipImage = Path.Combine(_env.WebRootPath, wholesale.SlipImage);
            if (System.IO.File.Exists(slipImage))
            {
                System.IO.File.Delete(slipImage);
            }
            foreach (var image in imagesToDelete)
            {
                string imagePath = Path.Combine(_env.WebRootPath, image.Link);
                if (System.IO.File.Exists(imagePath))
                {
                    System.IO.File.Delete(imagePath);
                }
            }
            var delete = await _context.QuotationsProductImages.Where(x => x.QuotationId == QuotationId).ExecuteDeleteAsync();
            var deleteWholesale = await _wholesaleService.Delete(QuotationId);
            return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult DownloadFile(string filePath)
        {
            try
            {
                var fileName = Path.GetFileName(filePath);
                var file = Path.Combine($"{_env.WebRootPath}\\{filePath}");
                if (System.IO.File.Exists(file))
                {
                    var fileBytes = System.IO.File.ReadAllBytes(file);
                    var contentType = GetContentType(fileName);

                    return File(fileBytes, contentType, fileName);
                }
                else
                {
                    return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
                }
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToDownloadSlip;
                return RedirectToAction(Constants.AdminActionMethods.QuotationList, Constants.AdminActionMethods.Admin);
            }
        }



        public async Task<PartialViewResult> ProductVariantImageSliderAsync(int id)
        {
            var variant = await _productVariantService.GetByIdAsync(id);
            return PartialView("_ProductVariantImageSliderPartial", variant);
        }

        public async Task<PartialViewResult> ProductVariantVideoSliderAsync(int id)
        {
            var variant = await _productVariantService.GetByIdAsync(id);
            return PartialView("_ProductVariantVideoSliderPartial", variant);
        }
        #endregion

        #region CategoryCRUD
        [AuthorizeAdministrativeAccounts]
        public IActionResult CategoryList([FromQuery] Pagination pagination, string searchString, string sortByName, string currentFilter)
        {
            try
            {
                ViewBag.CurrentSort = sortByName;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sortByName) ? "name_asc" : (sortByName == "name_asc" ? "name_desc" : "name_asc");
                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                List<Category> category = _categoryService.GetAll();
                if (!string.IsNullOrEmpty(searchString))
                {
                    category = category.Where(c => c.Name.ToLower().Contains(searchString.ToLower())).ToList();
                }
                switch (sortByName)
                {
                    case "name_asc":
                        category = category.OrderBy(s => s.Name).ToList();
                        break;
                    case "name_desc":
                        category = category.OrderByDescending(s => s.Name).ToList();
                        break;

                    default:
                        category = category.OrderBy(s => s.CategoryId).ToList();
                        break;
                }
                var count = category.Count();
                category = category.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View(new PageResponse<List<Category>>(category, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.OrderNotFound;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult AddNewCategory()
        {
            try
            {
                SetRoleByCurrentUser();
                return View();
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }

        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult AddNewCategory(Category categoryModel)
        {
            try
            {
                SetRoleByCurrentUser();

                if (ModelState.IsValid)
                {
                    var createdBy = _currentUser.Email;
                    var category = new Category
                    {
                        Name = categoryModel.Name,
                        ImagePath = categoryModel.ImagePath,
                        CreatedDate = DateTime.Now,
                        CreatedBy = createdBy,
                        ModifiedDate = DateTime.Today,
                        ModifiedBy = createdBy,
                        IsActive = true,
                        IsDeleted = false
                    };
                    _categoryService.Add(category);
                    RedirectToAction("CategoryList", "Admin");
                }
                return View();
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToAddCategory;
                return RedirectToAction(Constants.AdminActionMethods.AddNewCategory, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult DeleteCategory(int id)
        {
            try
            {
                SetRoleByCurrentUser();
                var user = _categoryService.Delete(id);
                SetRoleByCurrentUser();
                return View("CategoryList", _categoryService.GetAll());
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToDeleteCategory;
                return RedirectToAction(Constants.AdminActionMethods.AddNewCategory, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult UpdateCategory()
        {
            try
            {
                SetRoleByCurrentUser();
                return View();
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }

        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult UpdateCategory(int categoryId, CategoryDto categoryDto)
        {
            try
            {
                SetRoleByCurrentUser();
                if (ModelState.IsValid)
                {
                    var createdBy = _currentUser.Email;
                    var category = new CategoryDto
                    {
                        Name = categoryDto.Name,
                        ImagePath = categoryDto.ImagePath,
                        CreatedDate = DateTime.Now,
                        CreatedBy = createdBy,
                        ModifiedDate = DateTime.Today,
                        ModifiedBy = createdBy,
                        IsActive = true,
                        IsDeleted = false
                    };
                    if (category != null)
                    {
                        _categoryService.Update(categoryId, categoryDto);
                    }

                    RedirectToAction("CategoryList", _categoryService.GetAll());
                }
                return View();
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToUpdateCategory;
                return RedirectToAction(Constants.AdminActionMethods.UpdateCategory, Constants.AdminActionMethods.Admin);
            }
        }
        #endregion

        #region Misc
        [AuthorizeAdministrativeAccounts]
        public IActionResult GetAllSellers([FromQuery] Pagination pagination, string searchString, string sortByName, string currentFilter)
        {
            try
            {
                SetRoleByCurrentUser();
                ViewBag.CurrentSort = sortByName;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sortByName) ? "name_asc" : (sortByName == "name_asc" ? "name_desc" : "name_asc");
                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                var sellers = _userService.GetAll();
                if (!string.IsNullOrEmpty(searchString))
                {
                    sellers = sellers.Where(x => x.Fullname.ToLower().Contains(searchString.ToLower())
                    || x.Email.ToLower().Contains(searchString.ToLower())
                    || (x.PhoneNumber != null && x.PhoneNumber.Contains(searchString))).ToList();
                }
                switch (sortByName)
                {
                    case "name_asc":
                        sellers = sellers.OrderBy(s => s.Fullname).ToList();
                        break;
                    case "name_desc":
                        sellers = sellers.OrderByDescending(s => s.CreatedDate).ToList();
                        break;

                    default:
                        sellers = sellers.OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                }
                var count = sellers.Count();
                sellers = sellers.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View(new PageResponse<List<User>>(sellers, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> Dashboard()
        {
            AdminDashboardDto adminDashboardDto = new AdminDashboardDto();
            try
            {
                SetRoleByCurrentUser();
                adminDashboardDto = await calculateAdminDashboardCounters();
                string? currentUserID = _userManager.GetUserId(HttpContext.User);
                var currentUser = _userService.GetById(currentUserID);
                HttpContext.Session.SetString("CurrentUser", JsonConvert.SerializeObject(currentUser));
                return View(adminDashboardDto);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.AdminLogin, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult GetAllConsumers([FromQuery] Pagination pagination, string searchString, string sortByName, string currentFilter)
        {
            try
            {
                SetRoleByCurrentUser();
                ViewBag.CurrentSort = sortByName;
                ViewBag.NameSortParm = string.IsNullOrEmpty(sortByName) ? "name_asc" : (sortByName == "name_asc" ? "name_desc" : "name_asc");
                if (searchString != null)
                {
                    pagination.PageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                var consumers = _consumerService.GetAllConsumer();
                if (!string.IsNullOrEmpty(searchString))
                {
                    consumers = consumers.Where(c => c.FullName.ToLower().Contains(searchString.ToLower())
                    || c.PrimaryPhoneNumber.Contains(searchString)
                    || (c.SecondaryPhoneNumber != null && c.SecondaryPhoneNumber.Contains(searchString))).ToList();
                }
                switch (sortByName)
                {
                    case "name_asc":
                        consumers = consumers.OrderBy(s => s.FullName).ToList();
                        break;
                    case "name_desc":
                        consumers = consumers.OrderByDescending(s => s.FullName).ToList();
                        break;

                    default:
                        consumers = consumers.OrderByDescending(x => x.Id).ToList();
                        break;
                }
                var count = consumers.Count();
                consumers = consumers.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View(new PageResponse<List<Consumer>>(consumers, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public IActionResult BlockOrUnblockConsumer(int userId)
        {
            try
            {
                SetRoleByCurrentUser();
                var consumer = _consumerService.BlockOrUnblockConsumer(userId);
                var consumers = _consumerService.GetAllConsumer().OrderByDescending(x => x.FullName).ToList();
                var pageResponse = new PageResponse<List<Consumer>>(
                           data: consumers,
                           pageNumber: 1,
                           pageSize: consumers.Count,
                           totalCount: consumers.Count
                       );
                SetRoleByCurrentUser();
                return View("GetAllConsumers", pageResponse);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToBlockOrUnblockConsumer;
                return RedirectToAction(Constants.AdminActionMethods.GetAllConsumers, Constants.AdminActionMethods.Admin);
            }
        }


        #endregion

        #region Reports
        [AuthorizeAdministrativeAccounts]
        public IActionResult Reports([FromQuery] Pagination pagination)
        {
            try
            {
                SetRoleByCurrentUser();
                var reports = _reportService.GetAllReports(pagination);
                return View("Reports", reports);
            }
            catch (Exception)
            {
                SetRoleByCurrentUser();
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AdminLogin;
                return RedirectToAction(Constants.AdminActionMethods.Dashboard, Constants.AdminActionMethods.Admin);
            }
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult ViewReport(int withdrawalId)
        {
            var orderDetail = _orderService.GetOrderByWithdraalId(withdrawalId);
            return PartialView("_TransactionDetailsPartialView", orderDetail);
        }

        [AuthorizeAdministrativeAccounts]
        public IActionResult GenerateInvoice(int withdrawalId, string userEmail)
        {
            var userId = _userService.GetUserIdByEmail(userEmail);
            var user = _userService.GetById(userId);
            var invoiceData = _reportService.GetTransactionDetail(withdrawalId, user);
            return PartialView("_RoportInvoice", invoiceData);
        }
        #endregion

        #region Wallet

        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public IActionResult Wallet()
        {
            var model = new AddWalletViewModel();
            return View(model);
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> Wallet(AddWalletViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userEmail = model.Email;
                var walletAmount = model.Amount;

                var profit = await _orderService.GetOrdersProfit(userEmail);
                var user = _userService.GetByEmail(userEmail);

                if (profit.AvailableBalance <= -600 && user != null && user.IsSeller == true && user.IsProfileCompleted == true && user.EmailConfirmed == true)
                {
                    var updateWallet = await _walletService.AddWallet(userEmail, walletAmount);
                    if (updateWallet)
                    {
                        var activateUser = _userService.ActivateUser(user.Id);
                    }
                    TempData[Constants.AdminTempData.NotificationMessage] = Constants.NotificationSuccessMessage.WalletAddedSuccefully;
                    return RedirectToAction(Constants.AdminActionMethods.Wallet, Constants.AdminActionMethods.Admin);
                }

                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToAddWallet;
            }
            return View(model);
        }

        #endregion


        #region Inventory Management
        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> GetInventory(string userId)
        {
            var result = await _inventoryService.GetByUserId(userId);
            return PartialView("_inventoryAssign", result);
        }

        [HttpPost]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> AddInventory(AddInventoryAssignModel addInventoryAssignModel)
        {
            addInventoryAssignModel.AssignBy = _currentUser.Email;
            var result = await _inventoryService.AddAssignInventory(addInventoryAssignModel);
            if (result)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.AssignSuccessfully;
                return RedirectToAction(Constants.AdminActionMethods.GetAllSellers, Constants.AdminActionMethods.Admin);
            }
            else
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.SomethingWrong;
                return RedirectToAction(Constants.AdminActionMethods.GetAllSellers, Constants.AdminActionMethods.Admin);
            }
        }


        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public async Task<IActionResult> ViewAssignHistory(string userId)
        {
            var result = await _inventoryService.GetInventoryHistoryByUserId(userId);
            return PartialView("_inventoryHistory", result);
        }

        [HttpGet]
        [AuthorizeAdministrativeAccounts]
        public async Task<ActionResult> SearchProduct(string name, string userId)
        {
            var Model = await _inventoryService.GetInventoryListByUserId(userId);
            var productIdsInList = Model.Select(p => p.ProductId).ToList();
            var products = await _context.Products
                .Where(p => p.Name.Contains(name) && !productIdsInList.Contains(p.ProductId))
                .Select(p => new
                {
                    Id = p.ProductId,
                    Sourcing = p.Sourcing,
                    Name = p.Sourcing == Sourcing.Local ? p.Name + " (Local)" :
                           p.Sourcing == Sourcing.China ? p.Name + " (China)" :
                           p.Name
                })
                .ToListAsync();
            return Json(products);
        }
        #endregion

        #region private methods
        private string GetContentType(string fileName)
        {
            string contentType = "application/octet-stream";

            if (fileName.EndsWith(".jpg"))
            {
                contentType = "image/jpeg";
            }
            else if (fileName.EndsWith(".png"))
            {
                contentType = "image/png";
            }
            else if (fileName.EndsWith(".gif"))
            {
                contentType = "image/gif";
            }
            else if (fileName.EndsWith(".bmp"))
            {
                contentType = "image/bmp";
            }
            else if (fileName.EndsWith(".webp"))
            {
                contentType = "image/webp";
            }
            else if (fileName.EndsWith(".tiff") || fileName.EndsWith(".tif"))
            {
                contentType = "image/tiff";
            }
            else if (fileName.EndsWith(".svg"))
            {
                contentType = "image/svg+xml";
            }
            else if (fileName.EndsWith(".ico"))
            {
                contentType = "image/x-icon";
            }

            return contentType;
        }
        private IEnumerable<AddWithdrawalUserViewModel> getProfileAndWithdrawalData(IEnumerable<Withdrawals> withdrawals, IEnumerable<Profiles> profiles)
        {
            var combinedData = from withdrawal in withdrawals
                               join profile in profiles
                               on withdrawal.UserEmail equals profile.User.Email into joinedData
                               from profileData in joinedData.DefaultIfEmpty()
                               select new AddWithdrawalUserViewModel
                               {
                                   WithDrawalId = withdrawal.WithdrawalId,
                                   UserEmail = withdrawal.UserEmail,
                                   AmountInPkr = withdrawal.AmountInPkr,
                                   paymentStatus = withdrawal.PaymentStatus,
                                   ProcessedBy = withdrawal.ProcessedBy,
                                   CreatedDate = withdrawal.CreatedDate,
                                   AccountTitle = profileData?.BankAccountTitle,
                                   BankAccountNumberOrIBAN = profileData?.BankAccountNumberOrIBAN,
                                   BankName = profileData?.BankName,
                                   Withdrawals = new List<Withdrawals> { withdrawal },
                                   Profiles = profileData != null ? new List<Profiles> { profileData } : new List<Profiles>()
                               };
            return combinedData;
        }



        private void SetRoleByCurrentUser()
        {
            TempData["IsAdmin"] = _currentUser.IsAdmin;
            TempData["IsSuperAdmin"] = _currentUser.IsSuperAdmin;
        }

        private async Task<AdminDashboardDto> calculateAdminDashboardCounters()
        {
            AdminDashboardDto adminDashboardDto = new AdminDashboardDto();
            adminDashboardDto.TotalSellers = _userService.GetAll().Count;
            adminDashboardDto.TotalActiveSubscriber = _context.Subscriptions.Where(x => (int)x.SubscriptionStatus == (int)SubscriptionStatus.Approved).Count();
            adminDashboardDto.TotalPendingWithdrawalRequests = _context.Withdrawals.Where(x => (int)x.PaymentStatus == (int)PaymentStatus.Processing && (int)x.PaymentStatus == (int)PaymentStatus.UnPaid).Count();
            adminDashboardDto.TotalPendingOrders = _context.Orders.Where(x => (int)x.OrderStatus == (int)OrderStatus.Pending).Count();
            adminDashboardDto.TotalOrders = _orderService.GetAll().Count();
            adminDashboardDto.ReturnedOrders = await _orderService.GetAllReturnedOrders();
            adminDashboardDto.OrdersInTransit = await _orderService.GetAllOrdersInTransit();
            adminDashboardDto.TotalDeliveredOrders = await _orderService.GetAllDeliveredOrders();
            adminDashboardDto.TotalSales = await _orderService.GetAllGrandTotal();
            adminDashboardDto.TotalCancelledOrders = await _orderService.GetAllCancelledOrders();
            adminDashboardDto.TotalPaidAmount = await _withdrawlsService.GetPaidWithdrawalAmount();
            adminDashboardDto.TotalUnpaidAmount = await _withdrawlsService.GetAllUnpaidAmount();
            return adminDashboardDto;
        }
        #endregion
    }
}