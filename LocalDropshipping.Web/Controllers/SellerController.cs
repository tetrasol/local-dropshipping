﻿using LocalDropshipping.Web.Attributes;
using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Dtos;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Exceptions;
using LocalDropshipping.Web.Extensions;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Helpers.Constants;
using LocalDropshipping.Web.Helpers.Inventory;
using LocalDropshipping.Web.Models;
using LocalDropshipping.Web.Models.Inventory;
using LocalDropshipping.Web.Models.ProductViewModels;
using LocalDropshipping.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NuGet.Configuration;
using Org.BouncyCastle.Asn1.X509;
using System.IO.Compression;
using System.Security.Claims;

namespace LocalDropshipping.Web.Controllers
{
    public class SellerController : Controller
    {
        private readonly IProfilesService _profileService;
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        private readonly IOrderService _orderService;
        private readonly IProductsService _productsService;
        private readonly IWishListService _wishlistService;
        private readonly IProductVariantService _productVariantService;
        private readonly UserManager<User> _userManager;
        private readonly IConsumerService _consumerService;
        private readonly IOrderItemService _orderItemService;
        private readonly IFocSettingService _focSettingService;
        private readonly LocalDropshippingContext _context;
        private readonly IWebHostEnvironment _env;
        private readonly IInventoryService _inventoryService;
        private readonly ICategoryService _categoryService;
        private readonly SignInManager<User> _signInManager;
        private readonly IWithdrawalService _withdrawlsService;
        private readonly IAdminService _service;
        private readonly ICourierService _courierService;
        private readonly IProfilesService _profilesService;
        private readonly IReportService _reportService;
        private readonly IWalletService _walletService;
        private readonly IWholesaleService _wholesaleService;
        private readonly string _system = "system@focfulfillment.com";
        private readonly LocalDropshippingContext context;

        private User _currentUser { get => (User)HttpContext.Items["CurrentUser"]; }
        private bool _isLoggedIn { get => HttpContext.User.Identity.IsAuthenticated; }
        public SellerController(IAccountService accountService, IProfilesService profileService, IUserService userService, IOrderService orderService, UserManager<User> userManager, IWishListService wishList, IProductsService productsService, IProductVariantService productVariantService, IConsumerService consumerService, IOrderItemService orderItemService, IFocSettingService focSettingService, SignInManager<User> signInManager, ICategoryService categoryService, IWithdrawalService withdrawlsService, IAdminService service, ICourierService courierService, IProfilesService profilesService, IReportService reportService, IWalletService walletService, IWholesaleService wholesaleService,LocalDropshippingContext context,IWebHostEnvironment env,IInventoryService inventoryService)
        {
            _service = service;
            _courierService = courierService;
            _profilesService = profilesService;
            _reportService = reportService;
            _walletService = walletService;
            _accountService = accountService;
            _profileService = profileService;
            _userService = userService;
            _orderService = orderService;
            _productsService = productsService;
            _wishlistService = wishList;
            _productVariantService = productVariantService;
            _userManager = userManager;
            _consumerService = consumerService;
            _orderItemService = orderItemService;
            _focSettingService = focSettingService;
            _signInManager = signInManager;
            _categoryService = categoryService;
            _withdrawlsService = withdrawlsService;
            _wholesaleService = wholesaleService;
            _context = context;
            _env = env;
            _inventoryService = inventoryService;
        }


        #region Accounts
        public IActionResult Register()
        {
            if (_isLoggedIn)
            {
                if (_currentUser.IsSeller)
                {
                    if (!_currentUser.EmailConfirmed)
                        return RedirectToAction("VerificationEmailSent", "Seller");

                    if (!_currentUser.IsProfileCompleted)
                        return RedirectToAction("ProfileVerification", "Seller");

                    return RedirectToAction("SellerDashboard", "Seller");
                }
                if (_currentUser.IsAdmin || _currentUser.IsSuperAdmin)
                    return RedirectToAction("Dashboard", "Admin");

            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(SignupViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User user = new User
                    {
                        Email = model.Email.ToLower().Trim(),
                        UserName = model.Email.Substring(0, model.Email.IndexOf("@")),
                        Fullname = model.FullName,
                        PhoneNumber = model.PhoneNumber,
                        IsSeller = true,
                        CreatedBy = _system,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now,
                    };
                    var userExist = _userService.CheckEmailExist(user.Email);
                    if (userExist)
                    {
                        ModelState.AddModelError("Email", "Email already exist.");
                        return View(model);
                    }
                    var result = await _accountService.RegisterAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        TempData.Add("EmailSent", true);
                        return RedirectToAction("VerificationEmailSent");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            if (error.Code == "DuplicateUserName")
                            {
                                ModelState.AddModelError("", string.Join(": ", "DuplicateEmail", "Email already exists"));
                            }
                            else
                            {
                                ModelState.AddModelError("", string.Join(": ", error.Code, error.Description));
                            }
                        }
                    }
                }
                return View(model);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToRegister;
                return RedirectToAction(Constants.AdminActionMethods.ProductionErrorView, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult VerificationEmailSent()
        {
            try
            {
                if (_isLoggedIn)
                {
                    if (!_currentUser.EmailConfirmed)
                        return View();
                    if (!_currentUser.IsProfileCompleted)
                        return RedirectToAction("ProfileVerification", "Seller");

                    return RedirectToAction("SellerDashboard", "Seller");
                }
                if (TempData.ContainsKey("EmailSent") && (bool)TempData["EmailSent"])
                {
                    return View();
                }
                return RedirectToAction("Login", "Seller");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        public async Task<IActionResult> EmailVerification(string userId, string token)
        {
            try
            {
                if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(token))
                {
                    return RedirectToAction("InvalidVerificationLink");
                }
                var isVerified = await _accountService.ConfirmEmailAsync(userId, token);
                if (isVerified)
                {
                    return RedirectToAction("Login","Seller");
                }
                else
                {
                    return RedirectToAction("InvalidVerificationLink");
                }
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult EmailVerified()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult Login()
        {
            if (_isLoggedIn)
            {
                return _currentUser.IsSeller ? RedirectToAction("SellerDashboard", "Seller") : RedirectToAction("Dashboard", "Admin");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _accountService.SellerLoginAsync(model.Email, model.Password, model.RememberMe);
                    if (user != null)
                    {
                        if (user.IsActive)
                        {
                            await UpdateOrderStatuses(user.Email);
                            
                            if (!System.String.IsNullOrEmpty(returnUrl))
                            {
                                return LocalRedirect(returnUrl);
                            }
                            if (model.RememberMe)
                            {
                                var rememberMeCookieOptions = new CookieOptions
                                {
                                    Expires = DateTime.UtcNow.AddDays(7),
                                    IsEssential = true,
                                };
                                Response.Cookies.Append("RememberMeEmail", model.Email, rememberMeCookieOptions);
                                Response.Cookies.Append("RememberMePassword", model.Password, rememberMeCookieOptions);
                            }
                            return RedirectToAction("SellerDashboard", "Seller");
                        }
                        ModelState.AddModelError("", "This account is not active, please contact support@focfulfillment.com");
                    }
                }
                return View(model);
            }
            catch (IdentityException ex)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = ex.Message;
                return View(model);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.ProductionErrorView, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult LoginWithGoogleAsync(string returnUrl)
        {
            return _accountService.GoogleSignin(Url.Action("ExternalLoginCallback", "Seller", new { ReturnUrl = returnUrl }));
        }

        public IActionResult LoginWithFacebookAsync(string returnUrl)
        {
            return _accountService.FacebookSignin(Url.Action("ExternalLoginCallback", "Seller", new { ReturnUrl = returnUrl }));
        }

        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                return BadRequest("Some went wrong. Invalid response from the third party.");
            }

            var succeeded = await _accountService.ExternalLoginAsync();
            if (succeeded)
            {
                if (!System.String.IsNullOrEmpty(returnUrl))
                {
                    return LocalRedirect(returnUrl);
                }
                return RedirectToAction("SellerDashboard");
            }
            return BadRequest("Some went wrong. May be, Invalid response from the third party or any internal issue.");
        }

        public IActionResult ForgotPassword()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]

        public async Task<IActionResult> ForgotPassword(string email)
        {
            try
            {
                var isPasswordResetLinkSent = await _accountService.ForgotPasswordAsync(email);
                if (isPasswordResetLinkSent)
                {
                    TempData["ForgotPassword"] = "Password Forgot successful. You can now log in.";
                    return RedirectToAction("ForgotPasswordMessage");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Email not found.");
                    return View("ForgotPassword");
                }
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedtoUpdatePassword;
                return RedirectToAction(Constants.AdminActionMethods.ForgotPassword, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult ForgotPasswordMessage()
        {
            try
            {
                return View();

            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }
        public IActionResult UpdatePassword(string userId, string token)
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePassword(NewPasswordViewModel model)
        {
            try
            {
                var isUpdated = await _accountService.UpdatePasswordAsync(model);
                if (isUpdated)
                {
                    ViewBag.ShowSuccessMessageAndRedirect = true;
                    return RedirectToAction("UpdatePasswordMessage");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Password update failed.");
                    return View("UpdatePassword");
                }
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedtoUpdatePassword;
                return RedirectToAction(Constants.AdminActionMethods.UpdatePassword, Constants.AdminActionMethods.Seller);
            }

        }

        [Authorize]
        public IActionResult UpdatePasswordMessage()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        #endregion

        public IActionResult contactUs()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult contactUsMessage()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        public async Task<IActionResult> contactUs(ContactUsViewModel contactUsViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool emailSent = await _accountService.SendContactEmailAsync(contactUsViewModel);
                    if (emailSent)
                    {
                        return RedirectToAction("contactUsMessage");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Failed to send contact email. Please try again later.");
                    }
                }
                return View("ContactUs", contactUsViewModel);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.ContactUs, Constants.AdminActionMethods.Seller);
            }
        }

        [Authorize]
        public IActionResult Subscribe()
        {
            try
            {
                if (!_currentUser.EmailConfirmed)
                    return RedirectToAction("VerificationEmailSent", "Seller");
                if (!_currentUser.IsProfileCompleted)
                    return RedirectToAction("ProfileVerification", "Seller");
                if (!_currentUser.IsSubscribed)
                    return View();
                return RedirectToAction("SellerDashboard", "Seller");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult Shop()
        {
            try
            {
                var currentUserName = _currentUser.Fullname;
                ViewBag.categories = _categoryService.GetAll();
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                var bestSellingProducts = _productsService.GetBestSelleingProducts().ToList();
                var newArrivalProducts = _productsService.GetNewArrivalProducts().ToList();
                var topRatedProducts = _productsService.GetTopRatedProducts().ToList();
                var random = new Random();
                var shuffledBestSellingProducts = bestSellingProducts.OrderBy(x => random.Next()).Select(x => new ShopProductViewModel
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                    VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                    VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                    ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                    HasVariants = x.Variants.Count() > 1,

                }).ToList();
                var shuffledNewArrivalProducts = newArrivalProducts.OrderBy(x => random.Next()).Select(x => new ShopProductViewModel
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                    VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                    VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                    ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                    HasVariants = x.Variants.Count() > 1,

                }).ToList();
                var shuffledTopRatedProducts = topRatedProducts.OrderBy(x => random.Next()).Select(x => new ShopProductViewModel
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                    VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                    VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                    ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                    HasVariants = x.Variants.Count() > 1,

                }).ToList();
                ShopViewModel shopViewModel = new ShopViewModel()
                {
                    BestSeller = shuffledBestSellingProducts.Take(15).ToList(),
                    NewArrival = shuffledNewArrivalProducts.Take(15).ToList(),
                    TopRated = shuffledTopRatedProducts.Take(15).ToList(),
                    Cart = cart
                };
                ViewBag.total = TotalCost();
                return View(shopViewModel);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.SellerLogin, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        [OnlySellerWithCompleteProfile]
        public async Task<JsonResult> AddToCart(string id)
        {
            try
            {
                Product productItem = _productsService.GetById(Convert.ToInt32(id == string.Empty ? 0 : id));
                var mainVariant = productItem.Variants.First();
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                int quantity = 1;
                double itemSubtotal;
                decimal newTotalCost;
                decimal newGrandTotal;
                decimal shipping = 0;
                decimal serviceCharges = 0;
                decimal? totalWeight = 0;

                var quantityIsAssigned = await _context.InventoryAssigns.FirstOrDefaultAsync(x => x.ProductId == productItem.ProductId && x.UserId == _currentUser.Id);

                if (quantityIsAssigned == null || quantityIsAssigned.Quantity <= 0)
                {
                    TempData["AddToCart"] = false;
                    return Json(new { Success = false, notificationMessage = Constants.ErrorMessage.ZeroProductRemaining });
                }

                if (cart == null)
                {
                    cart = new List<OrderItem>();
                }

                var index = cart.FindIndex(w => w.ProductId == (Convert.ToInt32(id)));

                if (index != -1)
                {
                    if (cart[index].Quantity < 9)
                    {
                        cart[index].Quantity++;
                        cart[index].SubTotal = cart[index].Quantity * cart[index].Price;

                        await _inventoryService.DecreaseQuantity(_currentUser.Id, productItem.ProductId, quantity);
                    }
                    else
                    {
                        return Json(new { Success = false, notificationMessage = Constants.ErrorMessage.MaxQuantityReached });
                    }
                }
                else
                {
                    cart.Add(new OrderItem
                    {
                        ProductId = productItem.ProductId,
                        Name = productItem.Name,
                        Image = mainVariant.FeatureImageLink,
                        Quantity = quantity,
                        Price = mainVariant.DiscountedPrice,
                        SubTotal = quantity * mainVariant.DiscountedPrice
                    });

                    await _inventoryService.DecreaseQuantity(_currentUser.Id, productItem.ProductId, quantity);
                }
                foreach (var item in cart)
                {
                    Product product = _productsService.GetById(item.ProductId);
                    totalWeight += product.Weight * item.Quantity;
                    ProductVariant productVariant = _productVariantService.GetByProductIdAsync(item.ProductId);
                    serviceCharges += productVariant.DiscountedPrice * item.Quantity;
                }

                shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
                serviceCharges = serviceCharges / 100 * 10 * quantity;

                HttpContext.Session.Set<List<OrderItem>>("cart", cart);
                int newIndex = cart.FindIndex(w => w.ProductId == (Convert.ToInt32(id)));
                itemSubtotal = cart[newIndex].SubTotal;
                newTotalCost = TotalCost();
                newGrandTotal = newTotalCost + shipping + serviceCharges;

                return Json(new
                {
                    Success = true,
                    notificationMessage = Constants.ErrorMessage.ItemAddToCart,
                    newSubtotal = itemSubtotal,
                    totalCost = newTotalCost,
                    grandTotal = newGrandTotal,
                    serviceCharges = serviceCharges,
                    shipping = shipping,
                    Counter = cart.Count,
                    Cart = cart
                });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.ToString() });
            }
        }


        [OnlySellerWithCompleteProfile]
        public async Task<JsonResult> Minus(string id)
        {
            try
            {
                Product productItem = _productsService.GetById(Convert.ToInt32(id == string.Empty ? 0 : id));
                var mainVariant = productItem.Variants.FirstOrDefault(x => x.VariantType == "MAIN_VARIANT");
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                decimal newTotalCost;
                decimal newGrandTotal = 0;
                decimal shipping = 0;
                decimal serviceCharges = 0;

                if (cart != null && cart.Any())
                {
                    await _inventoryService.IncreaseQuantity(_currentUser.Id, productItem.ProductId, 1);
                    int index = cart.FindIndex(w => w.ProductId == (Convert.ToInt32(id == string.Empty ? 0 : id)));

                    if (index == -1)
                    {
                        return Json(new { error = false, Counter = cart.Count, Cart = cart });
                    }
                    else
                    {

                        if (cart[index].Quantity == 1)
                        {
                            cart.RemoveAt(index);

                        }
                        else
                        {
                            cart[index].Quantity--;
                            cart[index].SubTotal = cart[index].Quantity * cart[index].Price;
                        }

                        HttpContext.Session.Set<List<OrderItem>>("cart", cart);

                        newTotalCost = TotalCost();
                        decimal? totalWeight = 0;
                        foreach (var item in cart)
                        {
                            totalWeight += productItem.Weight * item.Quantity;
                            ProductVariant productVariant = _productVariantService.GetByProductIdAsync(item.ProductId);
                            serviceCharges += productVariant.DiscountedPrice * item.Quantity;
                            shipping += ShippingCostCalculator.CalculateShippingCost(productItem.Weight * item.Quantity, ShippingCost());
                        }

                        serviceCharges = serviceCharges / 100 * 10;

                        newGrandTotal = newTotalCost + shipping + serviceCharges;

                        int newIndex = cart.FindIndex(w => w.ProductId == (Convert.ToInt32(id)));

                        if (newIndex == -1)
                        {
                            return Json(new
                            {
                                Success = true,
                                newQuantity = 0,
                                totalCost = newTotalCost,
                                shipping = shipping,
                                serviceCharges = serviceCharges,
                                grandTotal = newGrandTotal,
                                Counter = cart.Count,
                                Cart = cart
                            });
                        }
                        else
                        {
                            double itemSubtotal = cart[newIndex].SubTotal;
                            int itemQuantity = cart[newIndex].Quantity;

                            return Json(new
                            {
                                Success = true,
                                notificationMessage = Constants.ErrorMessage.ItemRemovedFromCart,
                                newQuantity = itemQuantity,
                                newSubtotal = itemSubtotal,
                                shipping = shipping,
                                serviceCharges = serviceCharges,
                                totalCost = newTotalCost,
                                grandTotal = newGrandTotal,
                                Counter = cart.Count,
                                Cart = cart
                            });
                        }
                    }
                }
                else
                {
                    return Json(new { error = false });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.ToString() });
            }
        }

        public PartialViewResult GetCartItems()
        {
            try
            {
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                ViewBag.total = TotalCost();
                return PartialView("_cartItem", cart);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return PartialView("_cartItem");
            }
        }

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> Withdrawal([FromQuery] Pagination pagination)
        {
            try
            {
                var withdrawals = _withdrawlsService.GetWithdrawalByUserEmail(_currentUser.Email);
                ViewBag.orders = _orderService.GetByEmail(_currentUser.Email).OrderByDescending(x => x.CreatedDate).ToList();
                ViewBag.model = await _orderService.GetOrdersProfit(_currentUser.Email);
                ViewBag.shipping = ShippingCost();
                if (withdrawals != null)
                {
                    var withdrawalModel = withdrawals.Select(withdrawal => new WithdrawalModel
                    {
                        AmountInPkr = withdrawal.AmountInPkr,
                        PaymentStatus = withdrawal.PaymentStatus,
                        TransactionId = withdrawal.TransactionId,
                        RequestDate = withdrawal.CreatedDate,
                        Reason = withdrawal.Reason
                    }).ToList();
                    withdrawalModel = withdrawalModel.OrderByDescending(x => x.RequestDate).ToList();
                    var count = withdrawalModel.Count();
                    withdrawalModel = withdrawalModel.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                    return View(new PageResponse<List<WithdrawalModel>>(withdrawalModel, pagination.PageNumber, pagination.PageSize, count));
                }
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.SellerDashboard, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpGet]
        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> SellerDashboard()
        {
            try
            {
                string? currentUserID = _userManager.GetUserId(HttpContext.User);
                var currentUser = _userService.GetById(currentUserID);
                HttpContext.Session.SetString("CurrentUser", JsonConvert.SerializeObject(currentUser));
                ViewBag.model = await _orderService.GetOrdersProfit(currentUser.Email);
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpGet]
        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> Wishlist([FromQuery] Pagination pagination)
        {
            try
            {
                var cartData = _wishlistService.GetAllbyUserId(_currentUser.Id);
                List<AddProductVariantViewModel> data = new List<AddProductVariantViewModel>();
                foreach (var item in cartData)
                {
                    var product = _productsService.GetById(item.ProductId);
                    data.Add(new AddProductVariantViewModel
                    {
                        Product = product,
                    });
                }
                var count = data.Count();
                data = data.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View(new PageResponse<List<AddProductVariantViewModel>>(data, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.SellerDashboard, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        [OnlySellerWithCompleteProfile]
        public IActionResult Wishlist(int ProductId)
        {
            try
            {

                if (_wishlistService.ValidateWishlistProduct(_currentUser.Id, ProductId))
                {
                    _wishlistService.Add(_currentUser.Id, ProductId);
                }
                else
                {
                    _wishlistService.Delete(_currentUser.Id, ProductId);
                    return Json(data: new { success = true, notificationMessage = Constants.ErrorMessage.ItemRemoveFromWishlist });
                }
                return Json(data: new { success = true, notificationMessage = Constants.ErrorMessage.ItemAddToWishlist });
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.Wishlist;
                return RedirectToAction(Constants.AdminActionMethods.Wishlist, Constants.AdminActionMethods.Seller);
            }

        }

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> DownlaodMediaAsZipAsync(int id)
        {
            var mediaLinks = new List<string>();
            var product = _productsService.GetById(id);
            if (product == null)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage._404;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }

            var productVariants = product.Variants;
            foreach (var variants in productVariants)
            {
                mediaLinks.Add(variants.FeatureImageLink);
                mediaLinks.AddRange(variants.Images.Select(x => x.Link));
                mediaLinks.AddRange(variants.Videos.Select(x => x.Link));
            }
            mediaLinks = mediaLinks.Select(x => Environment.CurrentDirectory + "\\wwwroot" + x.Replace("/", "\\")).ToList();
            var zipFileMemoryStream = new MemoryStream();
            using (ZipArchive archive = new ZipArchive(zipFileMemoryStream, ZipArchiveMode.Update, leaveOpen: true))
            {
                foreach (var link in mediaLinks)
                {
                    var mediaFile = Path.GetFileName(link);
                    var entry = archive.CreateEntry(mediaFile);
                    using (var entryStream = entry.Open())
                    using (var fileStream = System.IO.File.OpenRead(link))
                    {
                        await fileStream.CopyToAsync(entryStream);
                    }
                }
            }
            zipFileMemoryStream.Seek(0, SeekOrigin.Begin);
            return File(zipFileMemoryStream, "application/octet-stream", $"{product.Name}.zip");
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult Product(int id)
        {
            try
            {
                Product? product = _productsService.GetById(id);
                var bestSellingProducts = _productsService.GetAll()
                                                          .Where(x => x.IsBestSelling)
                                                          .Take(4).Select(x => new ShopProductViewModel
                                                          {
                                                              ProductId = x.ProductId,
                                                              Name = x.Name,
                                                              DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                                                              VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                                                              VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                                                              ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                                                              HasVariants = x.Variants.Count() > 1,
                                                          }).ToList();
                var topRatedProducts = _productsService.GetAll()
                                                                     .Where(x => x.IsTopRated)
                                                                     .Take(12).Select(x => new ShopProductViewModel
                                                                     {
                                                                         ProductId = x.ProductId,
                                                                         Name = x.Name,
                                                                         DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                                                                         VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                                                                         VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                                                                         ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                                                                         HasVariants = x.Variants.Count() > 1,
                                                                     }).ToList();
                if (product != null)
                {
                    return View(new ProductPageViewModel { Product = product, BestSellingProducts = bestSellingProducts, TopRatedProducts = topRatedProducts });
                }
                TempData["Message"] = "Product does not exist.";
                return RedirectToAction("Shop");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult Cart()
        {
            try
            {
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                decimal? totalWeight = 0;
                decimal serviceCharges = 0;
                foreach (var item in cart)
                {
                    Product product = _productsService.GetById(item.ProductId);
                    totalWeight += product.Weight * item.Quantity;
                    ProductVariant productVariant = _productVariantService.GetByProductIdAsync(item.ProductId);
                    serviceCharges += productVariant.DiscountedPrice * item.Quantity;
                }
                decimal shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
                serviceCharges = serviceCharges / 100 * 10;
                ViewBag.total = TotalCost();
                ViewBag.shipping = shipping;
                ViewBag.serviceCharges = serviceCharges;
                ViewBag.totalCost = ViewBag.total + ViewBag.shipping + serviceCharges;
                return View(cart);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> RemoveFromCart(int id)
        {
            try
            {
                var cart = DeleteItemFromCart(id);
                return RedirectToAction("Cart");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public async Task<PartialViewResult> DeleteFromCart(int id)
        {
            try
            {
                var cart = await DeleteItemFromCart(id);
                return GetCartItems();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return GetCartItems();
            }
        }

        [OnlySellerWithCompleteProfile]
        private async Task<List<OrderItem>> DeleteItemFromCart(int id)
        {
            var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
            var items = cart.FirstOrDefault(x => x.ProductId == id);
            await _inventoryService.IncreaseQuantity(_currentUser.Id, id, items.Quantity);
            int index = cart.FindIndex(w => w.ProductId == id);
            cart.RemoveAt(index);
            HttpContext.Session.Set<List<OrderItem>>("cart", cart);
            return cart;
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult Checkout()
        {
            try
            {
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                ViewBag.total = TotalCost();
                decimal? totalWeight = 0;
                decimal serviceCharges = 0;
                foreach (var item in cart)
                {
                    Product product = _productsService.GetById(item.ProductId);
                    totalWeight += product.Weight * item.Quantity;
                    ProductVariant productVariant = _productVariantService.GetByProductIdAsync(item.ProductId);
                    serviceCharges += productVariant.DiscountedPrice * item.Quantity;
                }
                decimal shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
                ViewBag.shipping = shipping;
                serviceCharges = serviceCharges / 100 * 10;
                ViewBag.serviceCharges = serviceCharges;
                ViewBag.totalCost = ViewBag.total + ViewBag.shipping + serviceCharges;
                var checkoutViewModel = new CheckoutViewModel
                {
                    Cart = cart,
                };
                return View(checkoutViewModel);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [Authorize]
        public IActionResult ProfileVerification()
        {
            try
            {

                if (_currentUser.IsAdmin || _currentUser.IsSuperAdmin)
                    return RedirectToAction("Dashboard", "Admin");

                if (!_currentUser.EmailConfirmed)
                    return RedirectToAction("EmailVerification", "Seller");

                if (!_currentUser.IsProfileCompleted)
                    return View();

                return RedirectToAction("SellerDashboard", "Seller");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        public async Task<IActionResult> ProfileVerification(ProfileVerificationViewModel profileVerificationViewModel)
        {
            try
            {
                if (ModelState.IsValid == false)
                {
                    return View(profileVerificationViewModel);
                }
                var user = _currentUser;
                user.IsProfileCompleted = true;
                var profile = profileVerificationViewModel.ToEntity();
                profile.UserId = user.Id;
                await _userService.UpdateUserAsync(user);
                _profileService.Add(profile);
                return RedirectToAction("SellerDashboard", "Seller");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToVerifyProfile;
                return RedirectToAction(Constants.AdminActionMethods.ProfileVerification, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult SellerOrders([FromQuery] Pagination pagination)
        {
            try
            {
                SellerViewOrder orders;
                string currentUserEmail = _currentUser.Email;
                if (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue)
                {
                    orders = _orderService.GetFilteredOrders(pagination,currentUserEmail);
                }
                else
                {
                   orders = _orderService.GetOrders(currentUserEmail);
                }
                var count = orders.Orders.Count();
                orders.Orders = orders.Orders.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View("SellerOrders", new PageResponse<List<SellerViewOrder>>(new List<SellerViewOrder> { orders }, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult ViewOrder(int id)
        {
            try
            {
                var order = _orderService.GetById(id);
                var userId = _userService.GetUserIdByEmail(order.Seller);
                var profile = _profilesService.GetProfileById(userId);
                var consumer = _consumerService.GetConsumerByOrderId(id);
                var orderDetails = new
                {
                    businessName = profile?.StoreName,
                    courierService = order.CourierServiceType == null ? CourierServiceType.NotSelected.GetDescription() : order.CourierServiceType.GetDescription(),
                    sellPrice = order?.SellPrice,
                    shippingFee = ShippingCost(),
                    total = order?.GrandTotal,
                    paymentMethod = "COD",
                    yourCustomerCity = consumer?.City == null ? "<Not Available>" : consumer.City,
                    yourCustomerAddress = consumer?.Address == null ? "<Not Available>" : consumer.Address,
                    orderId = order?.Id,
                    createdDate = order?.CreatedDate.ToString("MMM dd, yyyy"),
                    status = order?.OrderStatus == null ? OrderStatus.Pending.GetDescription() : order.OrderStatus.GetDescription(),
                    remarks = order?.SpecialInstructions == null ? "<Not Available>" : order.SpecialInstructions
                };
                if (orderDetails != null) return Json(orderDetails);
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLoadOrder;
                return RedirectToAction(Constants.AdminActionMethods.SellerOrders, Constants.AdminActionMethods.Seller);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLoadOrder;
                return RedirectToAction(Constants.AdminActionMethods.SellerOrders, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> PlaceOrder(CheckoutViewModel customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                    decimal sellingPrice = Convert.ToDecimal(customer.SellingPrice);
                    decimal serviceCharges = 0;
                    decimal? totalWeight = 0;
                    foreach (var item in cart)
                    {
                        Product product = _productsService.GetById(item.ProductId);
                        totalWeight += product.Weight * item.Quantity;
                        ProductVariant productVariant = _productVariantService.GetByProductIdAsync(item.ProductId);
                        serviceCharges += productVariant.DiscountedPrice * item.Quantity;
                    }
                    var checkConsumer = _consumerService.CheckConsumer(customer.PrimaryPhoneNumber, customer.SecondaryPhoneNumber);
                    decimal shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
                    serviceCharges = serviceCharges / 100m * 10m;
                    var GrandTotal = serviceCharges + shipping;
                    Order order;
                    Consumer consumer;
                    if (!checkConsumer)
                    {
                        order = await _orderService.AddOrder(cart, _currentUser.Email, sellingPrice, GrandTotal);
                        var orderId = order.Id;
                        consumer = _consumerService.AddConsumer(customer, orderId, _currentUser.Email);
                        HttpContext.Session.Remove("cart");
                    }
                    else
                    {
                        TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLoadOrder;
                        return RedirectToAction(Constants.AdminActionMethods.Checkout, Constants.AdminActionMethods.Seller);
                    }
                    var orderItems = _orderItemService.GetOrderItemsById(order.Id);
                    OrderSuccessModel model = new OrderSuccessModel()
                    {
                        OrderId = order.Id,
                        OrderItems = orderItems,
                        TotalItems = orderItems.Count(),
                        TotalItemsAmount = order.GrandTotal - shipping - serviceCharges,
                        ShippingCharges = shipping,
                        ShippingAddress = consumer.Address,
                        ShippingCity = consumer.City,
                        GrandTotal = order.GrandTotal,
                        Name = consumer.FullName,
                        PhoneNumber = consumer.PrimaryPhoneNumber,
                        ServiceCharges = serviceCharges 
                    };
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Checkout");
                }
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToPlaceOrder;
                return RedirectToAction(Constants.AdminActionMethods.Checkout, Constants.AdminActionMethods.Seller);
            }
        }

        private decimal TotalCost()
        {
            try
            {
                decimal totalRecord;
                var cart = HttpContext.Session.Get<List<OrderItem>>("cart");
                if (cart != null)
                {
                    totalRecord = (decimal)cart.Sum(s => s.Quantity * s.Price);
                }
                else
                {
                    cart = new List<OrderItem>();
                    totalRecord = 0;
                }
                return totalRecord;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult Profile()
        {
            try
            {
                var userId = (HttpContext.Items["CurrentUser"] as User).Id;
                var userProfile = _profileService.GetProfileById(userId);
                var userProfileDto = new ProfilesDto
                {
                    StoreName = userProfile.StoreName,
                    StoreURL = userProfile.StoreURL,
                    BankName = userProfile.BankName,
                    BankAccountTitle = userProfile.BankAccountTitle,
                    BankAccountNumberOrIBAN = userProfile.BankAccountNumberOrIBAN,
                    BankBranch = userProfile.BankBranch,
                    Address = userProfile.Address,
                    ImageLink = userProfile.ImageLink
                };
                var userProfileList = new List<ProfilesDto> { userProfileDto };
                return View(userProfileList);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        [OnlySellerWithCompleteProfile]
        public IActionResult Profile(IFormFile profileImage)
        {
            try
            {
                if (profileImage != null && profileImage.Length > 0)
                {
                    var folderPath = Path.Combine("ProfileImages");
                    var uniqueFileName = profileImage.SaveTo(folderPath, profileImage.FileName);
                    var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                    var sellerProfile = _context.Profiles.FirstOrDefault(p => p.UserId == userId);
                    if (sellerProfile != null)
                    {
                        sellerProfile.ImageLink = "/ProfileImages/" + uniqueFileName;
                    }
                    else
                    {
                        sellerProfile = new Profiles
                        {
                            UserId = userId,

                            ImageLink = "/ProfileImages/" + uniqueFileName
                        };

                        _context.Profiles.Add(sellerProfile);
                    }
                    _context.SaveChanges();
                    return RedirectToAction("SellerDashboard");
                }
                return View("Profile");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToUpdateProfile;
                return RedirectToAction(Constants.AdminActionMethods.Profile, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> UpdateSellerPassword()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> UpdateSellerPassword(SellerUpdatePasswordViewModel model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    var token = await _accountService.GeneratePasswordUpdateToken(user.Email);
                    var isUpdated = await _accountService.UpdatePassword(user.Id, token, model.NewPassword);
                    if (isUpdated)
                    {
                        await _accountService.LogoutAsync();
                        return RedirectToAction("Login", "Seller");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Failed to update password.");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "User not found.");
                }
                return View("Profile");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedtoUpdatePassword;
                return RedirectToAction(Constants.AdminActionMethods.UpdateSellerPassword, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult FilterPage([FromQuery] string searchString, string sortProduct, bool newArrivals, bool bestSellers, bool topRated, string minPrice, string maxPrice,ProductPagination pagination)
        {
            try
            {
                ViewBag.Currentfilter = searchString;
                ViewBag.newArrivals = newArrivals;
                ViewBag.bestSellers = bestSellers;
                ViewBag.topRated = topRated;
                ViewBag.minPrice = minPrice;
                ViewBag.maxPrice = maxPrice;
                ViewBag.NameSortParmAz = "name_asc";
                ViewBag.NameSortParmZa = "name_desc";
                ViewBag.PriceSortParmAsc = "price_asc";
                ViewBag.PriceSortParmDesc = "price_desc";
                List<Product> products = _productsService.GetAll();
                if (!string.IsNullOrEmpty(searchString))
                {
                    products = products.Where(x => x.Name.ToLower().Contains(searchString.ToLower()) ||
                    x.Description.ToLower().Contains(searchString.ToLower()) ||
                     x.Category.Name.ToLower().Contains(searchString.ToLower())).ToList();
                }
                if (newArrivals)
                {
                    products = _productsService.GetNewArrivalProducts().ToList();
                }
                if (bestSellers)
                {
                    products = _productsService.GetBestSelleingProducts().ToList();
                }
                if (topRated)
                {
                    products = _productsService.GetTopRatedProducts().ToList();
                }
                if (!string.IsNullOrEmpty(minPrice) && !string.IsNullOrEmpty(maxPrice))
                {
                    // Apply price range filter
                    decimal min = decimal.Parse(minPrice);
                    decimal max = decimal.Parse(maxPrice);
                    products = products.Where(x => x.Variants.FirstOrDefault().DiscountedPrice >= min && x.Variants.FirstOrDefault().DiscountedPrice <= max).ToList();
                }
                switch (sortProduct)
                {
                    case "name_asc":
                        products = products.OrderBy(s => s.Name).ToList();
                        break;
                    case "name_desc":
                        products = products.OrderByDescending(s => s.Name).ToList();
                        break;
                    case "price_asc":
                        products = products.OrderBy(s => s.Variants.FirstOrDefault().DiscountedPrice).ToList();
                        break;
                    case "price_desc":
                        products = products.OrderByDescending(s => s.Variants.FirstOrDefault().DiscountedPrice).ToList();
                        break;
                    default:
                        products = products.OrderBy(s => s.ProductId).ToList();
                        break;
                }

                var count = products.Count();
                var newProducts = products.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).Select(x=> new ShopProductViewModel
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                    VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                    VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                    ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                    HasVariants = x.Variants.Count() > 1,
                }).ToList();
                return View(new PageResponse<List<ShopProductViewModel>>(newProducts, pagination.PageNumber, pagination.PageSize, count));
               
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> WithdrawalRequest()
        {
            try
            {
                var email = _currentUser.Email;
                if (email != null)
                {
                    var orderProfit = await _orderService.GetOrdersProfit(_currentUser.Email);
                    if (orderProfit != null)
                    {
                       await _walletService.EmptyWallet(_currentUser.Email);
                    }
                    if (orderProfit.AvailableBalance >= Convert.ToDecimal(_focSettingService.GetPaymentLimit()))
                    {
                        var result = _withdrawlsService.WithdrawalRequest(email, orderProfit.AvailableBalance);
                        if (result) return RedirectToAction("Withdrawal");
                    }
                    else
                    {
                        TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToWithdrawal;
                        return RedirectToAction(Constants.AdminActionMethods.Withdrawal, Constants.AdminActionMethods.Seller);
                    }
                }
                return RedirectToAction("Withdrawal");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.SellerDashboard, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SellerLogout()
        {
            try
            {
                await _accountService.LogoutAsync();
                return RedirectToAction("Login", "Seller");
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLogout;
                return RedirectToAction(Constants.AdminActionMethods.SellerDashboard, Constants.AdminActionMethods.Seller);
            }
        }
       
        [HttpGet]
        public IActionResult Catalog([FromQuery] string searchString, string sortProduct, bool newArrivals, bool bestSellers, bool topRated, string minPrice, string maxPrice, ProductPagination pagination)
        {
            ViewBag.Currentfilter = searchString;
            ViewBag.newArrivals = newArrivals;
            ViewBag.bestSellers = bestSellers;
            ViewBag.topRated = topRated;
            ViewBag.minPrice = minPrice;
            ViewBag.maxPrice = maxPrice;
            ViewBag.NameSortParmAz = "name_asc";
            ViewBag.NameSortParmZa = "name_desc";
            ViewBag.PriceSortParmAsc = "price_asc";
            ViewBag.PriceSortParmDesc = "price_desc";
            List<Product> products = _productsService.GetAll();
            if (!string.IsNullOrEmpty(searchString))
            {
                products = products.Where(x => x.Name.ToLower().Contains(searchString.ToLower()) ||
                x.Description.ToLower().Contains(searchString.ToLower()) ||
                 x.Category.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }
            if (newArrivals)
            {
                products = _productsService.GetNewArrivalProducts().ToList();
            }
            if (bestSellers)
            {
                products = _productsService.GetBestSelleingProducts().ToList();
            }
            if (topRated)
            {
                products = _productsService.GetTopRatedProducts().ToList();
            }
            if (!string.IsNullOrEmpty(minPrice) && !string.IsNullOrEmpty(maxPrice))
            {
                decimal min = decimal.Parse(minPrice);
                decimal max = decimal.Parse(maxPrice);
                products = products.Where(x => x.Variants.FirstOrDefault().DiscountedPrice >= min && x.Variants.FirstOrDefault().DiscountedPrice <= max).ToList();
            }
            switch (sortProduct)
            {
                case "name_asc":
                    products = products.OrderBy(s => s.Name).ToList();
                    break;
                case "name_desc":
                    products = products.OrderByDescending(s => s.Name).ToList();
                    break;
                case "price_asc":
                    products = products.OrderBy(s => s.Variants.FirstOrDefault().DiscountedPrice).ToList();
                    break;
                case "price_desc":
                    products = products.OrderByDescending(s => s.Variants.FirstOrDefault().DiscountedPrice).ToList();
                    break;
                default:
                    products = products.OrderBy(s => s.ProductId).ToList();
                    break;
            }

            var count = products.Count();
            var catalogProducts = products.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).Select(x=> new ShopProductViewModel
            {
                ProductId = x.ProductId,
                Name = x.Name,
                DiscountedPrice = x.Variants.FirstOrDefault().DiscountedPrice,
                VariantPrice = x.Variants.FirstOrDefault().VariantPrice,
                VariantId = x.Variants.FirstOrDefault().ProductVariantId,
                ImageLink = x.Variants.FirstOrDefault().FeatureImageLink,
                HasVariants = x.Variants.Count() > 1,
            }).ToList();
            return View(new PageResponse<List<ShopProductViewModel>>(catalogProducts, pagination.PageNumber, pagination.PageSize, count));
        }
        public IActionResult Quotation()
        {
            return View();
        }
      
        [HttpPost]
        public async Task<IActionResult> Quotation(WholesaleViewModel model)
        {
            if (ModelState.IsValid)
            {
                Quotations quotation = new Quotations
                {
                    Fullname = model.Fullname,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    WhatsappPhoneNumber = model.WhatsappPhoneNumber,
                    BudgetInUSD =  model.BudgetInUSD,
                    ProductInformation = model.ProductInformation,
                    HomeCity = model.HomeCity,
                    WebsiteURL = model.WebsiteURL,
                    Sourcing = model.Sourcing == "Local" ? Sourcing.Local : Sourcing.China,
                    Fulfilment = model.Fulfilment == "FOC" ? Fulfilment.FOC : Fulfilment.OWN,
                    Role = "Guest",
                    CreatedDate = DateTime.UtcNow,
                    ModifiedDate = DateTime.UtcNow,
                    SlipImage = await SaveSlipImage(model.SlipImage),
                    QuotationStatus = QuotationStatus.Pending,
                    IsVerified = false
                };

                var result = await _wholesaleService.AddNewWholesale(quotation);

                if (result && model.ProductImages != null)
                {
                    foreach (var image in model.ProductImages)
                    {
                        var productImage = new QuotationsProductImages
                        {
                            QuotationId = quotation.QuotationId,
                            Link = await SaveImage(image)
                        };
                       await _context.QuotationsProductImages.AddAsync(productImage);
                       await _context.SaveChangesAsync();
                    }
                }

                if (result)
                {
                    TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.QuotationSuccess;
                    return RedirectToAction(Constants.AdminActionMethods.Quotation, Constants.AdminActionMethods.Seller);
                }
                else
                {
                    TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.SomethingWrong;
                    ModelState.AddModelError(string.Empty, "Something went wrong, please try later.");
                }
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult SaveQuote(int productId, int budget, int quantity, string sourcingSelection, string fulfilmentSelection)
        {
            var user = _currentUser;
            string userId = user.Id;
            var wizard = _profileService.GetProfileById(userId);
            var product = _productsService.GetById(productId);
            if (user != null)
            {
                Sourcing sourcing = sourcingSelection == "Local" ? Sourcing.Local : Sourcing.China;
                Fulfilment fulfilment = fulfilmentSelection == "FOC" ? Fulfilment.FOC : Fulfilment.OWN;
                var productName = product.Name;

                var quotationData = new Quotations
                {
                    Fullname = user.Fullname,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    WhatsappPhoneNumber = user.PhoneNumber,
                    ProductInformation = productName,
                    BudgetInUSD = Convert.ToDecimal(budget),
                    Quantity = quantity,
                    Sourcing = sourcing,
                    Fulfilment = fulfilment,
                    HomeCity = wizard.Address,
                    WebsiteURL = wizard.StoreURL,
                    Role = "Seller",
                    CreatedDate = DateTime.Now, 
                    ModifiedDate = DateTime.Now, 
                   
                };
                _context.Quotations.Add(quotationData);
                _context.SaveChanges(); 
            }
            TempData["notificationMessage"] = "Your request has been submitted successfully!";
            return RedirectToAction("Shop", "Seller");
        }
        

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> MyProducts([FromQuery] Pagination pagination)
        {
            try
            {
                var result = await _inventoryService.GetInventoryListByUserId(_currentUser.Id);
                var count = result.Count();
                result = result.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToList();
                return View("MyProducts", new PageResponse<List<ViewMyProductModel>>(result, pagination.PageNumber, pagination.PageSize, count));
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.SellerDashboard, Constants.AdminActionMethods.Seller);
            }
        }

        [HttpGet]
        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> ViewAssignHistory(int assignId)
        {
            var result = await _inventoryService.GetInventoryHistoryByAssignId(assignId);
            return PartialView("_inventoryHistory", result);
        }

        [OnlySellerWithCompleteProfile]
        public async Task<IActionResult> MyProduct(int id)
        {
            try
            {
                Product? product = _productsService.GetById(id);
                var quantityIsAssigned = await _context.InventoryAssigns.FirstOrDefaultAsync(x => x.ProductId == id && x.UserId == _currentUser.Id);
                if(quantityIsAssigned == null || quantityIsAssigned.Quantity <= 0)
                {
                    TempData["AddToCart"] = false;
                    return View(new ProductPageViewModel { Product = product });
                }
                TempData["AddToCart"] = true;
                return View(new ProductPageViewModel { Product = product });
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Shop, Constants.AdminActionMethods.Seller);
            }
        }

        public IActionResult GetMarqueeData()
        {
            string marqueeData = News();
            return Json(marqueeData);
        }
        #region Reports
        [OnlySellerWithCompleteProfile]
        public IActionResult Reports([FromQuery] Pagination pagination)
        {
            try
            {
                var pageResponse = _reportService.GetReportOfSeller(_currentUser, pagination);
                return View("Reports", pageResponse);
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.FailedToLogout;
                return RedirectToAction(Constants.AdminActionMethods.SellerDashboard, Constants.AdminActionMethods.Seller);
            }
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult ViewReport(int withdrawalId)
        {
            var orderDetail = _orderService.GetOrderByWithdraalId(withdrawalId);
            return PartialView("_TransactionDetailsPartialView", orderDetail);
        }

        [OnlySellerWithCompleteProfile]
        public IActionResult GenerateInvoice(int withdrawalId)
        {
            var invoiceData = _reportService.GetTransactionDetail(withdrawalId, _currentUser);
            return PartialView("_RoportInvoice", invoiceData);
        }
        #endregion

        public IActionResult TestingView()
        {
            return View();
        }
        public IActionResult ProductionErrorView()
        {
            return View();
        }

        #region Private Method


        private async Task<string> SaveImage(IFormFile imageFile)
        {
            var imagesDirectory = Path.Combine(_env.WebRootPath, "images", "wholesaleproduct");
            if (!Directory.Exists(imagesDirectory))
            {
                Directory.CreateDirectory(imagesDirectory);
            }

            string extension = Path.GetExtension(imageFile.FileName);
            var guid = Guid.NewGuid().ToString();
            var uniqueFileName = $"WholesaleProduct_{guid}{extension}";
            var filePath = Path.Combine(imagesDirectory, uniqueFileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }

            return Path.Combine("images", "wholesaleproduct", uniqueFileName.Replace(" ", "_"));
        }

        private async Task<string> SaveSlipImage(IFormFile imageFile)
        {
            var imagesDirectory = Path.Combine(_env.WebRootPath, "images", "Slips");
            if (!Directory.Exists(imagesDirectory))
            {
                Directory.CreateDirectory(imagesDirectory);
            }
            string extension = Path.GetExtension(imageFile.FileName);
            var guid = Guid.NewGuid().ToString();
            var uniqueFileName = $"Slip_{guid}{extension}";
            var filePath = Path.Combine(imagesDirectory, uniqueFileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }

            return Path.Combine("images", "Slips", uniqueFileName.Replace(" ", "_"));
        }

        private async Task UpdateProductQuantity(string email)
        {
            var ordersList = await _orderService.GetCourierOrderIdsWithStatusByEmail(email);
            
            if (ordersList != null && ordersList.Count > 0)
            {
                foreach (var order in ordersList)
                {
                    var ordersItems = _orderItemService.GetOrderItemsById(order.Id);
                    if (ordersItems != null)
                    {
                        foreach(var orderItem in ordersItems)
                        {
                            if (order.OrderStatus == OrderStatus.ReturnedToShipper ||
                                order.OrderStatus == OrderStatus.Returned ||
                                order.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                                order.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                                order.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                                order.OrderStatus == OrderStatus.ReturnConfirm ||
                                order.OrderStatus == OrderStatus.Inward ||
                                order.OrderStatus == OrderStatus.Cancelled)
                            {
                                await _inventoryService.IncreaseQuantity(_currentUser.Id, orderItem.ProductId, orderItem.Quantity);
                            }
                        }
                    }
                }
            }
        }

        private async Task UpdateOrderStatuses(string email)
        {
            var ordersList = await _orderService.GetCourierOrderIdsWithStatusByEmail(email);
            if (ordersList != null && ordersList.Count > 0)
            {
                foreach (var order in ordersList)
                {
                    if (order.CourierServiceType == CourierServiceType.Leopard)
                    {
                        var result = await _courierService.UpdateLeopardOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                    else if (order.CourierServiceType == CourierServiceType.Trax)
                    {
                        var result = await _courierService.UpdateTraxOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                    else if (order.CourierServiceType == CourierServiceType.Leopard)
                    {
                        var result = await _courierService.UpdateDaewooOrderByIdAsync(order.CourierOrderId, order.Id);
                    } 
                    else if(order.CourierServiceType == CourierServiceType.Rider)
                    {
                        var result = await _courierService.UpdateRiderOrderByIdAsync(order.CourierOrderId, order.Id);
                    }
                }
            }
        }
        private string News()
        {
            string news = "News";
            return _focSettingService.GetNews(news);
        }
        private decimal ShippingCost()
        {
            string shippingCost = "Shipping Cost";
            return Convert.ToDecimal(_focSettingService.GetShippingCost(shippingCost));
        } 
        #endregion
    }
}
