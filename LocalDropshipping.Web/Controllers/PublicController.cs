﻿using LocalDropshipping.Web.Helpers.Constants;
using LocalDropshipping.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace LocalDropshipping.Web.Controllers
{
    public class PublicController : Controller
    {
        #region About Us
        public IActionResult AboutUs()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion

        #region Contact Us
        public IActionResult ContactUs()
        {
            try
            {
                return View();

            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion

        #region Terms and Conditions
        public IActionResult TermAndCondition()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion

        #region PrivacyPolicy
        public IActionResult PrivacyPolicy()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion

        #region FAQs
        public IActionResult Faq()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion

        #region Error
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion

        #region LandingPage
        public IActionResult LandingPage()
        {
            return View();
        }
        #endregion


        #region Local Sourcing
        public IActionResult LocalSourcing()
        {
            return View();
        }

        #endregion

        #region CHina Sourcing
        public IActionResult ChinaSourcing()
        {
            return View();
        }
        #endregion

        #region
        public IActionResult FulFillment()
        {
            return View();
        }


        #endregion



        #region AuthrizationFailed
        public IActionResult AuthorizationFailed()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion

        #region GuideLines
        public IActionResult GuideLines()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                TempData[Constants.AdminTempData.NotificationMessage] = Constants.ErrorMessage.CommonErrorMessage;
                return RedirectToAction(Constants.AdminActionMethods.Register, Constants.AdminActionMethods.Seller);
            }
        }
        #endregion
    }
}
