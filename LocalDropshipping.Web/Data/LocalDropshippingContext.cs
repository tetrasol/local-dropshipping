﻿using LocalDropshipping.Web.Data.Entities;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LocalDropshipping.Web.Data
{
    public class LocalDropshippingContext : IdentityDbContext<User>, IDataProtectionKeyContext
    {
        public LocalDropshippingContext(DbContextOptions<LocalDropshippingContext> options) : base(options)
        { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<WishList> WishList { get; set; }
        public DbSet<Withdrawals> Withdrawals { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Profiles> Profiles { get; set; }
        public DbSet<ProductVariant> ProductVariants { get; set; }
        public DbSet<ProductVariantImage> ProductVariantImages { get; set; }
        public DbSet<ProductVariantVideo> ProductVariantVideos { get; set; }
        public DbSet<Consumer> Consumers { get; set; }
        public DbSet<FocSetting> FocSettings { get; set; }
        public DbSet<Quotations> Quotations { get; set; }
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<QuotationsProductImages> QuotationsProductImages { get; set; }
        public DbSet<InventoryAssign> InventoryAssigns { get; set; }
        public DbSet<InventoryHistory> InventoryHistories { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Profiles>()
                .HasMany(p => p.Wallets)
                .WithOne(w => w.Profile)
                .HasForeignKey(w => w.ProfileId);
        }
    }
}
