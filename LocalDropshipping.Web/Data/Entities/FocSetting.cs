﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Data.Entities
{
    
    public class FocSetting
    {
        [Key]
        public string? Name { get; set; }
        public string? Value { get; set; }
        public string Description { get; set; }
    }
}
