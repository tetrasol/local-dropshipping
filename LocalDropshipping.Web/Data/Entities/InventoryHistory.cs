﻿using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Data.Entities
{
    public class InventoryHistory
    {
        [Key]
        public int Id { get; set; }
        public int AssignInventoryId { get; set; }
        public string ProductName { get; set; }
        public string StoreName { get; set; }
        public int AssignQuantity { get; set; }
        public int StockQuantity { get; set; }
        public decimal Price { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string Comments { get; set; }
    }
}
