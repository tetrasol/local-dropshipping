﻿using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Data.Entities
{
    public class QuotationsProductImages
    {
        [Key]
        public int Id { get; set; }
        public int QuotationId { get; set; }
        public string Link { get; set; }
    }
}
