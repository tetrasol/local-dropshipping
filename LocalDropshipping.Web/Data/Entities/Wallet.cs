﻿using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Data.Entities
{
    public class Wallet
    {
        [Key]
        public int WalletId { get; set; } 
        public string Email { get; set; }
        public decimal Amount { get; set; }
        public int ProfileId { get; set; }
        public virtual Profiles Profile { get; set; }
    }
}
