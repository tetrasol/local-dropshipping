﻿using LocalDropshipping.Web.Enums;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;

namespace LocalDropshipping.Web.Data.Entities
{
    public class Quotations
    {
        [Key]
        public int QuotationId { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string WhatsappPhoneNumber { get; set; }
        public string HomeCity { get; set; }
        public string ProductInformation { get; set; }
        public decimal BudgetInUSD { get; set; } 
        public string WebsiteURL { get; set; }
        public bool IsVerified { get; set; }
        public Sourcing Sourcing { get; set; }
        public Fulfilment Fulfilment { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public QuotationStatus QuotationStatus { get; set; }
        public bool IsDeleted { get; set; }
        public string SlipImage { get; set; }
        public int Quantity { get; set; }
        public string Role { get; set; }
    }
}
