﻿namespace LocalDropshipping.Web.Dtos
{
    public class AdminDashboardDto
    {
        public int TotalOrders { get; set; }
        public int TotalSellers { get; set; }
        public int TotalPendingOrders { get; set; }
        public int TotalActiveSubscriber { get; set; }
        public int TotalPendingWithdrawalRequests { get; set; }
        public int OrdersInTransit { get; set; }
        public int ReturnedOrders { get; set; }
        public int? TotalWholeSellers { get; set; }
        public int TotalDeliveredOrders { get; set; }
        public int TotalCancelledOrders { get; set; }
        public decimal TotalPaidAmount { get; set; }
        public decimal TotalSales { get; set; }
        public decimal TotalUnpaidAmount { get; set; }
    }
}
