﻿namespace LocalDropshipping.Web.Helpers.Inventory
{
    public static class ShippingCostCalculator
    {
        public static decimal CalculateShippingCost(decimal? weight, decimal shippingCost)
        {
            if (weight >= 0.1m && weight < 1.0m)
            {
                return shippingCost;
            }
            else if (weight >= 1.0m && weight < 2.0m)
            {
                return shippingCost * 2;
            }
            else if (weight >= 2.0m && weight < 3.0m)
            {
                return shippingCost * 3;
            }
            else if (weight >= 3.0m && weight < 4.0m)
            {
                return shippingCost * 4;
            }
            else if (weight >= 4.0m && weight < 5.0m)
            {
                return shippingCost * 5;
            }
            else
            {
                return shippingCost * 6;
            }
        }
    }
}
