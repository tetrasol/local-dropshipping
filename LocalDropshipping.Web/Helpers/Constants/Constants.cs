﻿namespace LocalDropshipping.Web.Helpers.Constants
{
    public static class Constants
    {
        public static class ErrorMessage
        {
            public const string EmailAlreadyRegister = "This email address is already register";
            public const string EnterRegisterEmail = "Please enter your register email";
            public const string VerifyEmail = "Please verify your email";
            public const string PasswordNotMatch = "Your Password does not match";
            public const string EnterSendCode = "Please Enter the code we have sent to your email";
            public const string CodeAlreadyRegister = "Your verification code is already verified";
            public const string VerificationCodeExpired = "Your verification code is expired";
            public const string EmailAlreadyVerified = "Your email is already verified";
            public const string LinkIsExpired = "Your link is expired. Please create a new link";
            public const string UnableToFindUser = "Unable to find user";
            public const string TryAgain = "Please try again";
            public const string InvalidCode = "Invalid code";
            public const string InvalidRequest = "Invalid Request";
            public const string _400 = "You have made Bad Request";
            public const string _404 = "resources not found";
            public const string _500 = "server error";
            public const string _401 = "You are not authorized";
            public const string ErrorDuringMigration = "An error occured during migration";
            public const string UserNotFound = "User Not Found!Please create new account";
            public const string FailedToUpdateOrder = "Failed to update order please try later...";
            public const string OrderNotFound = "Something went wrong in order list";
            public const string AddNewCategory = "Something went wrong while adding category";
            public const string AdminLogin = "Something went wrong please try later...";
            public const string FailedToAddUser = "Failed to add user please try later...";
            public const string FailedToDeleteUser = "Failed to delete user please try later...";
            public const string FailedToActivateUser = "Failed to Active / Disable User please try later...";
            public const string FailedToLogout = "Failed to logout please try later...";
            public const string FailedToAddProduct = "Failed to Add product please try later...";
            public const string FailedTowithdrawal = "Failed to withdrawal please try later...";
            public const string FailedToAddCategory = "Failed to add category please try later...";
            public const string FailedToDeleteCategory = "Failed to Delete category please try later...";
            public const string FailedToUpdateCategory = "Failed to Update category please try later...";
            public const string FailedToBlockOrUnblockConsumer = "Failed to Block / UnblockConsumer please try later...";
            public const string CommonErrorMessage = "Something went wrong please try later...";
            public const string FailedtoUpdatePassword = "Failed to update password please try later...";
            public const string Wishlist = "Failed to add item in wishlist please try later...";
            public const string FailedToVerifyProfile = "Failed to verify your profile please try later...";
            public const string FailedToPlaceOrder = "Failed to place your order please try later...";
            public const string FailedToUpdateProfile = "Failed to update your profile please try later...";
            public const string FailedToLogin = "Failed to login profile please try later...";
            public const string FailedToRegister = "Failed to register your account please try later...";
            public const string NoVideosForThisVariant = "Video(s) for this variant have not been uploaded yet";
            public const string NoImagesForThisVariant = "Image(s) for this variant have not been uploaded yet";
            public const string FailedToLoadOrder = "Your provided number is in our blocked list. Please try with another contact number.";
            public const string FailedToWithdrawal = "You are not eligible for withdrawals";
            public const string NoOrdersFound = "No Orders were found";
            public const string ItemAddToCart = "Product has been added into cart!";//"Product quantity updated!";
            public const string ItemRemovedFromCart = "Product has been removed from cart!";//"Product quantity updated!";
            public const string ItemRemoveFromWishlist = "Product has been removed from Wishlist!";
            public const string ItemAddToWishlist = "Product has been Added into Wishlist!";
            public const string FailedToAddWallet = "Failed to add wallet";
            public const string FailedToLoadWholesaleDetail = "Failed to load wholesale detail";
            public const string FailedToDownloadSlip = "Failed to download slip";
            public const string NoWholesaleFound = "No Quotations were found";
            public const string UpdateWholesale = "Status Updated Successfully";
            public const string FailedToUpdateWholesale = "Failed To Updated  Status";
            public const string QuotationSuccess = "Your query has been submitted successfully!";
            public const string SomethingWrong = "Something went wrong.try again later.";
            public const string AssignSuccessfully = "Product Assign Successfully";
            public const string ZeroProductRemaining = "You have reached to maximum quantity!";
            public const string MaxQuantityReached = "You have reached to maximum quantity!";
        }

        public static class AdminActionMethods
        {
            //Controllers
            public const string Admin = "Admin";
            public const string Seller = "Seller";
            //Seller Action Methods 
            public const string Shop = "Shop";
            public const string ContactUs = "ContactUs";
            public const string UpdatePassword = "UpdatePassword";
            public const string SellerLogin = "Login";
            public const string SellerDashboard = "SellerDashboard";
            public const string Wishlist = "wishlist";
            public const string ProfileVerification = "ProfileVerification";
            public const string PlaceOrder = "PlaceOrder";
            public const string Checkout = "Checkout";
            public const string Profile = "Profile";
            public const string UpdateSellerPassword = "UpdateSellerPassword";
            public const string ProductionErrorView = "ProductionErrorView";
            public const string Register = "Register";
            public const string SellerOrders = "SellerOrders";
            public const string Quotation = "Quotation";


            //Admin Action Methods
            public const string OrdersList = "OrdersList";
            public const string Dashboard = "Dashboard";
            public const string AdminLogin = "Login";
            public const string AddNewUser = "AddNewUser";
            public const string Withdrawal = "Withdrawal";
            public const string AddNewCategory = "AddNewCategory";
            public const string UpdateCategory = "UpdateCategory";
            public const string GetAllConsumers = "GetAllConsumers";
            public const string AddUpdateProduct = "AddUpdateProduct";
            public const string GetAllSellers = "GetAllSellers";
            public const string ForgotPassword= "ForgotPassword";
            public const string Wallet = "Wallet";
            public const string QuotationList = "QuotationList";
        }
        public static class AdminTempData
        {
            public const string NotificationMessage = "notificationMessage";
        }
        public static class ContentTypeFiles
        {
            public const string ApplicationJson = "application/json";
            public const string TextCsv = "text/csv";

        }
		public static class Configuration
		{
			public const string webUrl = "webUrl";
		}
        public static class FOCSettings
        {
            public const string PaymentLimit = "Payment Limit";
        }
        public static class NotificationSuccessMessage
        {
            public const string OrdersUpdateSuccessfully = "Status Updated Successfully.";
            public const string WalletAddedSuccefully = "Wallet added successfully!";
        }
    }
}
