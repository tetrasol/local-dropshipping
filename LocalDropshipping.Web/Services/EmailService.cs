﻿using LocalDropshipping.Web.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace LocalDropshipping.Web.Services
{
	public class EmailService : IEmailService
	{
		private const string _templateBasePath = @"Templates/EmailTemplate/{0}.html";
		private readonly SMTPConfigModel _smtpconfig;
		private readonly IWebHostEnvironment _webHostEnvironment;

		public EmailService(IOptions<SMTPConfigModel> smtpconfig, IWebHostEnvironment webHostEnvironment)
		{
			_smtpconfig = smtpconfig.Value;
			_webHostEnvironment = webHostEnvironment;
		}

		public async Task ReceiveEmail(EmailMessage userEmailOptions)
		{
			try
			{
				var mailMessage = new MimeMessage();
				mailMessage.From.Add(MailboxAddress.Parse(_smtpconfig.From));

				mailMessage.To.Add(MailboxAddress.Parse(_smtpconfig.From));
				mailMessage.Subject = userEmailOptions.Subject;

				var template = GetTemplate(userEmailOptions.TemplatePath);
				mailMessage.Body = new TextPart(TextFormat.Html) { Text = UpdatePlaceHolders(template, userEmailOptions.Placeholders) };

				using var smtp = new SmtpClient();
				smtp.CheckCertificateRevocation = false;

				smtp.Connect(_smtpconfig.SmtpServer, _smtpconfig.Port, SecureSocketOptions.Auto);
				smtp.Authenticate(_smtpconfig.UserName, _smtpconfig.Password);
				await smtp.SendAsync(mailMessage);
				smtp.Disconnect(true);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		public async Task SendEmail(EmailMessage userEmailOptions)
		{
			try
			{
				var mailMessage = new MimeMessage();
				mailMessage.From.Add(MailboxAddress.Parse(_smtpconfig.From));

				mailMessage.To.Add(MailboxAddress.Parse(userEmailOptions.ToEmail));
				mailMessage.Subject = userEmailOptions.Subject;

				var template = GetTemplate(userEmailOptions.TemplatePath);
				mailMessage.Body = new TextPart(TextFormat.Html) { Text = UpdatePlaceHolders(template, userEmailOptions.Placeholders) };

				using var smtp = new SmtpClient();
				smtp.CheckCertificateRevocation = false;

				smtp.Connect(_smtpconfig.SmtpServer, _smtpconfig.Port, SecureSocketOptions.Auto);
				smtp.Authenticate(_smtpconfig.UserName, _smtpconfig.Password);
				await smtp.SendAsync(mailMessage);
				smtp.Disconnect(true);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		private string GetTemplate(string templateName)
		{
			string wwwrootPath = _webHostEnvironment.WebRootPath; 

			string relativeFilePath = string.Format(_templateBasePath, templateName); 

			string fullPath = Path.Combine(wwwrootPath, relativeFilePath); 

			var template = File.ReadAllText(fullPath);

			return template;
		}

		private string UpdatePlaceHolders(string template, List<KeyValuePair<String,String>> keyValuePairs)
		{
			if (!string.IsNullOrEmpty(template) && keyValuePairs != null)
			{
				foreach (var placeholder in keyValuePairs) 
				{
					if (template.Contains(placeholder.Key))
					{
						template = template.Replace(placeholder.Key, placeholder.Value);

					}
				}
			}
			return template;
		}  

	}
}
