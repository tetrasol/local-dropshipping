﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Helpers;

namespace LocalDropshipping.Web.Services
{
    public interface IWholesaleService
    {
        Task<List<Quotations>> GetAllWholesales();
        Task<bool> AddNewWholesale(Quotations consumer);
        Task<Quotations> GetById(int id);
        Task<bool> Delete(int wholesaleId);
        Task<List<Quotations>> GetPendingWholesales();
        Task<List<Quotations>> GetProcessedWholesales();
        List<Quotations> GetFilteredWholesales(Pagination pagination);
        List<Quotations> GetWholesaleByDaterange(DateTime start, DateTime end);

        Task<bool> UpdateStatus(int wholesaleId, string Status);
    }
}
