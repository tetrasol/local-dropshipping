﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Dtos;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Helpers.Inventory;
using LocalDropshipping.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace LocalDropshipping.Web.Services
{
    public class OrderService : IOrderService
    {
        private readonly LocalDropshippingContext _context;
        private readonly IFocSettingService _focSettingService;
        private readonly ICourierService _courierService;
        private readonly IWithdrawalService _withdrawalService;
        private readonly IConsumerService _consumerService;
        private readonly IOrderItemService _orderItemService;
        private readonly IUserService _userService;
        private readonly IWalletService _walletService;
        private readonly IInventoryService _inventoryService;
        private readonly IProductsService _productsService;
        private readonly IProductVariantService _productVariantService;
        private readonly UserManager<User> _userManager;

        public OrderService(LocalDropshippingContext context, IFocSettingService focSettingService, ICourierService courierService, IWithdrawalService withdrawalService,IConsumerService consumerService,IOrderItemService orderItemService,IUserService userService,IWalletService walletService,IInventoryService inventoryService,IProductsService productsService,IProductVariantService productVariantService, UserManager<User> userManager)
        {
            _context = context;
            _focSettingService = focSettingService;
            _courierService = courierService;
            _withdrawalService = withdrawalService;
            _consumerService = consumerService;
            _orderItemService = orderItemService;
            _userService = userService;
            _walletService = walletService;
            _inventoryService = inventoryService;
            _productsService = productsService;
            _productVariantService = productVariantService;
            _userManager = userManager;
        }

        public async Task<Order> AddOrder(List<OrderItem> cart, string email, decimal sellPrice, decimal GrandTotal)
        {

            Order order = new Order()
            {
                Id = GenerateOrderId(),
                Seller = email,
                CreatedDate = DateTime.Now,
                CreatedBy = email,
                GrandTotal = (Convert.ToDecimal(cart.Sum(s => s.Quantity * s.Price)) + GrandTotal),
                OrderStatus = OrderStatus.Pending,
                SellPrice = sellPrice,
                PaymentStatus = PaymentStatus.UnPaid
            };
            _context.Orders.Add(order);
            _context.SaveChanges();

            foreach (var items in cart)
            {
                var orderItems = new OrderItem()
                {
                    OrderId = order.Id,
                    ProductId = items.ProductId,
                    Image = items.Image,
                    Name = items.Name,
                    Price = items.Price,
                    Quantity = items.Quantity,
                    SubTotal = items.Quantity * items.Price,
                    CreatedDate = DateTime.Now,
                    CreatedBy = email
                };
                var user = _userService.GetByEmail(email);
                _context.OrderItems.Add(orderItems);
            }
            _context.SaveChanges();
            return order;
        }

        public Order? Delete(int orderId)
        {
            var order = _context.Orders.FirstOrDefault(x => x.Id == orderId);
            if (order != null)
            {
                order.IsDeleted = true;
                _context.SaveChanges();
            }
            return order;
        }

        public List<Order> GetAll()
        {
            var orderlist = new List<Order>();
            orderlist = _context.Orders.Where(x => x.IsDeleted == false).ToList();
            if (orderlist != null)
            {
                return orderlist;
            }
            return new List<Order>();
        }

        public Order? GetById(int orderId)
        {
            return _context.Orders.FirstOrDefault(x => x.Id == orderId);
        }

        public List<Order> GetByEmail(string userEmail)
        {
            return _context.Orders.Where(x => x.Seller == userEmail && x.IsDeleted == false).ToList();
        }

        public Order? Update(int orderid, OrderDto order)
        {
            var exOrder = _context.Orders.FirstOrDefault(x => x.Id == orderid);
            if (exOrder != null)
            {
                exOrder.OrderStatus = order.Status;
                exOrder.SpecialInstructions = order.SpecialInstructions;
                exOrder.GrandTotal = order.GrandTotal;
                _context.SaveChanges();
            }
            return exOrder;
        }

        public List<Order> GetOrdersByStatus(OrderStatus status)
        {
            return _context.Orders.Where(x => x.OrderStatus == status).ToList();
        }

        public int CountOrdersByStatus(OrderStatus status)
        {
            return _context.Orders.Count(x => x.OrderStatus == status);
        }

        Order? IOrderService.Add(Order order)
        {
            throw new NotImplementedException();
        }
      
        public async Task<SellerOrdersViewModel> GetOrdersProfit(string email)
        {
            SellerOrdersViewModel model = new SellerOrdersViewModel();
            var user = await _context.Users.Where(x => x.Email == email).FirstOrDefaultAsync();
            decimal profit = 0;
            decimal cost = 0;
            var orders = _context.Orders.Where(o => o.Seller == email && o.PaymentStatus == PaymentStatus.UnPaid && (
            o.OrderStatus == OrderStatus.Delivered || 
            o.OrderStatus == OrderStatus.ShipmentDelivered ||
            o.OrderStatus == OrderStatus.ShipmentPicked || 
            o.OrderStatus == OrderStatus.DliveredAtHome ||
            o.OrderStatus == OrderStatus.ShipmentOnHoldForSelfCollection ||
            o.OrderStatus == OrderStatus.Collected)).ToList();
            if (orders.Any())
            {
                profit = orders.Where(o => o.PaymentStatus == PaymentStatus.UnPaid).Sum(o => o.SellPrice - o.GrandTotal);
            }
            var returnedOrders = _context.Orders.Where(o => o.Seller == email && o.PaymentStatus == PaymentStatus.UnPaid && (
                o.OrderStatus == OrderStatus.ReturnedToShipper ||
                o.OrderStatus == OrderStatus.ReadyForReturn ||
                o.OrderStatus == OrderStatus.BeingReturn ||
                o.OrderStatus == OrderStatus.ShipmentDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ShipmentNonServiceArea ||
                o.OrderStatus == OrderStatus.ShipmentReturnConfirmationPending ||
                o.OrderStatus == OrderStatus.ReturnConfirm ||
                o.OrderStatus == OrderStatus.ReturnInTransit ||
                o.OrderStatus == OrderStatus.ReturnArrivedAtOrigin ||
                o.OrderStatus == OrderStatus.ReturnDispatched ||
                o.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ReturnDeliveredToShipper ||
                o.OrderStatus == OrderStatus.ReturnNotAttempted ||
                o.OrderStatus == OrderStatus.ReturnOnHold ||
                o.OrderStatus == OrderStatus.ReturnDispatched ||
                o.OrderStatus == OrderStatus.ReturnedUnableToReturn ||
                o.OrderStatus == OrderStatus.ReturnNotShifted ||
                o.OrderStatus == OrderStatus.ReturnRiderExchange ||
                o.OrderStatus == OrderStatus.ReplacementInTransit ||
                o.OrderStatus == OrderStatus.ReplacementArrivedAtOrigin ||
                o.OrderStatus == OrderStatus.ReplacementDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ReplacementDeliveredToShipper ||
                o.OrderStatus == OrderStatus.ReplacementRiderExchange ||
                o.OrderStatus == OrderStatus.ReplacementNotCollected ||
                o.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                o.OrderStatus == OrderStatus.RefusedDelivery ||
                o.OrderStatus == OrderStatus.ReturnToShipper ||
                o.OrderStatus == OrderStatus.ReturnToOrigin ||
                o.OrderStatus == OrderStatus.HoldForCollection ||
                o.OrderStatus == OrderStatus.ReturnToShipper ||
                o.OrderStatus == OrderStatus.QCRejectedHub ||
                o.OrderStatus == OrderStatus.AwaitingReturn ||
                o.OrderStatus == OrderStatus.Returned ||
                o.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                o.OrderStatus == OrderStatus.ReturnInProgress ||
                o.OrderStatus == OrderStatus.ReturnAttemptFailed ||
                o.OrderStatus == OrderStatus.ReturnInTransit ||
                o.OrderStatus == OrderStatus.TransitToHubReturn ||
                o.OrderStatus == OrderStatus.AwaitingLinehaulReturn)).ToList();
            cost = ShipmentPackagingCharges(returnedOrders);
            profit -= cost;

            profit -= _withdrawalService.GetAllWithdrawalAmount(email);

            decimal walletAmount = await _walletService.GetTotalWalletAmount(email);

            profit += walletAmount;
            model.PendingOrders = await PendingOrders(email);
            model.TotalDeliveredOrders = TotalDeliveredOrders(email);
            model.TotalSales = TotalSales(email);
            model.TotalReturnedOrders = await GetReturenedOrderCount(email);
            model.TotalOrders = await GetOrderByEmail(email);
            model.AmountWithdrawal = _withdrawalService.GetAllPaidWithdrawal(email);
            model.AvailableBalance = profit;
            model.UnpaidEarnings = _withdrawalService.GetAllWithdrawalAmount(email);
            model.MyWallet = walletAmount;
            model.CancelledOrders = await GetCancelledOrdersByEmail(email);
            if (model.AvailableBalance <= -600 && user.IsActive)
            {
                var result = _userService.DeactivateUser(email, model.AvailableBalance);
                await _userManager.UpdateSecurityStampAsync(user);
            }
            return model;
        }


        private int GenerateOrderId()
        {
            int orderId;
            do
            {
                Random random = new Random();
                orderId = random.Next(10000000, 99999999);
            }
            while (GetById(orderId) != null ? true : false);

            return orderId;
        }

        public async Task<Order> UpdateOrder(OrderViewModel orderViewModel)
        {
            Order result = (from p in _context.Orders
                                  where p.Id == orderViewModel.OrderId
                                  select p).SingleOrDefault();
            result.OrderStatus = orderViewModel.OrderStatus;
            result.CourierServiceType = orderViewModel.CourierServiceType;
            result.SpecialInstructions = orderViewModel.SpecialInstructions;
            result.UpdatedDate = DateTime.UtcNow;
            result.UpdatedBy = orderViewModel.UpdatedBy;
            result.CourierOrderId = orderViewModel.CourierOrderId;
            _context.SaveChanges();
            if(orderViewModel.OrderStatus != OrderStatus.Cancelled && orderViewModel.CourierServiceType != CourierServiceType.NotSelected)
            {
                var update = await UpdateOrderTracking(orderViewModel);
            }
            await IncreasedQunatityInCaseOfReturn(result);
            return result;
        }
        private decimal ShippingCost()
        {
            string shippingCost = "Shipping Cost";
            return Convert.ToDecimal(_focSettingService.GetShippingCost(shippingCost));
        }

        private decimal ShipmentPackagingCharges(List<Order> returnedOrders)
        {
            decimal? totalWeight = 0;
            decimal serviceCharges = 0;
            decimal shippingCharges = 0;

            foreach (var order in returnedOrders)
            {
                var orderItems = _orderItemService.GetOrderItemsById(order.Id);
                foreach (var orderItem in orderItems)
                {
                    Product product = _productsService.GetById(orderItem.ProductId);
                    totalWeight += product.Weight * orderItem.Quantity;

                    ProductVariant productVariant = _productVariantService.GetByProductIdAsync(orderItem.ProductId);
                    serviceCharges += productVariant.DiscountedPrice * orderItem.Quantity;
                }
            }
            decimal shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : 0m;
            shippingCharges = shipping;
            serviceCharges = serviceCharges * 0.1m;
            return shippingCharges + serviceCharges;
        }


        private decimal ShipmentPackagingChargesPerOrder(Order returnedOrders)
        {
            decimal? totalWeight = 0;
            decimal serviceCharges = 0;
            decimal shippingCharges = 0;

                var orderItems = _orderItemService.GetOrderItemsById(returnedOrders.Id);
                foreach (var orderItem in orderItems)
                {
                    Product product = _productsService.GetById(orderItem.ProductId);
                    totalWeight += product.Weight * orderItem.Quantity;

                    ProductVariant productVariant = _productVariantService.GetByProductIdAsync(orderItem.ProductId);
                    serviceCharges += productVariant.DiscountedPrice * orderItem.Quantity;
                }

            decimal shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : 0m;
            shippingCharges = shipping;
            serviceCharges = serviceCharges * 0.1m;
            return shippingCharges + serviceCharges;
        }


        public string[] GetCourierOrderIdsByEmail(string email)
        {
            var orders = GetByEmail(email);

            if (orders != null)
            {
                string[] courierOrderIds = orders.Select(order => order.CourierOrderId).ToArray();
                return courierOrderIds;
            }
            return new string[0]; 
        }
        public async Task<List<Order>> GetCourierOrderIdsWithStatusByEmail(string email)
        {
            var orders = await _context.Orders.Where(x => x.Seller == email && x.PaymentStatus == PaymentStatus.UnPaid || x.PaymentStatus == PaymentStatus.Processing).ToListAsync();
            var filteredOrders = orders.Where(x => !IsExcludedStatus(x.OrderStatus)).ToList();
            return filteredOrders;
        }
   
        public List<TransactionDetailItemViewModel> GetOrderByWithdraalId(int withdrawalId)
        {
            var withdrawal = _context.Withdrawals.FirstOrDefault(x => x.WithdrawalId == withdrawalId);
            var orders = _context.Orders.Where(x => x.WithdrawalId == withdrawalId).ToList();
            var result = new List<TransactionDetailItemViewModel>();

            foreach (var order in orders)
            {
                var orderItems = _context.OrderItems.Where(x => x.OrderId == order.Id).ToList();
                decimal cost = 0;
                //if (order.OrderStatus == OrderStatus.ReturnedToShipper ||
                //order.OrderStatus == OrderStatus.ReadyForReturn ||
                //order.OrderStatus == OrderStatus.BeingReturn ||
                //order.OrderStatus == OrderStatus.ShipmentDeliveryUnsuccessfull ||
                //order.OrderStatus == OrderStatus.ShipmentNonServiceArea ||
                //order.OrderStatus == OrderStatus.ShipmentReturnConfirmationPending ||
                //order.OrderStatus == OrderStatus.ReturnConfirm ||
                //order.OrderStatus == OrderStatus.ReturnInTransit ||
                //order.OrderStatus == OrderStatus.ReturnArrivedAtOrigin ||
                //order.OrderStatus == OrderStatus.ReturnDispatched ||
                //order.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                //order.OrderStatus == OrderStatus.ReturnDeliveredToShipper ||
                //order.OrderStatus == OrderStatus.ReturnNotAttempted ||
                //order.OrderStatus == OrderStatus.ReturnOnHold ||
                //order.OrderStatus == OrderStatus.ReturnDispatched ||
                //order.OrderStatus == OrderStatus.ReturnedUnableToReturn ||
                //order.OrderStatus == OrderStatus.ReturnNotShifted ||
                //order.OrderStatus == OrderStatus.ReturnRiderExchange ||
                //order.OrderStatus == OrderStatus.ReplacementInTransit ||
                //order.OrderStatus == OrderStatus.ReplacementArrivedAtOrigin ||
                //order.OrderStatus == OrderStatus.ReplacementDeliveryUnsuccessfull ||
                //order.OrderStatus == OrderStatus.ReplacementDeliveredToShipper ||
                //order.OrderStatus == OrderStatus.ReplacementRiderExchange ||
                //order.OrderStatus == OrderStatus.ReplacementNotCollected ||
                //order.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                //order.OrderStatus == OrderStatus.RefusedDelivery ||
                //order.OrderStatus == OrderStatus.ReturnToShipper ||
                //order.OrderStatus == OrderStatus.ReturnToOrigin ||
                //order.OrderStatus == OrderStatus.HoldForCollection ||
                //order.OrderStatus == OrderStatus.ReturnToShipper ||
                //order.OrderStatus == OrderStatus.QCRejectedHub ||
                //order.OrderStatus == OrderStatus.AwaitingReturn ||
                //order.OrderStatus == OrderStatus.Returned ||
                //order.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                //order.OrderStatus == OrderStatus.ReturnInProgress ||
                //order.OrderStatus == OrderStatus.ReturnAttemptFailed ||
                //order.OrderStatus == OrderStatus.ReturnInTransit ||
                //order.OrderStatus == OrderStatus.TransitToHubReturn ||
                //order.OrderStatus == OrderStatus.AwaitingLinehaulReturn)
                //{
                //    cost = ShipmentPackagingChargesPerOrder(order);
                //}
                foreach (var orderItem in orderItems)
                {

                    var item = new TransactionDetailItemViewModel
                    {
                        TransactionId = withdrawal.TransactionId,
                        OrderId = order.Id,
                        PaymentStatus = withdrawal.PaymentStatus,
                        Price = Convert.ToDecimal(orderItem.Price),
                        SellPrice = CalculateSellPriceForEachOrderItem(orderItem),
                        Profit = CalculatePackagingAndShippingForEachOrderItem(orderItem, order),
                        ProductName = orderItem.Name
                    };
                    result.Add(item);
                }
            }
            return result;
        }

        private decimal CalculateSellPriceForEachOrderItem(OrderItem orderItem)
        {
            decimal serviceCharges = 0;
            decimal? totalWeight = 0;
            decimal shipping = 0;

            Product product = _productsService.GetById(orderItem.ProductId);
            totalWeight += product.Weight * orderItem.Quantity;
            ProductVariant productVariant = _productVariantService.GetByProductIdAsync(orderItem.ProductId);
            serviceCharges += productVariant.DiscountedPrice * orderItem.Quantity;
            shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
            serviceCharges = serviceCharges / 100 * 10 * orderItem.Quantity;
            
            return serviceCharges + shipping + productVariant.DiscountedPrice;
        }

        private decimal CalculatePackagingAndShippingForEachOrderItem(OrderItem orderItem,Order order)
        {
            decimal serviceCharges = 0;
            decimal? totalWeight = 0;
            decimal shipping = 0;
            if (order.OrderStatus == OrderStatus.ReturnedToShipper ||
                order.OrderStatus == OrderStatus.ReadyForReturn ||
                order.OrderStatus == OrderStatus.BeingReturn ||
                order.OrderStatus == OrderStatus.ShipmentDeliveryUnsuccessfull ||
                order.OrderStatus == OrderStatus.ShipmentNonServiceArea ||
                order.OrderStatus == OrderStatus.ShipmentReturnConfirmationPending ||
                order.OrderStatus == OrderStatus.ReturnConfirm ||
                order.OrderStatus == OrderStatus.ReturnInTransit ||
                order.OrderStatus == OrderStatus.ReturnArrivedAtOrigin ||
                order.OrderStatus == OrderStatus.ReturnDispatched ||
                order.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                order.OrderStatus == OrderStatus.ReturnDeliveredToShipper ||
                order.OrderStatus == OrderStatus.ReturnNotAttempted ||
                order.OrderStatus == OrderStatus.ReturnOnHold ||
                order.OrderStatus == OrderStatus.ReturnDispatched ||
                order.OrderStatus == OrderStatus.ReturnedUnableToReturn ||
                order.OrderStatus == OrderStatus.ReturnNotShifted ||
                order.OrderStatus == OrderStatus.ReturnRiderExchange ||
                order.OrderStatus == OrderStatus.ReplacementInTransit ||
                order.OrderStatus == OrderStatus.ReplacementArrivedAtOrigin ||
                order.OrderStatus == OrderStatus.ReplacementDeliveryUnsuccessfull ||
                order.OrderStatus == OrderStatus.ReplacementDeliveredToShipper ||
                order.OrderStatus == OrderStatus.ReplacementRiderExchange ||
                order.OrderStatus == OrderStatus.ReplacementNotCollected ||
                order.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                order.OrderStatus == OrderStatus.RefusedDelivery ||
                order.OrderStatus == OrderStatus.ReturnToShipper ||
                order.OrderStatus == OrderStatus.ReturnToOrigin ||
                order.OrderStatus == OrderStatus.HoldForCollection ||
                order.OrderStatus == OrderStatus.ReturnToShipper ||
                order.OrderStatus == OrderStatus.QCRejectedHub ||
                order.OrderStatus == OrderStatus.AwaitingReturn ||
                order.OrderStatus == OrderStatus.Returned ||
                order.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                order.OrderStatus == OrderStatus.ReturnInProgress ||
                order.OrderStatus == OrderStatus.ReturnAttemptFailed ||
                order.OrderStatus == OrderStatus.ReturnInTransit ||
                order.OrderStatus == OrderStatus.TransitToHubReturn ||
                order.OrderStatus == OrderStatus.AwaitingLinehaulReturn)
            {
                Product product = _productsService.GetById(orderItem.ProductId);
                totalWeight += product.Weight * orderItem.Quantity;
                ProductVariant productVariant = _productVariantService.GetByProductIdAsync(orderItem.ProductId);
                serviceCharges += productVariant.DiscountedPrice * orderItem.Quantity;
                shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
                serviceCharges = serviceCharges / 100 * 10 * orderItem.Quantity;
                var deductcharges = serviceCharges + shipping;
                return - deductcharges;
            }
            else
            {
                Product product = _productsService.GetById(orderItem.ProductId);
                totalWeight += product.Weight * orderItem.Quantity;
                ProductVariant productVariant = _productVariantService.GetByProductIdAsync(orderItem.ProductId);
                serviceCharges += productVariant.DiscountedPrice * orderItem.Quantity;
                shipping = totalWeight != 0 ? ShippingCostCalculator.CalculateShippingCost(totalWeight, ShippingCost()) : ShippingCost();
                serviceCharges = serviceCharges / 100 * 10 * orderItem.Quantity;
                return serviceCharges + shipping;
            }
        }

        private decimal CalculateProfit(decimal grandTotal, decimal sellPrice, decimal cost)
        {
            if(cost != 0)
            {
                return - cost;
            }
            return (sellPrice - grandTotal);
        }

        public OrderListModel GetListOfOrderWithConsumer()
        {
            var orders = GetAll();
            var consumers = new List<Consumer>();

            foreach (var order in orders)
            {
                var consumer = _consumerService.GetConsumerByOrderId(order.Id);
                if (consumer != null)
                {
                    consumers.Add(consumer);
                }
            }
            var orderListModel = new OrderListModel
            {
                Orders = orders.ToList(),
                Consumers = consumers.ToList()
            };
            return orderListModel;
        }
        
        public SellerViewOrder GetFilteredOrders(Pagination pagination, string email)
        {
            var query = _context.Orders.AsQueryable();
            if (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue)
            {
                query = query.Where(x => x.Seller == email && x.CreatedDate >= pagination.From && x.CreatedDate <= pagination.To).OrderByDescending(x => x.CreatedDate);
            }

            var orders = query.ToList();
            var consumers = new List<Consumer>();
            var orderItems = new List<OrderItem>();

            foreach (var order in orders)
            {
                var orderItem = _orderItemService.GetOrderItemById(order.Id);
                var consumer = _consumerService.GetConsumerByOrderId(order.Id);

                if (orderItem != null)
                {
                    orderItems.Add(orderItem);
                }

                if (consumer != null)
                {
                    consumers.Add(consumer);
                }
            }

            var sellerViewModel = new SellerViewOrder
            {
                Orders = orders.ToList(),
                Consumers = consumers.ToList(),
                OrderItems = orderItems.ToList()
            };
            return sellerViewModel;
        }


        public SellerViewOrder GetOrders(string email)
        {
            var orders = _context.Orders.Where(x => x.Seller == email && x.IsDeleted == false).OrderByDescending(x => x.CreatedDate).ToList();
            var consumers = new List<Consumer>();
            var orderItems = new List<OrderItem>();

            foreach (var order in orders)
            {
                var orderItem = _orderItemService.GetOrderItemById(order.Id);
                var consumer = _consumerService.GetConsumerByOrderId(order.Id);

                if (orderItem != null)
                {
                    orderItems.Add(orderItem);
                }

                if (consumer != null)
                {
                    consumers.Add(consumer);
                }
            }

            var sellerViewModel = new SellerViewOrder
            {
                Orders = orders.ToList(),
                Consumers = consumers.ToList(),
                OrderItems = orderItems.ToList()
            };
            return sellerViewModel;
        }

        public async Task<int> GetAllDeliveredOrders()
        {
            return await _context.Orders.Where(o => o.OrderStatus == OrderStatus.Delivered ||
                          o.OrderStatus == OrderStatus.ShipmentDelivered).CountAsync();
        }

        public async Task<int> GetAllOrdersInTransit()
        {
            var orders = GetAll().ToList();
            return orders.Where(o => InProcessOrderStatuses(o.OrderStatus)).Count();
        }
        public async Task<int> GetAllReturnedOrders()
        {
            var orders = GetAll().ToList();
            return orders.Where(o => InReturnedOrderStatuses(o.OrderStatus)).Count();
        }

        public async Task<decimal> GetAllGrandTotal()
        {
            return await _context.Orders.SumAsync(x => x.GrandTotal);
        }

        public async Task<int> GetAllCancelledOrders()
        {
            var orders = GetAll().ToList();
            return orders.Where(o => InCancelledOrderStatuses(o.OrderStatus)).Count();
        }

        public async Task<List<Order>> GetUnpaidAndProcessedOrder()
        {
            return await _context.Orders.Where(x => x.PaymentStatus == PaymentStatus.UnPaid || x.PaymentStatus == PaymentStatus.Processing).ToListAsync();
        }
        #region Private Methods

        private async Task<bool> UpdateOrderTracking(OrderViewModel orderViewModel)
        {
            if (orderViewModel.CourierServiceType == CourierServiceType.Leopard)
            {
                var result = await _courierService.UpdateLeopardOrderByIdAsync(orderViewModel.CourierOrderId, orderViewModel.OrderId);
                if (result) return true;
            } else if(orderViewModel.CourierServiceType == CourierServiceType.Trax)
            {
                var result = await _courierService.UpdateTraxOrderByIdAsync(orderViewModel.CourierOrderId, orderViewModel.OrderId);
                if (result) return true;
            }
            else if (orderViewModel.CourierServiceType == CourierServiceType.Daewoo)
            {
                var result = await _courierService.UpdateDaewooOrderByIdAsync(orderViewModel.CourierOrderId, orderViewModel.OrderId);
                if (result) return true;
            } else if(orderViewModel.CourierServiceType == CourierServiceType.Rider)
            {
                var result = await _courierService.UpdateRiderOrderByIdAsync(orderViewModel.CourierOrderId, orderViewModel.OrderId);
                if (result) return true;
            }
            return false;
        }

        private async Task<int> GetReturenedOrderCount(string email)
        {
            var orders = await _context.Orders.Where(x => x.Seller == email).ToListAsync();
            var filteredOrders = orders.Where(o => o.OrderStatus == OrderStatus.ReturnedToShipper ||
                o.OrderStatus == OrderStatus.ReadyForReturn ||
                o.OrderStatus == OrderStatus.BeingReturn ||
                o.OrderStatus == OrderStatus.ShipmentDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ShipmentNonServiceArea ||
                o.OrderStatus == OrderStatus.ShipmentReturnConfirmationPending ||
                o.OrderStatus == OrderStatus.ReturnConfirm ||
                o.OrderStatus == OrderStatus.ReturnInTransit ||
                o.OrderStatus == OrderStatus.ReturnArrivedAtOrigin ||
                o.OrderStatus == OrderStatus.ReturnDispatched ||
                o.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ReturnDeliveredToShipper ||
                o.OrderStatus == OrderStatus.ReturnNotAttempted ||
                o.OrderStatus == OrderStatus.ReturnOnHold ||
                o.OrderStatus == OrderStatus.ReturnDispatched ||
                o.OrderStatus == OrderStatus.ReturnedUnableToReturn ||
                o.OrderStatus == OrderStatus.ReturnNotShifted ||
                o.OrderStatus == OrderStatus.ReturnRiderExchange ||
                o.OrderStatus == OrderStatus.ReplacementInTransit ||
                o.OrderStatus == OrderStatus.ReplacementArrivedAtOrigin ||
                o.OrderStatus == OrderStatus.ReplacementDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ReplacementDeliveredToShipper ||
                o.OrderStatus == OrderStatus.ReplacementRiderExchange ||
                o.OrderStatus == OrderStatus.ReplacementNotCollected ||
                o.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                o.OrderStatus == OrderStatus.RefusedDelivery ||
                o.OrderStatus == OrderStatus.ReturnToShipper ||
                o.OrderStatus == OrderStatus.ReturnToOrigin ||
                o.OrderStatus == OrderStatus.HoldForCollection ||
                o.OrderStatus == OrderStatus.ReturnToShipper ||
                o.OrderStatus == OrderStatus.QCRejectedHub ||
                o.OrderStatus == OrderStatus.AwaitingReturn ||
                o.OrderStatus == OrderStatus.Returned ||
                o.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                o.OrderStatus == OrderStatus.ReturnInProgress ||
                o.OrderStatus == OrderStatus.ReturnAttemptFailed ||
                o.OrderStatus == OrderStatus.ReturnInTransit ||
                o.OrderStatus == OrderStatus.TransitToHubReturn ||
                o.OrderStatus == OrderStatus.AwaitingLinehaulReturn).Count();
            return filteredOrders;
        }
        private async Task<int> GetOrderByEmail(string email)
        {
            var order = await _context.Orders.Where(x => x.Seller == email).ToListAsync();
            return order.Count();
        }

        private async Task<int> GetCancelledOrdersByEmail(string email)
        {
            var orders = await _context.Orders.Where(x => x.Seller == email && (x.OrderStatus == OrderStatus.FortyEightHoursAutoCanceled ||
                                                    x.OrderStatus == OrderStatus.Cancelled ||
                                                    x.OrderStatus == OrderStatus.ShipmentCancelled ||
                                                    x.OrderStatus == OrderStatus.BookingCancelled ||
                                                    x.OrderStatus == OrderStatus.CancelledByClient)).ToListAsync();
            return orders.Count();
        }
        private async Task<int> PendingOrders(string email)
        {
            var orders = await _context.Orders.Where(x => x.Seller == email && x.PaymentStatus == PaymentStatus.UnPaid || x.PaymentStatus == PaymentStatus.Processing).ToListAsync();
            var filteredOrders = orders.Where(x => !IsExcludedStatus(x.OrderStatus)).Count();
            return filteredOrders;
        }
        private decimal TotalSales(string email)
        {
            decimal totalSales = 0;
            var orders = _context.Orders.Where(o => o.Seller == email && (o.OrderStatus == OrderStatus.Delivered ||
            o.OrderStatus == OrderStatus.ShipmentDelivered ||
            o.OrderStatus == OrderStatus.ShipmentPicked ||
            o.OrderStatus == OrderStatus.DliveredAtHome ||
            o.OrderStatus == OrderStatus.Collected)).ToList();
            if (orders.Any())
            {
                totalSales = orders.Sum(o => o.SellPrice);
            }
            return totalSales;
        }
        private int TotalDeliveredOrders(string email)
        {
            var orders = _context.Orders.Where(o => o.Seller == email && (o.OrderStatus == OrderStatus.Delivered ||
            o.OrderStatus == OrderStatus.ShipmentDelivered ||
            o.OrderStatus == OrderStatus.ShipmentPicked ||
            o.OrderStatus == OrderStatus.DliveredAtHome ||
            o.OrderStatus == OrderStatus.ShipmentOnHoldForSelfCollection ||
            o.OrderStatus == OrderStatus.Collected)).ToList();
            return orders.Count();
        }

        public List<Order> GetOrderByDateRange(DateTime start, DateTime end)
        {
            return _context.Orders.Where(x => x.CreatedDate >= start && x.CreatedDate <= end).ToList(); 
        }

        private decimal CalculateProfit(decimal price, decimal sellPrice)
        {
            return sellPrice - price;
        }


        private decimal DeductedServiceCharges(string email)
        {
            return _context.Orders.Where(o => o.Seller == email && o.PaymentStatus == PaymentStatus.UnPaid &&
            (o.OrderStatus == OrderStatus.ReturnedToShipper ||
            o.OrderStatus == OrderStatus.Returned ||
            o.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
            o.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
            o.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
            o.OrderStatus == OrderStatus.ReturnConfirm ||
            o.OrderStatus == OrderStatus.Inward ||
            o.OrderStatus == OrderStatus.Delivered ||
            o.OrderStatus == OrderStatus.ShipmentDelivered ||
            o.OrderStatus == OrderStatus.ShipmentPicked ||
            o.OrderStatus == OrderStatus.DliveredAtHome ||
            o.OrderStatus == OrderStatus.Collected)).Sum(x => x.SellPrice/100 * 10);
        }

        private bool InCancelledOrderStatuses(OrderStatus orderStatus)
        {
            var includeStatuses = new[]
            {
                OrderStatus.FortyEightHoursAutoCanceled,
                OrderStatus.Cancelled, 
                OrderStatus.ShipmentCancelled,
                OrderStatus.BookingCancelled,
                OrderStatus.CancelledByClient
            };
            return includeStatuses.Contains(orderStatus);
        }
        private bool InReturnedOrderStatuses(OrderStatus orderStatusValue)
        {
            var includeStatuses = new[]
            {
                OrderStatus.ReturnedToShipper,
                OrderStatus.ReadyForReturn,
                OrderStatus.BeingReturn,
                OrderStatus.ShipmentDeliveryUnsuccessfull,
                OrderStatus.ShipmentNonServiceArea,
                OrderStatus.ShipmentReturnConfirmationPending,
                OrderStatus.ReturnConfirm,
                OrderStatus.ReturnInTransit,
                OrderStatus.ReturnArrivedAtOrigin,
                OrderStatus.ReturnDispatched,
                OrderStatus.ReturnDeliveryUnsuccessfull,
                OrderStatus.ReturnDeliveredToShipper,
                OrderStatus.ReturnNotAttempted,
                OrderStatus.ReturnOnHold,
                OrderStatus.ReturnDispatched,
                OrderStatus.ReturnedUnableToReturn,
                OrderStatus.ReturnNotShifted,
                OrderStatus.ReturnRiderExchange,
                OrderStatus.ReplacementInTransit,
                OrderStatus.ReplacementArrivedAtOrigin,
                OrderStatus.ReplacementDeliveryUnsuccessfull,
                OrderStatus.ReplacementDeliveredToShipper,
                OrderStatus.ReplacementRiderExchange,
                OrderStatus.ReplacementNotCollected,
                OrderStatus.ReturnUndeliveredHomeDelivery,
                OrderStatus.RefusedDelivery,
                OrderStatus.ReturnToShipper,
                OrderStatus.ReturnToOrigin,
                OrderStatus.HoldForCollection,
                OrderStatus.ReturnToShipper,
                OrderStatus.QCRejectedHub,
                OrderStatus.AwaitingReturn,
                OrderStatus.Returned,
                OrderStatus.DeliveryAttemptFailed,
                OrderStatus.ReturnInProgress,
                OrderStatus.ReturnAttemptFailed,
                OrderStatus.ReturnInTransit,
                OrderStatus.TransitToHubReturn,
                OrderStatus.AwaitingLinehaulReturn
            };
            return includeStatuses.Contains(orderStatusValue);
        }


        private bool InProcessOrderStatuses(OrderStatus orderstatus)
        {
            var includeStatuses = new[]
            {
                 OrderStatus.PickupRequestSent,
                 OrderStatus.ConsignmentBooked,
                 OrderStatus.InProcess,
                 OrderStatus.ShipmentBooked,
                 OrderStatus.ClaimPaid,
                 OrderStatus.OrderBooked,
                 OrderStatus.RiderOnItsWay,
                 OrderStatus.AssignToCourier,
                 OrderStatus.ArrivedAtStation,
                 OrderStatus.Missroute,
                 OrderStatus.Dispatached,
                 OrderStatus.ShipmentPicked,
                 OrderStatus.DropOffAtExpressCenter,
                 OrderStatus.ShipmentArrivedAtOrigin,
                 OrderStatus.ShipmentOutForDelivery,
                 OrderStatus.ShipmentInTransit,
                 OrderStatus.ShipmentArrivedAtDestination,
                 OrderStatus.ShipmentOutForDelivery,
                 OrderStatus.ShipmentRiderExchange,
                 OrderStatus.ShipmentNotAttempt,
                 OrderStatus.ShipmentOnHold,
                OrderStatus.ShipmentMisrouted,
                OrderStatus.ShipmentReAttempt,
                OrderStatus.ShipmentCaseClosed,
                OrderStatus.ShipmentReAttemptRequested,
                OrderStatus.ShipmentRiderPicked,
                OrderStatus.ShipmentMissrouteForwarded,
                OrderStatus.ShipmentArrivalServiceCenter,
                OrderStatus.ShipmentMultiplePiecesHold,
                OrderStatus.ShipmentDispatchedFromWarehouse,
                OrderStatus.ShipmentReceivedFromShipper,
                OrderStatus.ShipmentRecalled,
                OrderStatus.ShipmentReBooked,
                OrderStatus.ShipmentConsolidated,
                OrderStatus.ShipmentWaitingForConsolidation,
                OrderStatus.ReplacementDispatched,
                OrderStatus.ReplacementCollected,
                OrderStatus.InterceptRequested,
                OrderStatus.InterceptApproved,
                OrderStatus.Booked,
                OrderStatus.Dispatached,
                OrderStatus.DispatachedPartially,
                OrderStatus.Inward,
                OrderStatus.DispatachedForHomeDelivery,
                OrderStatus.Collected,
                OrderStatus.AwaitingDispatch,
                OrderStatus.RiderIsOutForDelivery,
                OrderStatus.DeliveryInProgress,
                OrderStatus.InTransit,
                OrderStatus.AwaitingLinehaulDispatch,
                OrderStatus.TransitToHub

            };
            return includeStatuses.Contains(orderstatus);
        }

        private bool IsExcludedStatus(OrderStatus status)
        {
            var excludedStatuses = new[]
            {
                OrderStatus.Delivered,
                OrderStatus.ShipmentDelivered,
                OrderStatus.ShipmentPicked,
                OrderStatus.DliveredAtHome,
                OrderStatus.ShipmentOnHoldForSelfCollection,
                OrderStatus.Collected,
                OrderStatus.FortyEightHoursAutoCanceled,
                OrderStatus.Cancelled,
                OrderStatus.ShipmentCancelled,
                OrderStatus.BookingCancelled,
                OrderStatus.CancelledByClient,
                OrderStatus.ReturnedToShipper,
                OrderStatus.ReadyForReturn,
                OrderStatus.BeingReturn,
                OrderStatus.ShipmentDeliveryUnsuccessfull,
                OrderStatus.ShipmentNonServiceArea,
                OrderStatus.ShipmentReturnConfirmationPending,
                OrderStatus.ReturnConfirm,
                OrderStatus.ReturnInTransit,
                OrderStatus.ReturnArrivedAtOrigin,
                OrderStatus.ReturnDispatched,
                OrderStatus.ReturnDeliveryUnsuccessfull,
                OrderStatus.ReturnDeliveredToShipper,
                OrderStatus.ReturnNotAttempted,
                OrderStatus.ReturnOnHold,
                OrderStatus.ReturnDispatched,
                OrderStatus.ReturnedUnableToReturn,
                OrderStatus.ReturnNotShifted,
                OrderStatus.ReturnRiderExchange,
                OrderStatus.ReplacementInTransit,
                OrderStatus.ReplacementArrivedAtOrigin,
                OrderStatus.ReplacementDeliveryUnsuccessfull,
                OrderStatus.ReplacementDeliveredToShipper,
                OrderStatus.ReplacementRiderExchange,
                OrderStatus.ReplacementNotCollected,
                OrderStatus.ReturnUndeliveredHomeDelivery,
                OrderStatus.RefusedDelivery,
                OrderStatus.ReturnToShipper,
                OrderStatus.ReturnToOrigin,
                OrderStatus.HoldForCollection,
                OrderStatus.ReturnToShipper,
                OrderStatus.QCRejectedHub,
                OrderStatus.AwaitingReturn,
                OrderStatus.Returned,
                OrderStatus.DeliveryAttemptFailed,
                OrderStatus.ReturnInProgress,
                OrderStatus.ReturnAttemptFailed,
                OrderStatus.ReturnInTransit,
                OrderStatus.TransitToHubReturn,
                OrderStatus.AwaitingLinehaulReturn
            };
            return excludedStatuses.Contains(status);
        }


        private async Task IncreasedQunatityInCaseOfReturn(Order order)
        {
            var user = await _context.Users.Where(x => x.Email == order.Seller).FirstOrDefaultAsync();
            var ordersItems = _orderItemService.GetOrderItemsById(order.Id);
            if (ordersItems != null)
            {
                foreach (var orderItem in ordersItems)
                {
                    if (order.OrderStatus == OrderStatus.ReturnedToShipper ||
                order.OrderStatus == OrderStatus.ReadyForReturn ||
                order.OrderStatus == OrderStatus.BeingReturn ||
                order.OrderStatus == OrderStatus.ShipmentDeliveryUnsuccessfull ||
                order.OrderStatus == OrderStatus.ShipmentNonServiceArea ||
                order.OrderStatus == OrderStatus.ShipmentReturnConfirmationPending ||
                order.OrderStatus == OrderStatus.ReturnConfirm ||
                order.OrderStatus == OrderStatus.ReturnInTransit ||
                order.OrderStatus == OrderStatus.ReturnArrivedAtOrigin ||
                order.OrderStatus == OrderStatus.ReturnDispatched ||
                order.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                order.OrderStatus == OrderStatus.ReturnDeliveredToShipper ||
                order.OrderStatus == OrderStatus.ReturnNotAttempted ||
                order.OrderStatus == OrderStatus.ReturnOnHold ||
                order.OrderStatus == OrderStatus.ReturnDispatched ||
                order.OrderStatus == OrderStatus.ReturnedUnableToReturn ||
                order.OrderStatus == OrderStatus.ReturnNotShifted ||
                order.OrderStatus == OrderStatus.ReturnRiderExchange ||
                order.OrderStatus == OrderStatus.ReplacementInTransit ||
                order.OrderStatus == OrderStatus.ReplacementArrivedAtOrigin ||
                order.OrderStatus == OrderStatus.ReplacementDeliveryUnsuccessfull ||
                order.OrderStatus == OrderStatus.ReplacementDeliveredToShipper ||
                order.OrderStatus == OrderStatus.ReplacementRiderExchange ||
                order.OrderStatus == OrderStatus.ReplacementNotCollected ||
                order.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                order.OrderStatus == OrderStatus.RefusedDelivery ||
                order.OrderStatus == OrderStatus.ReturnToShipper ||
                order.OrderStatus == OrderStatus.ReturnToOrigin ||
                order.OrderStatus == OrderStatus.HoldForCollection ||
                order.OrderStatus == OrderStatus.ReturnToShipper ||
                order.OrderStatus == OrderStatus.QCRejectedHub ||
                order.OrderStatus == OrderStatus.AwaitingReturn ||
                order.OrderStatus == OrderStatus.Returned ||
                order.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                order.OrderStatus == OrderStatus.ReturnInProgress ||
                order.OrderStatus == OrderStatus.ReturnAttemptFailed ||
                order.OrderStatus == OrderStatus.ReturnInTransit ||
                order.OrderStatus == OrderStatus.TransitToHubReturn ||
                order.OrderStatus == OrderStatus.AwaitingLinehaulReturn ||
                order.OrderStatus == OrderStatus.Cancelled ||
                order.OrderStatus == OrderStatus.FortyEightHoursAutoCanceled ||
                order.OrderStatus == OrderStatus.Cancelled ||
                order.OrderStatus == OrderStatus.ShipmentCancelled ||
                order.OrderStatus == OrderStatus.BookingCancelled ||
                order.OrderStatus == OrderStatus.CancelledByClient 
                )
                    {
                        await _inventoryService.IncreaseQuantity(user.Id, orderItem.ProductId, orderItem.Quantity);
                    }
                }
            }
        }

        #endregion
    }
}
