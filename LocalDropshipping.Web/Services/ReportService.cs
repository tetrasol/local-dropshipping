﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Models;

namespace LocalDropshipping.Web.Services
{
    public class ReportService : IReportService
    {
        private readonly LocalDropshippingContext _context;
        private readonly IWithdrawalService _withdrawalService;
        private readonly IProfilesService _profilesService;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IOrderService _orderService;
        private readonly IOrderItemService _orderItemService;

        public ReportService(LocalDropshippingContext context, IWithdrawalService withdrawalService, IProfilesService profilesService,IWebHostEnvironment webHostEnvironment,IOrderService orderService, IOrderItemService orderItemService)
        {
            _context = context;
            _withdrawalService = withdrawalService;
            _profilesService = profilesService;
            _webHostEnvironment = webHostEnvironment;
            _orderService = orderService;
            _orderItemService = orderItemService;
        }

        public PageResponse<List<ReportViewModel>> GetReportOfSeller(User user, Pagination pagination)
        {
            var withdrawals = new List<Withdrawals>();
            if (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue)
            {
                withdrawals = _context.Withdrawals.Where(x => x.UserEmail == user.Email && x.CreatedDate >= pagination.From && x.CreatedDate <= pagination.To).ToList();
            }
            else
            {
                withdrawals = _withdrawalService.GetWithdrawalByUserEmail(user.Email);
            }
            var profile = _profilesService.GetProfileById(user.Id);
      
            var reportViewModel = new ReportViewModel
            {
                Withdrawals = withdrawals.ToList(),
                Profile = profile
            };
            var pageResponse = new PageResponse<List<ReportViewModel>>(new List<ReportViewModel> { reportViewModel },
                                                                    pagination.PageNumber,
                                                                    pagination.PageSize,
                                                                    withdrawals.Count());
            return pageResponse;
        }



        public PageResponse<List<ReportViewModel>> GetAllReports(Pagination pagination)
        {
            var withdrawals = new List<Withdrawals>();
            if (pagination.PaymentStatus != "All" || (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue))
            {
                withdrawals = _withdrawalService.GetFilteredWithdrawals(pagination);
            }
            else
            {
                withdrawals = _withdrawalService.GetAll();
            }
            var profiles = _profilesService.GetAllProfiles();
            var combineData = getProfileAndWithdrawalData(withdrawals, profiles);
            var reportViewModels = combineData.Select(data =>
            {
                return new ReportViewModel
                {
                    Withdrawals = data.Withdrawals,
                    Profiles = data.Profiles
                };
            }).ToList();

            var pageResponse = new PageResponse<List<ReportViewModel>>(reportViewModels,
                                                             pagination.PageNumber,
                                                             pagination.PageSize,
                                                             withdrawals.Count());
            return pageResponse;

        }

        public TransactionDetailViewModel GetTransactionDetail(int withdrawalId,User user)
        {
            var withdrawal = _withdrawalService.GetWithdrawalRequestsById(withdrawalId);
            var profile = _profilesService.GetProfileById(user.Id);
            var orderList = _orderService.GetOrderByWithdraalId(withdrawalId);
            if (withdrawal == null || profile == null || orderList == null)
            {
                return null;
            }

            var invoiceDetail = new TransactionDetailViewModel
            {    
                TransactionId = withdrawal.TransactionId,
                CreatedDate = withdrawal.CreatedDate,
                PaymentStatus = withdrawal.PaymentStatus,
                AccountNumber = profile.BankAccountNumberOrIBAN?.ToString(),
                AccountTitle = profile.BankAccountTitle,
                BusinessName = profile.StoreName,
                Seller = withdrawal.UserEmail,
                BankName = profile.BankName,
                TransactionDetails = orderList.ToList()
                };
            return invoiceDetail;
        }


            private IEnumerable<AddWithdrawalUserViewModel> getProfileAndWithdrawalData(IEnumerable<Withdrawals> withdrawals, IEnumerable<Profiles> profiles)
            {
            var combinedData = from withdrawal in withdrawals
                               join profile in profiles
                               on withdrawal.UserEmail equals profile.User.Email into joinedData
                               from profileData in joinedData.DefaultIfEmpty()
                               select new AddWithdrawalUserViewModel
                               {
                                   WithDrawalId = withdrawal.WithdrawalId,
                                   AmountInPkr = withdrawal.AmountInPkr,
                                   paymentStatus = withdrawal.PaymentStatus,
                                   AccountTitle = profileData?.BankAccountTitle,
                                   BankAccountNumberOrIBAN = profileData?.BankAccountNumberOrIBAN,
                                   Withdrawals = new List<Withdrawals> { withdrawal },
                                   Profiles = profileData != null ? new List<Profiles> { profileData } : new List<Profiles>()
                               };
            return combinedData;

        }
    }
}
