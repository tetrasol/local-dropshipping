﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Dtos;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace LocalDropshipping.Web.Services
{
    public class WithdrawalService : IWithdrawalService
    {
        private readonly LocalDropshippingContext _context;
        public WithdrawalService(LocalDropshippingContext context)
        {
            _context = context;
        }

        public Withdrawals GetWithdrawalRequestsById(int withdrawalId)
        {
            return _context.Withdrawals.FirstOrDefault(x => x.WithdrawalId == withdrawalId);
        }

        public Withdrawals GetWithdrawalRequestsByUserEmail(string userEmail)
        {
            return _context.Withdrawals.FirstOrDefault(x => x.UserEmail == userEmail);
        }

        public Withdrawals ProcessWithdrawal(ProcessWidrawalDto processDto)
        {
            var withdrawal = _context.Withdrawals.FirstOrDefault(x => x.WithdrawalId == processDto.WithdrawalId);
            if (withdrawal != null)
            {
                withdrawal.TransactionId = processDto.TransactionId;
                withdrawal.ProcessedBy = processDto.ProcessedBy.ToString();
                withdrawal.UpdatedDate = DateTime.Now;
                _context.SaveChanges();
            }
            return withdrawal;
        }

        public bool WithdrawalRequest(string email, decimal AvailableBalance)
        {
            if (email != null)
            { 
                Withdrawals withdrawals = new Withdrawals
                {
                    UserEmail = email,
                    AmountInPkr = AvailableBalance,
                    PaymentStatus = PaymentStatus.Processing,
                    CreatedDate = DateTime.Now,
                    CreatedBy = email,
                };
                _context.Withdrawals.Update(withdrawals);
                var result = _context.SaveChanges();

                if (result > 0) return true;
            }
            return false;

        }

        public List<Withdrawals?> GetAll()
        {
            var withdrawal = new List<Withdrawals?>();
            withdrawal = _context.Withdrawals.ToList();
            if (withdrawal != null)
            {
                return withdrawal;
            }
            return new List<Withdrawals>();
        }
        public Withdrawals UpdateWithdrawal(PaymentViewModel withdrawal)
        {
            Withdrawals result = (from p in _context.Withdrawals
                             where p.WithdrawalId == withdrawal.WithdrawalId
                                  select p).SingleOrDefault();

            result.UpdateBy = withdrawal.UpdatedBy;
            result.ProcessedBy = withdrawal.ProcessedBy;
            result.PaymentStatus = withdrawal.PaymentStatus;
            result.TransactionId = withdrawal.TransactionId;
            result.UpdatedDate = DateTime.Now;
            result.Reason = withdrawal.Reason;
            _context.SaveChanges();

            var ordersToUpdate = _context.Orders
                   .Where(o => o.Seller == result.UserEmail && (
                           o.OrderStatus == OrderStatus.ReturnedToShipper ||
                o.OrderStatus == OrderStatus.ReadyForReturn ||
                o.OrderStatus == OrderStatus.BeingReturn ||
                o.OrderStatus == OrderStatus.ShipmentDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ShipmentNonServiceArea ||
                o.OrderStatus == OrderStatus.ShipmentReturnConfirmationPending ||
                o.OrderStatus == OrderStatus.ReturnConfirm ||
                o.OrderStatus == OrderStatus.ReturnInTransit ||
                o.OrderStatus == OrderStatus.ReturnArrivedAtOrigin ||
                o.OrderStatus == OrderStatus.ReturnDispatched ||
                o.OrderStatus == OrderStatus.ReturnDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ReturnDeliveredToShipper ||
                o.OrderStatus == OrderStatus.ReturnNotAttempted ||
                o.OrderStatus == OrderStatus.ReturnOnHold ||
                o.OrderStatus == OrderStatus.ReturnDispatched ||
                o.OrderStatus == OrderStatus.ReturnedUnableToReturn ||
                o.OrderStatus == OrderStatus.ReturnNotShifted ||
                o.OrderStatus == OrderStatus.ReturnRiderExchange ||
                o.OrderStatus == OrderStatus.ReplacementInTransit ||
                o.OrderStatus == OrderStatus.ReplacementArrivedAtOrigin ||
                o.OrderStatus == OrderStatus.ReplacementDeliveryUnsuccessfull ||
                o.OrderStatus == OrderStatus.ReplacementDeliveredToShipper ||
                o.OrderStatus == OrderStatus.ReplacementRiderExchange ||
                o.OrderStatus == OrderStatus.ReplacementNotCollected ||
                o.OrderStatus == OrderStatus.ReturnUndeliveredHomeDelivery ||
                o.OrderStatus == OrderStatus.RefusedDelivery ||
                o.OrderStatus == OrderStatus.ReturnToShipper ||
                o.OrderStatus == OrderStatus.ReturnToOrigin ||
                o.OrderStatus == OrderStatus.HoldForCollection ||
                o.OrderStatus == OrderStatus.ReturnToShipper ||
                o.OrderStatus == OrderStatus.QCRejectedHub ||
                o.OrderStatus == OrderStatus.AwaitingReturn ||
                o.OrderStatus == OrderStatus.Returned ||
                o.OrderStatus == OrderStatus.DeliveryAttemptFailed ||
                o.OrderStatus == OrderStatus.ReturnInProgress ||
                o.OrderStatus == OrderStatus.ReturnAttemptFailed ||
                o.OrderStatus == OrderStatus.ReturnInTransit ||
                o.OrderStatus == OrderStatus.TransitToHubReturn ||
                o.OrderStatus == OrderStatus.AwaitingLinehaulReturn ||
                o.OrderStatus == OrderStatus.Delivered ||
                o.OrderStatus == OrderStatus.ShipmentDelivered ||
                o.OrderStatus == OrderStatus.ShipmentPicked ||
                o.OrderStatus == OrderStatus.DliveredAtHome ||
                o.OrderStatus == OrderStatus.ShipmentOnHoldForSelfCollection ||
                o.OrderStatus == OrderStatus.Collected) && o.PaymentStatus == PaymentStatus.UnPaid).ToList();

            foreach (var order in ordersToUpdate)
            {
                order.WithdrawalId = result.WithdrawalId;
                order.PaymentStatus = result.PaymentStatus;
            }

            _context.SaveChanges();
            return result;
        }

        public bool UpdateWithpaymentStatus(PaymentStatus paymentStatus, int WithdrawalId)
        {
            var attachedWithdrawal = _context.Withdrawals.Attach(new Withdrawals { WithdrawalId = WithdrawalId });
            attachedWithdrawal.Entity.PaymentStatus = paymentStatus;
            _context.SaveChanges();
            return true;
        }

        public List<Withdrawals> GetWithdrawalByUserEmail(string email)
        {
            var withdrawals = _context.Withdrawals.Where(x => x.UserEmail == email).OrderByDescending(x => x.CreatedDate).ToList();
            return withdrawals;
        }

        public List<Withdrawals> GetFilteredWithdrawals(Pagination pagination)
        {
                var query = _context.Withdrawals.AsQueryable();

                if (pagination.PaymentStatus == "Paid")
                {
                    query = query.Where(x => x.PaymentStatus == PaymentStatus.Paid);
                }
                else if (pagination.PaymentStatus == "Unpaid")
                {
                    query = query.Where(x => x.PaymentStatus == PaymentStatus.UnPaid);
                }

                if (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue)
                {
                    query = query.Where(x => x.CreatedDate >= pagination.From && x.CreatedDate <= pagination.To);
                }

                return query.ToList();
        }
        public decimal GetAllWithdrawalAmount(string email)
        {
            return _context.Withdrawals.Where(x => x.UserEmail == email && (x.PaymentStatus == PaymentStatus.UnPaid || x.PaymentStatus == PaymentStatus.Processing)).Sum(x => x.AmountInPkr);
        }
        public decimal GetAllPaidWithdrawal(string email)
        {
            return _context.Withdrawals.Where(x => x.UserEmail == email && (x.PaymentStatus == PaymentStatus.Paid)).Sum(x => x.AmountInPkr);
        }

        public async Task<decimal> GetPaidWithdrawalAmount()
        {
            return await _context.Withdrawals.Where(x => x.PaymentStatus == PaymentStatus.Paid).SumAsync(x => x.AmountInPkr);
        }

        public async Task<decimal> GetAllUnpaidAmount()
        {
            return await _context.Withdrawals.Where(x => x.PaymentStatus == PaymentStatus.UnPaid && x.PaymentStatus == PaymentStatus.Processing).SumAsync(x => x.AmountInPkr);
        }

        public async Task<List<Withdrawals>> GetWithdrawalsByDateRange(DateTime start, DateTime end)
        {
            return await _context.Withdrawals.Where(x => x.CreatedDate >= start && x.CreatedDate <= end).ToListAsync();
        }
    }
}
