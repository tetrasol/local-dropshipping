﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Exceptions;
using Microsoft.AspNetCore.Identity;
using LocalDropshipping.Web.Dtos;
using LocalDropshipping.Web.Models;
using LocalDropshipping.Web.Helpers.Constants;

namespace LocalDropshipping.Web.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly LocalDropshippingContext _context;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;

        public UserService(UserManager<User> userManager, SignInManager<User> signInManager, LocalDropshippingContext context, IEmailService emailService, IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _emailService = emailService;
            _configuration = configuration;
        }

        [Obsolete("Use HttpContext.Items[\"Current\"] instead")]
        public async Task<User?> GetCurrentUserAsync()
        {
            var user = await _userManager.GetUserAsync(_signInManager.Context.User);
            return user;
        }

        public bool IsUserSignedIn()
        {
            return _signInManager.IsSignedIn(_signInManager.Context.User);
        }

        public async Task UpdateUserAsync(User user)
        {
            var existingUser = _context.Users.FirstOrDefault(x => x.Id == user.Id);
            if (existingUser != null)
            {
                existingUser.Fullname = user.Fullname;
                existingUser.IsSeller = user.IsSeller;
                existingUser.IsAdmin = user.IsAdmin;
                existingUser.IsSuperAdmin = user.IsSuperAdmin;
                existingUser.IsActive = user.IsActive;
                existingUser.IsDeleted = user.IsDeleted;
                existingUser.IsSubscribed = user.IsSubscribed;
                existingUser.EmailConfirmed = user.EmailConfirmed;
                existingUser.PhoneNumber = user.PhoneNumber;
                existingUser.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
                existingUser.IsProfileCompleted = user.IsProfileCompleted;
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new UserNotFoundException();
            }
        }

        public User Add(User user)
        {
            user.IsActive = true;

            _context.Users.Add(user);

            return user;
        }

        public User Delete(string userId)
        {
            try
            {
                var user = _context.Users.Find(userId);
                if (user != null)
                {
                    user.IsDeleted = true;

                    _context.SaveChanges();
                    return user;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public bool DisableUser(string userId)
        {
            try
            {
                var user = _context.Users.Find(userId);
                if (user != null)
                {
                    user.IsActive = false;
                    _context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //public bool? ActivateUser(string userId)
        //{
        //    try
        //    {
        //        var user = _context.Users.Find(userId);
        //        if (user != null && user.IsActive == false)
        //        {

        //            user.IsActive = true;



        //            _context.SaveChanges();
        //        }
        //        else
        //        {
        //            user.IsActive = false;

        //            _context.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }

        //}

        public bool? ActivateUser(string userId)
        {
            try
            {
                var user = _context.Users.Find(userId);
                if (user != null && user.IsActive == false)
                {
                    user.IsActive = true;
                    _context.SaveChanges();
                    var userName = user.Fullname;
                    var webUrl = _configuration.GetValue<string>(Constants.Configuration.webUrl);
                    var Link = $"{webUrl}Seller/Login";
                    var emailOptions = new EmailMessage
                    {
                        ToEmail = user.Email,
                        Subject = "Account Activation",
                        TemplatePath = "AccountActivation",
                        Placeholders = new List<KeyValuePair<string, string>>()

                    };
                    emailOptions.Placeholders.Add(new KeyValuePair<string, string>("{{UserName}}", userName));
                    emailOptions.Placeholders.Add(new KeyValuePair<string, string>("{{Link}}", Link));
                    _emailService.SendEmail(emailOptions);
                }
                else
                {
                    user.IsActive = false;
                    _context.SaveChanges();
                    var userName = user.Fullname;
                    var webUrl = _configuration.GetValue<string>(Constants.Configuration.webUrl);
                    var Link = $"{webUrl}Seller/ContactUs";
                    var emailOptions = new EmailMessage
                    {
                        ToEmail = user.Email,
                        Subject = "Account Deactivate",
                        TemplatePath = "AccountDeactivation",
                        Placeholders = new List<KeyValuePair<string, string>>()

                    };
                    emailOptions.Placeholders.Add(new KeyValuePair<string, string>("{{UserName}}", userName));
                    emailOptions.Placeholders.Add(new KeyValuePair<string, string>("{{Link}}", Link));
                    _emailService.SendEmail(emailOptions);

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool? IsSubscribe(string userId)
        {
            try
            {
                var user = _context.Users.Find(userId);
                if (user != null && user.IsSubscribed == false)
                {

                    user.IsSubscribed = true;
                    _context.SaveChanges();
                }
                else
                {
                    user.IsSubscribed = false;
                    _context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public List<User> GetAllStaffMember()
        {
            return _context.Users.Where(x => x.IsAdmin == true && x.IsDeleted == false).ToList();
        }

        public List<User> GetAll()
        {
            return _context.Users.Where(x => x.IsAdmin == false && x.IsSuperAdmin == false && x.IsDeleted == false).ToList();
        }

        public User? GetById(string userId)
        {
            return _context.Users.FirstOrDefault(c => c.Id == userId);
        }

        public User? Update(string userId, UserDto userDto)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.UserName = userDto.Name;
                user.PhoneNumber = userDto.PhoneNumber;
                _context.SaveChanges();
            }
            return user;
        }

        public string GetUserIdByEmail(string userEmail)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == userEmail);
            return user.Id.ToString();
        }

        public bool CheckEmailExist(string email)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == email);
            if (user != null) return true;
            return false;
        }

        public bool DeactivateUser(string email,decimal Amount)
        {
            var user = _context.Users.FirstOrDefault(x => x.Email == email);
            if (user != null)
            {
                user.IsActive = false;
                _context.SaveChanges();
                var userName = user.Fullname;
                var webUrl = _configuration.GetValue<string>(Constants.Configuration.webUrl);
                var Link = $"{webUrl}Seller/ContactUs";
                var emailOptions = new EmailMessage
                {
                    ToEmail = user.Email,
                    Subject = "Account Deactivate",
                    TemplatePath = "AccountDeactivationForWallet",
                    Placeholders = new List<KeyValuePair<string, string>>()
                };
                emailOptions.Placeholders.Add(new KeyValuePair<string, string>("{{UserName}}", userName));
                emailOptions.Placeholders.Add(new KeyValuePair<string, string>("{{Amount}}", Amount.ToString()));
                _emailService.SendEmail(emailOptions);
                return true;
            }
            return false;
        }

        public User GetByEmail(string email)
        {
            return _context.Users.FirstOrDefault(x => x.Email == email);
        }
    }
}
