﻿namespace LocalDropshipping.Web.Services
{
    public interface ICourierService
    {
        Task<bool> UpdateLeopardOrderByIdAsync(string courierOrderId,int Id);
        Task<bool> UpdateLeopardOrdersAsync(string[] courierOrderIds);
        Task<bool> UpdateTraxOrderByIdAsync(string courierOrderId, int Id);
        Task<bool> UpdateDaewooOrderByIdAsync(string courierOrderId, int Id);
        Task<bool> UpdateRiderOrderByIdAsync(string courierOrderId, int Id);
    }
}
