﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Models.Inventory;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol.Plugins;

namespace LocalDropshipping.Web.Services
{
    public class InventoryService : IInventoryService
    {
        private readonly LocalDropshippingContext _context;
        private readonly IUserService _userService;
        private readonly IProfilesService _profilesService;
        private readonly IProductVariantService _productVariantService;

        public InventoryService(LocalDropshippingContext context,IUserService userService,IProfilesService profilesService,IProductVariantService productVariantService)
        {
            _context = context;
            _userService = userService;
            _profilesService = profilesService;
            _productVariantService = productVariantService;
        }

        public async Task<bool> AddAssignInventory(AddInventoryAssignModel assignModel)
        {
            var user = _userService.GetById(assignModel.UserId);
            var getById = await GetInventoryAssignById(assignModel.Id);
            var variantId = _productVariantService.GetByProductIdAsync(assignModel.ProductId);

            if (getById != null && assignModel.ProductId != 0)
            {
                getById.Price = assignModel.Price;
                getById.Quantity += assignModel.Quantity;
                getById.UpdatedDate = DateTime.Now;
                getById.Comments = assignModel.Comments;
                getById.ProductName = assignModel.ProductName;
                getById.StoreName = assignModel.StoreName;
                getById.VariantId = variantId.ProductVariantId;

                _context.InventoryAssigns.Update(getById);
                var addInventoryHistory = new InventoryHistory
                {
                    StoreName = assignModel.StoreName,
                    AssignQuantity = assignModel.Quantity,
                    Comments = assignModel.Comments,
                    AssignInventoryId = assignModel.Id,
                    CreatedDate = DateTime.Now,
                    Price = assignModel.Price,
                    ProductName = assignModel.ProductName,
                    StockQuantity = assignModel.Quantity,
                    UpdatedBy = assignModel.AssignBy,
                    UserId = user.Id,
                };
                await AddInventoryHistory(addInventoryHistory);
            }
            else
            {
                var assignInventory = new InventoryAssign
                {
                    AssignBy = assignModel.AssignBy,
                    CreatedDate = DateTime.Now,
                    Price = assignModel.Price,
                    Quantity = assignModel.Quantity,
                    ProductId = assignModel.ProductId,
                    ProductName = assignModel.ProductName,
                    UpdatedDate = DateTime.Now,
                    StoreName = assignModel.StoreName,
                    UserId = assignModel.UserId,
                    VariantId = variantId.ProductVariantId,
                    Comments = assignModel.Comments,
                    Sourcing = assignModel.Sourcing
                };

                var result = await _context.InventoryAssigns.AddAsync(assignInventory);
                await _context.SaveChangesAsync();
                var addInventoryHistory = new InventoryHistory
                {
                    StoreName = assignModel.StoreName,
                    AssignQuantity = assignModel.Quantity,
                    Comments = assignModel.Comments,
                    AssignInventoryId = result.Entity.Id,
                    CreatedDate = DateTime.Now,
                    Price = assignModel.Price,
                    ProductName = assignModel.ProductName,
                    StockQuantity = assignModel.Quantity,
                    UpdatedBy = assignModel.AssignBy,
                    UserId = user.Id,
                };
                await AddInventoryHistory(addInventoryHistory);
            }
            return true;
        }

        public async Task<bool> AddInventoryHistory(InventoryHistory historyModel)
        {
            await _context.InventoryHistories.AddAsync(historyModel);
            await _context.SaveChangesAsync();
            return true;
        }

        

        public async Task<List<InventoryAssign>> GetByUserId(string UserId)
        {
            var profile = _profilesService.GetProfileById(UserId);
            var result = await _context.InventoryAssigns
                .Where(x => x.UserId == UserId)
                .ToListAsync();

            var inventoryAssigns = result.Select(assign => new InventoryAssign
            {
                Id = assign.Id,
                ProductId = assign.ProductId,
                VariantId = assign.VariantId,
                UserId = UserId,
                ProductName = assign.ProductName,
                Quantity = assign.Quantity,
                Price = assign.Price,
                CreatedDate = assign.CreatedDate,
                StoreName = profile.StoreName,
                AssignBy = assign.AssignBy,
                UpdatedDate = assign.UpdatedDate,
                Comments = assign.Comments,
                Sourcing = assign.Sourcing
            }).ToList();

            if (inventoryAssigns.Count == 0 && profile != null)
            {
                inventoryAssigns.Add(new InventoryAssign
                {
                    StoreName = profile.StoreName,
                    UserId = UserId
                });
            }
            return inventoryAssigns;
        }

        public async Task<InventoryAssign> GetInventoryAssignById(int Id)
        {
            return await _context.InventoryAssigns.FirstOrDefaultAsync(x => x.Id == Id);
        }

        public async Task<List<InventoryHistory>> GetInventoryHistoryByAssignId(int Id)
        {
            var inventoryHistoryList = await _context.InventoryHistories.Where(x => x.AssignInventoryId == Id).ToListAsync();
            var modelList = new List<InventoryHistory>();
            foreach (var inventoryHistory in inventoryHistoryList)
            {
                string productName = inventoryHistory.ProductName;
                if (productName.Length > 30)
                {
                    var assign = await GetInventoryAssignById(inventoryHistory.AssignInventoryId);
                    if (assign.Sourcing == Sourcing.Local)
                    {
                        productName = productName.Substring(0, 30) + "... " + "(Local)";
                    }
                    else
                    {
                        productName = productName.Substring(0, 30) + "... " + "(China)";
                    }
                }
                var model = new InventoryHistory
                {
                    Id = inventoryHistory.Id,
                    AssignInventoryId = inventoryHistory.AssignInventoryId,
                    AssignQuantity = inventoryHistory.AssignQuantity,
                    Comments = inventoryHistory.Comments,
                    CreatedDate = inventoryHistory.CreatedDate,
                    Price = inventoryHistory.Price,
                    StockQuantity = inventoryHistory.StockQuantity,
                    StoreName = inventoryHistory.StoreName,
                    UpdatedBy = inventoryHistory.UpdatedBy,
                    UserId = inventoryHistory.UserId,
                    ProductName = productName
                };
                modelList.Add(model);
            }
            return modelList;
        }

        public async Task<List<InventoryHistory>> GetInventoryHistoryByUserId(string userId)
        {
            var inventoryHistoryList = await _context.InventoryHistories
                .Where(x => x.UserId == userId)
                .ToListAsync();
            
            var modelList = new List<InventoryHistory>();
            foreach (var inventoryHistory in inventoryHistoryList)
            {
                string productName = inventoryHistory.ProductName; 
                if (productName.Length > 30)
                {
                    var assign = await GetInventoryAssignById(inventoryHistory.AssignInventoryId);
                    if (assign.Sourcing == Sourcing.Local)
                    {
                        productName = productName.Substring(0, 30) + "... " + "(Local)";
                    }
                    else
                    {
                        productName = productName.Substring(0, 30) + "... " + "(China)";
                    }
                }
                var model = new InventoryHistory
                {
                    Id = inventoryHistory.Id,
                    AssignInventoryId = inventoryHistory.AssignInventoryId,
                    AssignQuantity = inventoryHistory.AssignQuantity,
                    Comments = inventoryHistory.Comments,
                    CreatedDate = inventoryHistory.CreatedDate,
                    Price = inventoryHistory.Price,
                    StockQuantity = inventoryHistory.StockQuantity,
                    StoreName = inventoryHistory.StoreName,
                    UpdatedBy = inventoryHistory.UpdatedBy,
                    UserId = inventoryHistory.UserId,
                    ProductName = productName 
                };
                modelList.Add(model);
            }
            return modelList;
        }

        public async Task<List<ViewMyProductModel>> GetInventoryListByUserId(string userId)
        {
            var inventoryList = await _context.InventoryAssigns
                .Where(x => x.UserId == userId)
                .ToListAsync();
            var modelList = new List<ViewMyProductModel>();
            
            foreach (var inventory in inventoryList)
            {
                var productVariant = await _context.ProductVariants.FirstOrDefaultAsync(x => x.ProductId == inventory.ProductId);
                string productName = inventory.ProductName;
                if (productName.Length > 30)
                {
                    if (inventory.Sourcing == Sourcing.Local)
                    {
                        productName = productName.Substring(0, 30) + "... " + "(Local)";
                    }
                    productName = productName.Substring(0, 30) + "... " + "(China)";
                }

                var model = new ViewMyProductModel
                {
                    Id = inventory.Id,
                    ProductId = productVariant.ProductId,
                    ProductImage = productVariant?.FeatureImageLink,
                    Quantity = inventory.Quantity,
                    Price = productVariant.DiscountedPrice,
                    ProductName = productName
                };
                modelList.Add(model);
            }
            return modelList.ToList();
        }

        public async Task IncreaseQuantity(string userId, int productId,int quantity)
        {
            try
            {
                
                var result = await _context.InventoryAssigns.FirstOrDefaultAsync(x => x.UserId == userId && x.ProductId == productId);
                if (result != null)
                {
                    result.Quantity += quantity;
                    _context.InventoryAssigns.Update(result);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task DecreaseQuantity(string userId, int productId,int quantity)
        {
            try
            {
                var result = await _context.InventoryAssigns.FirstOrDefaultAsync(x => x.UserId == userId && x.ProductId == productId);
                if (result != null)
                {
                    result.Quantity -= quantity;
                    _context.InventoryAssigns.Update(result);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
    }
}

