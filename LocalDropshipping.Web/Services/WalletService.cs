﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace LocalDropshipping.Web.Services
{
    public class WalletService : IWalletService
    {
        private readonly LocalDropshippingContext _context;

        public WalletService(LocalDropshippingContext context)
        {
            _context = context;
        }

        public async Task<bool> AddWallet(string email, decimal amount)
        {
            var user = await _context.Users.Where(x => x.Email == email).FirstOrDefaultAsync();
            var profile = await _context.Profiles.Where(x => x.UserId == user.Id).FirstOrDefaultAsync();
            Wallet wallet = new Wallet
            {
                Email = email,
                Amount = amount,
                ProfileId = profile.ProfileId,
                Profile = profile,
            };

            _context.Wallets.Add(wallet);
            await _context.SaveChangesAsync(); 
            return wallet.WalletId > 0; 
        }

        public async Task EmptyWallet(string email)
        {
            var walletsToUpdate = _context.Wallets
                .Where(w => w.Email == email);

            foreach (var wallet in walletsToUpdate)
            {
                wallet.Amount = 0;
            }
            await _context.SaveChangesAsync();
        }

        public async Task<decimal> GetTotalWalletAmount(string email)
        {
            decimal totalAmount = await _context.Wallets
                .Where(w => w.Email == email)
                .Select(w => w.Amount)
                .SumAsync();
            return totalAmount;
        }

        public async Task<bool> RemoveWallet(string email)
        {
            var wallet = await _context.Wallets
                .FirstOrDefaultAsync(w => w.Email == email);

            if (wallet != null)
            {
                _context.Wallets.Remove(wallet);
                await _context.SaveChangesAsync(); 
                return true;
            }

            return false;
        }

    }
}
