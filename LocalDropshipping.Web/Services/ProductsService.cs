﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace LocalDropshipping.Web.Services
{
    public class ProductsService : IProductsService
    {
        private readonly LocalDropshippingContext _context;
        private readonly IUserService _userService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProductsService(LocalDropshippingContext context, IUserService userService, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userService = userService;
            _webHostEnvironment = webHostEnvironment;
        }

        public List<Product> GetAll()
        {
            var data = _context.Products
                                .Where(x => !x.IsDeleted)
                                .Include(x => x.Category)
                                .Include(x => x.Variants)
                                .ToList();
            return data;
        }

        public Product Add(Product product, User currentUser)
        {
            var userEmail = currentUser.Email;

            product.CreatedDate = DateTime.Now;
            product.CreatedBy = userEmail;

            foreach (var variant in product.Variants)
            {
                variant.CreatedDate = DateTime.Now;
                variant.CreatedBy = userEmail;
            }

            _context.Products.Add(product);
            _context.SaveChanges();
            return product;
        }

        public Product? GetById(int productId)
        {
            return _context.Products
                .Include(x => x.Category)
                .Include(x => x.Variants)
                .ThenInclude(x => x.Images)
                .Include(x => x.Variants)
                .ThenInclude(x => x.Videos)
                .FirstOrDefault(p => p.ProductId == productId && !p.IsDeleted);
        }

        public Product? Delete(int productId, User currentUser)
        {
            var product = _context.Products.FirstOrDefault(x => x.ProductId == productId);
            if (product != null)
            {
                product.IsDeleted = true;
                product = Update(productId, product, currentUser);
            }
            return product;
        }

        public Product? Update(int productId, Product product, User currentUser, bool isSimpleProduct = true)
        {
            Product? exProduct = _context.Products
                .Include(x => x.Variants)
                .ThenInclude(x => x.Images)
                .Include(x => x.Variants)
                .ThenInclude(x => x.Videos)
                .FirstOrDefault(x => x.ProductId == productId);

            if (exProduct != null)
            {
                var userEmail = currentUser.Email;
                exProduct.Name = product.Name;
                exProduct.ShortDescription = product.ShortDescription;
                exProduct.ShortDescriptionContents = product.ShortDescriptionContents;
                exProduct.Description = product.Description;
                exProduct.DescriptionContents = product.DescriptionContents;
                exProduct.SKU = product.SKU;
                exProduct.CategoryId = product.CategoryId;
                exProduct.IsTopRated = product.IsTopRated;
                exProduct.IsBestSelling = product.IsBestSelling;
                exProduct.IsNewArravial = product.IsNewArravial;
                exProduct.Weight = product.Weight;
                exProduct.Sourcing = product.Sourcing;

                exProduct.IsDeleted = product.IsDeleted;

                if (isSimpleProduct)
                {
                    var variant = product.Variants.First();
                    var exVariant = exProduct.Variants.FirstOrDefault(x => x.ProductVariantId == variant.ProductVariantId);
                    exVariant.Quantity = variant.Quantity;
                    exVariant.VariantPrice = variant.VariantPrice;
                    exVariant.DiscountedPrice = variant.DiscountedPrice;
                    if (variant.Images.Any())
                    {
                        exVariant.Images.DeleteAllFromServer(_webHostEnvironment.ContentRootPath);
                        exVariant.Images.RemoveAll(_ => true);
                        exVariant.Images.AddRange(variant.Images);
                    }

                    if (variant.Videos.Any())
                    {
                        exVariant.Videos.DeleteAllFromServer(_webHostEnvironment.ContentRootPath);
                        exVariant.Videos.RemoveAll(_ => true);
                        exVariant.Videos.AddRange(variant.Videos);
                    }

                    if (!variant.FeatureImageLink.IsNullOrEmpty())
                    {
                        new ProductVariantImage { Link = exVariant.FeatureImageLink }.DeleteFromServer(_webHostEnvironment.ContentRootPath);
                        exVariant.FeatureImageLink = variant.FeatureImageLink;
                    }


                    variant.UpdatedDate = DateTime.Now;
                    variant.UpdatedBy = userEmail;
                }
                else
                {
                    var exVariants = exProduct.Variants;
                    foreach (var variant in product.Variants)
                    {
                        if (exVariants.Any(x => variant.ProductVariantId == x.ProductVariantId))
                        {
                            var exVariant = exVariants.First(x => variant.ProductVariantId == x.ProductVariantId);

                            exVariant.VariantType = variant.VariantType;
                            exVariant.Variant = variant.Variant;
                            exVariant.VariantPrice = variant.VariantPrice;
                            exVariant.DiscountedPrice = variant.DiscountedPrice;
                            exVariant.Quantity = variant.Quantity;

                            if (variant.Images.Any())
                            {
                                exVariant.Images.DeleteAllFromServer(_webHostEnvironment.ContentRootPath);
                                exVariant.Images.RemoveAll(_ => true);
                                exVariant.Images.AddRange(variant.Images);
                            }

                            if (variant.Videos.Any())
                            {
                                exVariant.Videos.DeleteAllFromServer(_webHostEnvironment.ContentRootPath);
                                exVariant.Videos.RemoveAll(_ => true);
                                exVariant.Videos.AddRange(variant.Videos);
                            }

                            if (!variant.FeatureImageLink.IsNullOrEmpty())
                            {
                                new ProductVariantImage { Link = exVariant.FeatureImageLink }.DeleteFromServer(_webHostEnvironment.ContentRootPath);
                                exVariant.FeatureImageLink = variant.FeatureImageLink;
                            }


                            exVariant.UpdatedDate = DateTime.Now;
                            exVariant.UpdatedBy = userEmail;
                        }
                        else
                        {
                            exProduct.Variants.Add(new ProductVariant
                            {
                                VariantType = variant.VariantType,
                                Variant = variant.Variant,
                                VariantPrice = variant.VariantPrice,
                                DiscountedPrice = variant.DiscountedPrice,
                                Quantity = variant.Quantity,

                                CreatedBy = userEmail,
                                CreatedDate = DateTime.Now,
                            });
                        }
                    }
                }


                exProduct.UpdatedDate = DateTime.Now;
                exProduct.UpdatedBy = userEmail;
                _context.SaveChanges();
            }
            return exProduct;
        }

        public List<Product> GetProductsByPriceRange(decimal minPrice, decimal maxPrice)
        {
            // TODO: Needs to be updated
            //return _context.Products.Where(x => x.IsDeleted == false && x.Price >= minPrice && x.Price <= maxPrice && !x.IsDeleted).ToList();
            return _context.Products.ToList();
        }

        public List<Product> GetProductsBySearch(string searchString)
        {
            return _context.Products.Where(x => !x.IsDeleted && x.Name.Contains(searchString))
                                .Include(x => x.Category)
                                .Include(x => x.Variants)
                                .ToList();


        }


        public IQueryable<Product> GetBestSelleingProducts()
        {
            return _context.Products.Where(x => x.IsBestSelling == true && x.IsDeleted == false).Include(x => x.Variants);
        }

        public IQueryable<Product> GetNewArrivalProducts()
        {
            return _context.Products.Where(x => x.IsNewArravial == true && x.IsDeleted == false).Include(x => x.Variants);
        }

        public IQueryable<Product> GetTopRatedProducts()
        {
            return _context.Products.Where(x => x.IsTopRated == true && x.IsDeleted == false).Include(x => x.Variants);
        }

    }

}
