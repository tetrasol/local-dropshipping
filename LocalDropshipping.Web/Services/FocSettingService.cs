﻿using LocalDropshipping.Web.Data;
using static LocalDropshipping.Web.Helpers.Constants.Constants;

namespace LocalDropshipping.Web.Services
{
    public class FocSettingService : IFocSettingService
    {
        private readonly LocalDropshippingContext _context;

        public FocSettingService(LocalDropshippingContext context)
        {
            _context = context;
        }

        public string GetPaymentLimit()
        {
            return _context.FocSettings.FirstOrDefault(x => x.Name == FOCSettings.PaymentLimit).Value;
        }

        public string GetNews(string news)
        {
            return _context.FocSettings.FirstOrDefault(x => x.Name == news).Value;
        }

        public string GetShippingCost(string cost)
        {
            return _context.FocSettings.FirstOrDefault(x=>x.Name == cost).Value;
        }
    }
}
