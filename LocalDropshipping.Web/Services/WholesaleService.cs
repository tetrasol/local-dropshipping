﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Helpers;
using Microsoft.EntityFrameworkCore;

namespace LocalDropshipping.Web.Services
{
    public class WholesaleService : IWholesaleService
    {
        private readonly LocalDropshippingContext _context;
        public WholesaleService(LocalDropshippingContext context)
        {
            _context = context;
        }

        public async Task<List<Quotations>> GetAllWholesales()
        {
            return await _context.Quotations.ToListAsync();
        }
        public async Task<Quotations> GetById(int id)
        {
            return await _context.Quotations.FirstOrDefaultAsync(x => x.QuotationId == id);
        }

        public async Task<bool>  AddNewWholesale(Quotations quotation)
        {
            try
            {
                await _context.Quotations.AddAsync(quotation);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;

            } 
        }

        public async Task<bool> Delete(int wholesaleId)
        {
            var quotation = await _context.Quotations.FirstOrDefaultAsync(x => x.QuotationId == wholesaleId);

            if (quotation != null)
            {
                _context.Quotations.Remove(quotation);
                await _context.SaveChangesAsync();
                return true; 
            }
            return false; 
        }

        public async Task<List<Quotations>> GetPendingWholesales()
        {
            return await _context.Quotations.Where(x => x.QuotationStatus == QuotationStatus.Pending).ToListAsync();
        }

        public async Task<List<Quotations>> GetProcessedWholesales()
        {
            return await _context.Quotations.Where(x => x.QuotationStatus == QuotationStatus.Processed).ToListAsync();

        }

        public List<Quotations> GetFilteredWholesales(Pagination pagination)
        {
            var query = _context.Quotations.AsQueryable();

            if (pagination.QuotationStatus == "Pending")
            {
                query = query.Where(x => x.QuotationStatus == QuotationStatus.Pending);
            }
            else if (pagination.QuotationStatus == "Processed")
            {
                query = query.Where(x => x.QuotationStatus == QuotationStatus.Processed);
            }

            if (pagination.From != DateTime.MinValue && pagination.To != DateTime.MinValue)
            {
                query = query.Where(x => x.CreatedDate >= pagination.From && x.CreatedDate <= pagination.To);
            }
            return query.OrderByDescending(x => x.CreatedDate).ToList();
        }

        public List<Quotations> GetWholesaleByDaterange(DateTime start, DateTime end)
        {
            return _context.Quotations.Where(x => x.CreatedDate >= start && x.CreatedDate <= end).ToList();
        }

        public async Task<bool> UpdateStatus(int quotationId, string Status)
        {
            var wholesale = await _context.Quotations.FirstOrDefaultAsync(x => x.QuotationId == quotationId);

            if (wholesale != null)
            {
                switch (Status)
                {
                    case "Pending":
                        wholesale.QuotationStatus = QuotationStatus.Pending;
                        break;
                    case "Processed":
                        wholesale.QuotationStatus = QuotationStatus.Processed;
                        break;
                }

              await  _context.SaveChangesAsync();
              return true;
            }
            return false;
        }
    }
}
