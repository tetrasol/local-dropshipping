﻿namespace LocalDropshipping.Web.Services
{
    public interface IWalletService
    {
        Task<bool> AddWallet(string email, decimal amount);
        Task<bool> RemoveWallet(string email);
        Task<decimal> GetTotalWalletAmount(string email);
        Task EmptyWallet(string email);
    }
}
