﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Helpers.Constants;
using LocalDropshipping.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace LocalDropshipping.Web.Services
{
    public class CourierService : ICourierService
    {
        private readonly CourierAuthentication _courierAuth;
        private readonly LocalDropshippingContext _context;
        private readonly IInventoryService _inventoryService;
        private readonly IOrderItemService _orderItemService;

        public CourierService(IOptions<CourierAuthentication> courierAuth,LocalDropshippingContext context,IInventoryService inventoryService,IOrderItemService orderItemService)
        {
            _courierAuth = courierAuth.Value;
            _context = context;
            _inventoryService = inventoryService;
            _orderItemService = orderItemService;
        }


        #region Leopard
        public async Task<bool> UpdateLeopardOrderByIdAsync(string courierOrderId, int Id)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string jsonData = $"{{\"api_key\":\"{_courierAuth.Leopard.ApiKey}\",\"api_password\":\"{_courierAuth.Leopard.ApiPassword}\",\"shipment_order_id\":[\"{courierOrderId}\"]}}";

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ContentTypeFiles.ApplicationJson));

                    HttpContent content = new StringContent(jsonData, Encoding.UTF8, Constants.ContentTypeFiles.ApplicationJson);

                    HttpResponseMessage response = await client.PostAsync(_courierAuth.Leopard.Url, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        dynamic responseData = JsonConvert.DeserializeObject(responseBody);
                        var details = responseData.data;

                        if (details != null && details.Count > 0)
                        {
                            dynamic orderData = details[0];
                            Order order  = (from p in _context.Orders
                                            where p.Id == Id
                                            select p).SingleOrDefault();
                            if (order != null)
                            {
                                order.OrderTrackingId = orderData.track_number;
                                order.OrderStatus = MapDescriptionToOrderStatus(orderData.booked_packet_status.ToString());
                                await _context.SaveChangesAsync();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateLeopardOrdersAsync(string[] courierOrderIds)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var requestData = new
                    {
                        api_key = _courierAuth.Leopard.ApiKey,
                        api_password = _courierAuth.Leopard.ApiPassword,
                        shipment_order_id = courierOrderIds
                    };

                    string jsonData = JsonConvert.SerializeObject(requestData);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ContentTypeFiles.ApplicationJson));

                    HttpContent content = new StringContent(jsonData, Encoding.UTF8, Constants.ContentTypeFiles.ApplicationJson);

                    HttpResponseMessage response = await client.PostAsync(_courierAuth.Leopard.Url, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        dynamic responseData = JsonConvert.DeserializeObject(responseBody);
                        var details = responseData.data;

                        if (details != null && details.Count > 0)
                        {
                            foreach (var orderData in details)
                            {
                                string courierOrderId = orderData.booked_packet_order_id.ToString();
                                Order order = (from p in _context.Orders
                                                     where p.CourierOrderId == courierOrderId 
                                                     select p).SingleOrDefault();
                                if (order != null)
                                {
                                    order.OrderTrackingId = orderData.track_number;
                                    order.OrderStatus = MapDescriptionToOrderStatus(orderData.booked_packet_status.ToString());
                                    await _context.SaveChangesAsync();
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

     
        #endregion

        #region Trax

        public async Task<bool> UpdateTraxOrderByIdAsync(string courierOrderId, int Id)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string apiUrl = $"{_courierAuth.Trax.Url}/shipment/status/order_id?order_id={courierOrderId}&type=0";

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(_courierAuth.Trax.ApiKey);

                    HttpResponseMessage response = await client.GetAsync(apiUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        dynamic responseData = JsonConvert.DeserializeObject(responseBody);
                        var details = responseData.details;

                        if (details != null && details.Count > 0)
                        {
                            dynamic orderData = details[0];
                            Order order = await _context.Orders.SingleOrDefaultAsync(p => p.Id == Id);

                            if (order != null)
                            {
                                order.OrderTrackingId = orderData.tracking_number;
                                order.OrderStatus = MapDescriptionToOrderStatus(orderData.status.ToString());
                                await _context.SaveChangesAsync();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Daewoo
        public async Task<bool> UpdateDaewooOrderByIdAsync(string courierOrderId, int Id)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string apiUrl = $"{_courierAuth.Daewoo.Url}/api/booking/quickTrack?trackingNo=" + courierOrderId;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer");
                    HttpResponseMessage response = await client.GetAsync(apiUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        dynamic responseData = JsonConvert.DeserializeObject(responseBody);
                        var details = responseData.Result.CurrentTrackStatus;

                        if (details != null && details.Count > 0)
                        {
                            dynamic orderData = details[0];
                            Order order = await _context.Orders.SingleOrDefaultAsync(p => p.Id == Id);

                            if (order != null)
                            {
                                order.OrderTrackingId = orderData.track_no;
                                order.OrderStatus = MapDescriptionToOrderStatus(orderData.status_name.ToString());
                                await _context.SaveChangesAsync();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception) { 
                return false;
            }
        }
        #endregion

        #region Rider

        public async Task<bool> UpdateRiderOrderByIdAsync(string courierOrderId, int Id)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string apiUrl = $"{_courierAuth.Rider.Url}?cn={courierOrderId}&loginid={_courierAuth.Rider.LoginId}&apiKey={_courierAuth.Rider.ApiKey}";
                    HttpResponseMessage response = await client.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        dynamic responseData = JsonConvert.DeserializeObject(responseBody);

                        if (responseData != null && responseData.Count > 0)
                        {
                            dynamic orderData = responseData[responseData.Count - 1];
                            Order order = await _context.Orders.SingleOrDefaultAsync(p => p.Id == Id);

                            if (order != null)
                            {
                                order.OrderTrackingId = orderData.consignment_id;
                                order.OrderStatus =  MapDescriptionToOrderStatus(orderData.order_status.ToString());
                                await _context.SaveChangesAsync();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        #endregion

        #region Private Methods
        public OrderStatus MapDescriptionToOrderStatus(string description)
        {
            foreach (OrderStatus status in Enum.GetValues(typeof(OrderStatus)))
            {
                if (status.GetDescription().Equals(description, StringComparison.OrdinalIgnoreCase))
                {
                    return status;
                }
            }

            return OrderStatus.InProcess;
        }
       
        #endregion
    }
}
