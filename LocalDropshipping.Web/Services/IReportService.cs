﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Models;

namespace LocalDropshipping.Web.Services
{
    public interface IReportService
    {
        PageResponse<List<ReportViewModel>> GetReportOfSeller(User user,Pagination pagination);
        PageResponse<List<ReportViewModel>> GetAllReports(Pagination pagination);
        TransactionDetailViewModel GetTransactionDetail(int withdrawalId, User user);
    }
}
