﻿using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Services
{
    public interface IProductsService
    {
        Product Add(Product product, User currentUser);
        List<Product> GetAll();
        Product? GetById(int productId);
        Product Delete(int productId, User currentUser);
        Product? Update(int productId, Product product, User currentUser, bool isSimpleProduct = true);
        List<Product> GetProductsByPriceRange(decimal minPrice, decimal maxPrice);
        List<Product> GetProductsBySearch(string searchString);
        IQueryable<Product> GetBestSelleingProducts();
        IQueryable<Product> GetNewArrivalProducts();
        IQueryable<Product> GetTopRatedProducts();
    }
}