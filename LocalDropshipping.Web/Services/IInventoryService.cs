﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Models.Inventory;

namespace LocalDropshipping.Web.Services
{
    public interface IInventoryService
    {
        Task<bool> AddAssignInventory(AddInventoryAssignModel assignModel);
        Task<bool> AddInventoryHistory(InventoryHistory historyModel);
        Task<InventoryAssign> GetInventoryAssignById(int Id);
        Task<List<InventoryHistory>> GetInventoryHistoryByAssignId(int Id);
        Task<List<InventoryAssign>> GetByUserId(string UserId);
        Task<List<InventoryHistory>> GetInventoryHistoryByUserId(string UserId);
        Task<List<ViewMyProductModel>> GetInventoryListByUserId(string UserId);
        Task IncreaseQuantity(string userId,int productIdint,int quantity);
        Task DecreaseQuantity(string userId, int productId, int quantity);
    }
}
