﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Dtos;
using LocalDropshipping.Web.Helpers;
using LocalDropshipping.Web.Models;

namespace LocalDropshipping.Web.Services
{
    public interface IOrderService
    {
        Task<Order?> AddOrder(List<OrderItem> cart, string email, decimal sellPrice, decimal GrandTotal);
        Order? Add(Order order);
        List<Order?> GetAll();
        Order? GetById(int orderId);
        List<Order> GetByEmail(string sellerEmail);
        Order? Delete(int orderId);
        Order? Update(int orderid, OrderDto order);
        Task<Order> UpdateOrder(OrderViewModel orderViewModel);
        string[] GetCourierOrderIdsByEmail(string email); 
        Task<SellerOrdersViewModel>? GetOrdersProfit(string email);
        List<TransactionDetailItemViewModel> GetOrderByWithdraalId(int withdrawalId);
        List<Order> GetOrderByDateRange(DateTime start, DateTime end);
        Task<List<Order>> GetCourierOrderIdsWithStatusByEmail(string email);
        SellerViewOrder GetFilteredOrders(Pagination pagination,string email);
        OrderListModel GetListOfOrderWithConsumer();
        SellerViewOrder GetOrders(string email);
        Task<int> GetAllDeliveredOrders();
        Task<int> GetAllOrdersInTransit();
        Task<int> GetAllReturnedOrders();
        Task<decimal> GetAllGrandTotal();
        Task<int> GetAllCancelledOrders();
        Task<List<Order>> GetUnpaidAndProcessedOrder();
    }
}
