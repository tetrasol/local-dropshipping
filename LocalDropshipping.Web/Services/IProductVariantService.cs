﻿
using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Services
{
    public interface IProductVariantService
    {
        Task<ProductVariant> Add(ProductVariant productVariant, User currentUser);
        List<ProductVariant> GetAll();
        Task<ProductVariant?> GetByIdAsync(int productVariantId);
        Task<ProductVariant> Delete(int productVariantId, User currentUser);
        Task<ProductVariant?> Update(int productVariantId, ProductVariant productVariant, User currentUser);
        ProductVariant GetByProductIdAsync(int ProductId);
    }
}
