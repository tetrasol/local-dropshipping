﻿using LocalDropshipping.Web.Data;
using LocalDropshipping.Web.Data.Entities;
using Microsoft.EntityFrameworkCore;


namespace LocalDropshipping.Web.Services
{
	public class WishListService : IWishListService
	{
		private readonly LocalDropshippingContext _context;

		public WishListService(LocalDropshippingContext context) 
		{
			_context = context;
		}

		public List<WishList> GetAll()
		{
			return _context.WishList.Where(x => x.IsActive == true).ToList();
		}

        public List<WishList> GetAllbyUserId(string UserId)
        {
            return _context.WishList.Where(x => x.IsActive == true && x.UserId==UserId).ToList();
        }
        public bool ValidateWishlistProduct(string userId, int productId)
        {
            bool isProductInWishlist = _context.WishList.Any(x => x.ProductId == productId && x.UserId == userId);
            if (isProductInWishlist)
            {
                return false;
            }

            return true;
        }
        public WishList Add(string UserId, int ProductId)
		{
			var wishList = new WishList
			{
				UserId = UserId,
				ProductId = ProductId,
				CreatedDate = DateTime.Now,
				IsActive = true
			};
			_context.WishList.Add(wishList);
			_context.SaveChanges();
			return wishList;
		}

		public WishList? GetById(int WishListId)
		{
			return _context.WishList.FirstOrDefault(c => c.WishListId == WishListId && c.IsActive );
		}

		public WishList? Delete(string UserId, int ProductId)
		{
			try
			{
				var Wish = _context.WishList.FirstOrDefault(c => c.UserId==UserId && c.ProductId == ProductId);
				if (Wish != null)
				{
                    _context.WishList.Remove(Wish);
                    _context.SaveChanges();
					return Wish;
				}
			}
			catch (Exception ex)
			{
			}
			return null;

		}
	}
}
