﻿using Humanizer;

namespace LocalDropshipping.Web.Extensions
{
    public static class IFormFileExtenstions
    {
        public static string SaveTo(this IFormFile file, string path, string filename)
        {
            filename = Guid.NewGuid().ToString();
            string extension = Path.GetExtension(file.FileName);
            
            string uniqueFileName;

            if (path.Contains("images/products", StringComparison.OrdinalIgnoreCase))
            {
                uniqueFileName = $"ProductImage_{filename}{extension}";
            }
            else 
            {
                uniqueFileName = $"ProductVideo_{filename}{extension}";
            }

            string uploads = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", path);

            using (FileStream fs = new FileStream(Path.Combine(uploads, uniqueFileName), FileMode.Create))
            {
                file.CopyTo(fs);
            }

            return "/" + path + "/" + uniqueFileName;
        }

        public static List<string> SaveTo(this IFormFile[] files, string path, string filename)
        {
            List<string> links = new List<string>();

            foreach (var file in files)
            {
                links.Add(file.SaveTo(path, filename));
            }

            return links;
        }

    }
}
