﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Enums;
using LocalDropshipping.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace LocalDropshipping.Web.Middlewares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
            
        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext, UserManager<User> userManager)
        {
            var user = await userManager.GetUserAsync(httpContext.User);
            httpContext.Items["CurrentUser"] = user;
            
            if(user != null)
            {
                List<Roles> currentUserRoles = new List<Roles>();
                if (user.IsSuperAdmin)
                {
                    currentUserRoles.Add(Roles.SuperAdmin);
                }
                if (user.IsAdmin)
                {
                    currentUserRoles.Add(Roles.Admin);
                }
                if (user.IsSeller)
                {
                    currentUserRoles.Add(Roles.Seller);
                }
                httpContext.Items.Add("CurrentUserRoles", currentUserRoles);

            }

            await _next(httpContext);
        }
    }
}
