﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Enums;
using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models
{
    public class QuotationListModel
    {
        public int QuotationId { get; set; }
        [Required(ErrorMessage = "Please enter your Full Name")]
        public string Fullname { get; set; }
        [Required(ErrorMessage = "Please enter your email address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Whatsapp phone number is required")]
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string WhatsappPhoneNumber { get; set; }
        [Required(ErrorMessage = "Please enter your Home / City")]
        public string HomeCity { get; set; }
        [Required(ErrorMessage = "Please enter your product information")]
        public string ProductInformation { get; set; }
        [Required(ErrorMessage = "Please enter your budget.")]
        public decimal BudgetInUSD { get; set; }
        [Required(ErrorMessage = "Please enter your website URL")]
        public string WebsiteURL { get; set; }
        public bool OWN { get; set; }
        public bool FOC { get; set; }
        public bool Local { get; set; }
        public bool China { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public List<QuotationsProductImages> ProductImages { get; set; }
        public QuotationStatus QuotationStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SlipImage { get; set; }
        public int Quantity { get; set; }
        public string Role { get; set; }
    }
}
