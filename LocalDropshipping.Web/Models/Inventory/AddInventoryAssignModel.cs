﻿using LocalDropshipping.Web.Enums;

namespace LocalDropshipping.Web.Models.Inventory
{
    public class AddInventoryAssignModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int VariantId { get; set; }
        public string UserId { get; set; }
        public string ProductName { get; set; }
        public string StoreName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AssignBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Comments { get; set; }
        public Sourcing Sourcing { get; set; }
    }
}
