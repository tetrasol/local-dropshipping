﻿using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models
{
    public class FocSettingViewModel
    {
        public string name { get; set; } = "news";
        public string value { get; set; }
        public string description { get; set; } = "header top value";

        //public string News { get; set; }
        //public string PaymentLimit { get; set; }
        //public string ShippingCost { get; set; }
    }
}
