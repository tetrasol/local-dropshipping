﻿using LocalDropshipping.Web.Enums;

namespace LocalDropshipping.Web.Models
{
    public class SellerOrdersViewModel
    {
        public decimal AmountWithdrawal { get; set; }
        public decimal UnpaidEarnings { get; set; }
        public decimal TotalSales { get; set; }
        public decimal AvailableBalance { get; set; }
        public decimal MyWallet { get; set; }
        public int TotalReturnedOrders { get; set; }
        public int TotalDeliveredOrders { get; set; }
        public int TotalOrders { get; set; }
        public int PendingOrders { get; set; }
        public int CancelledOrders { get; set; }
    }
}
