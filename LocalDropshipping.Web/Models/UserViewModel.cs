﻿using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models
{
    public class UserViewModel
    {
        [Required]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        [RegularExpression(@"^03\d{9}$",ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        // Properties for checkboxes (IsSeller, IsStaffMember)
        [Display(Name = "Admin")]
        public bool IsAdmin { get; set; }
        [Display(Name ="Seller")]
        public bool IsSeller { get; set; }
    }
}
