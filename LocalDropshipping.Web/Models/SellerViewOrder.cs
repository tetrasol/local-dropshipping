﻿using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Models
{
    public class SellerViewOrder
    {
        public List<Order> Orders { get; set; }
        public List<Consumer> Consumers { get; set; }
        public List<OrderItem> OrderItems { get; set; }
    }
}
