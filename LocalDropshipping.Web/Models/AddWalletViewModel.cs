﻿namespace LocalDropshipping.Web.Models
{
    public class AddWalletViewModel
    {
        public decimal Amount { get; set; }
        public string Email { get; set; }
    }
}
