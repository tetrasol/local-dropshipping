﻿using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Models
{
    public class ReportViewModel
    {
       public List<Withdrawals> Withdrawals { get; set; }
       public Profiles? Profile { get; set; }
       public List<Profiles> Profiles { get; set; }
    }
}
