﻿using LocalDropshipping.Web.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models
{
    public class CheckoutViewModel
    {
        public List<OrderItem> Cart { get; set; }

        [Required(ErrorMessage ="Selling price is required")]
        //[Range(0, int.MaxValue)]
        public string SellingPrice { get; set; }
        [Required(ErrorMessage ="Full name is required")]
        public string? FullName { get; set; }

        [Required(ErrorMessage = "Primary phone number is required")]
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string? PrimaryPhoneNumber { get; set; }
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string SecondaryPhoneNumber { get; set; }

        [Required(ErrorMessage ="Address is required")]
        public string? Address { get; set; }

        [Required(ErrorMessage ="City is required")]
        public string? City { get; set; }
    }
}
