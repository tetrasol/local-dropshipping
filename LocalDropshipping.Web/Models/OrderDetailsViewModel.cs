﻿using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Models
{
    public class OrderDetailsViewModel
    {
        public Order Order { get; set; }
        public Profiles Profile { get; set; }
        public Consumer Consumer { get; set; }
    }
}
