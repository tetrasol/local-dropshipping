﻿using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Models
{
    public class OrderListModel
    {
        public List<Order> Orders { get; set; }
        public List<Consumer> Consumers { get; set; }
        public int PageNumber { get; set; }
    }
}
