﻿namespace LocalDropshipping.Web.Models
{
    public class CourierAuthentication
    {
       public LeopardAuthenticationSettings Leopard { get; set; }
       public TraxAuthenticationSettings Trax { get; set; }
       public DaewooAuthenticationSettings Daewoo { get; set; }
       public RiderAuthenticationSettings Rider { get; set; }
       public class LeopardAuthenticationSettings
       {
          public string Url { get; set; }
          public string ApiKey { get; set; }
          public string ApiPassword { get; set; }
       }

       public class TraxAuthenticationSettings
       {
          public string Url { get; set; }
          public string ApiKey { get; set; }
          public string ApiPassword { get; set; }
       }
       public class DaewooAuthenticationSettings
       {
          public string Url { get; set; }
          public string UserId { get; set; }
          public string ApiKey { get; set; }
          public string ApiPassword { get; set; }
       }
       public class RiderAuthenticationSettings
       {
          public string Url { get; set; }
          public string LoginId { get; set; }
          public string ApiKey { get; set; }
       }
    }
}
