﻿using LocalDropshipping.Web.Enums;

namespace LocalDropshipping.Web.Models
{
    public class TransactionDetailViewModel
    {
        public string? TransactionId { get; set; }
        public int? OrderId { get; set; }
        public decimal? Price { get; set; }
        public decimal? SellPrice { get; set; }
        public decimal? Profit { get; set; }
        public string? Seller { get; set; }
        public string? BusinessName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string? BankName { get; set; }
        public string? AccountTitle { get; set; }
        public string? AccountNumber { get; set; }
        public PaymentStatus? PaymentStatus { get; set; }
        public decimal? NetPayable { get; set; }
        public List<TransactionDetailItemViewModel> TransactionDetails { get; set; } 
    }
    public class TransactionDetailItemViewModel
    {
        public string? TransactionId { get; set; }
        public int? OrderId { get; set; }
        public decimal? Price { get; set; }
        public decimal? SellPrice { get; set; }
        public decimal? Profit { get; set; }
        public PaymentStatus? PaymentStatus { get; set; }
        public string ProductName { get; set; }
    }

}
