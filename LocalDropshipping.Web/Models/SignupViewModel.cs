﻿using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models
{
    public class SignupViewModel
    {
        [Required]
        [Display(Name = "Full Name")]
        [StringLength(100)]
        public string  FullName { get; set; }

        [Required]
        [Display(Name = "Email address")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [MaxLength(50)]
        //[DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }
}
