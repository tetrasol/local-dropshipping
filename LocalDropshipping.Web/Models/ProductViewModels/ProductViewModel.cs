﻿using LocalDropshipping.Web.Data.Entities;
using LocalDropshipping.Web.Enums;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.CodeAnalysis;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models.ProductViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
       [Required(ErrorMessage = "Product Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Product Description is required.")]
        public string Description { get; set; }
        public string DescriptionContents { get; set; }

        //[Required(ErrorMessage ="Short Description is required")]
        public string ShortDescription { get; set; }
        public string ShortDescriptionContents { get; set; }

        [Required(ErrorMessage = "Product select one option below....")]
        public bool IsNewArravial { get; set; }

        public bool IsBestSelling { get; set; }

        public bool IsTopRated { get; set; }

        [Required(ErrorMessage = "Product Quantity is required.")]
        public int Quantity { get; set; }

        public int MainVariantId { get; set; }
        [Required(ErrorMessage = "Stock keeping unit is required.")]
        public string SKU { get; set; }


        [DisplayName("Category")]
        public int CategoryId { get; set; }

        [Range(0, int.MaxValue)]
        [Required(ErrorMessage = "Price is required.")]
        public int Price { get; set; }
        [Range(0, int.MaxValue)]
        [Required(ErrorMessage = "Discounted price is required.")]
        public int DiscountedPrice { get; set; }


        [ValidateNever]
        public int VariantCounts { get; set; } = 1;

        [ValidateNever]
        public int HasVariants { get; set; } = 0;

        public List<ProductVariantViewModel> Variants { get; set; }

        public Sourcing? Sourcing { get; set; }
        public decimal? Weight { get; set; }

        public ProductViewModel() { }

        public ProductViewModel(Product? product = null)
        {
            if (product != null)
            {
                var hasVariants = product.Variants.Count > 1;
                if (!hasVariants)
                {
                    Name = product.Name;
                    CategoryId = product.CategoryId;
                    IsBestSelling = product.IsBestSelling;
                    IsTopRated = product.IsTopRated;
                    IsNewArravial = product.IsNewArravial;
                    Description = product.Description;
                    DescriptionContents = product.DescriptionContents;
                    ShortDescription = product.ShortDescription;
                    ShortDescriptionContents = product.ShortDescriptionContents;
                    SKU = product.SKU;
                    Weight = product.Weight;
                    Sourcing = product.Sourcing;
                    Price = product.Variants.First().VariantPrice;
                    DiscountedPrice = product.Variants.First().DiscountedPrice;
                    Quantity = product.Variants.First().Quantity;
                    MainVariantId = product.Variants.First().ProductVariantId;
                    Variants = product.Variants.Select(x => new ProductVariantViewModel
                    {
                        Quantity = x.Quantity,
                        VariantPrice = x.VariantPrice,
                        VariantType = x.VariantType,
                        VariantId = x.ProductVariantId,
                        Variant = x.Variant,
                        DiscountedPrice = x.DiscountedPrice,
                        FeatureImageLink = x.FeatureImageLink,
                        Images = x.Images.Select(x => x.Link).ToList(),
                        Videos = x.Videos.Select(x => x.Link).ToList()
                    }).ToList();
                }
                else
                {
                    HasVariants = 1;
                    VariantCounts = product.Variants.Count;
                    Name = product.Name;
                    CategoryId = product.CategoryId;
                    IsBestSelling = product.IsBestSelling;
                    IsTopRated = product.IsTopRated;
                    IsNewArravial = product.IsNewArravial;
                    Description = product.Description;
                    DescriptionContents = product.DescriptionContents;
                    SKU = product.SKU;
                    Weight = product.Weight;
                    Sourcing = product.Sourcing;
                    Price = product.Variants.First().VariantPrice;
                    DiscountedPrice = product.Variants.First().DiscountedPrice;
                    Variants = new List<ProductVariantViewModel>();
                    Variants.AddRange(
                        product.Variants
                        .Select(x => new ProductVariantViewModel
                        {
                            Quantity = x.Quantity,
                            VariantPrice = x.VariantPrice,
                            VariantType = x.VariantType,
                            VariantId = x.ProductVariantId,
                            Variant = x.Variant,
                            DiscountedPrice = x.DiscountedPrice,
                            FeatureImageLink = x.FeatureImageLink,
                            Images = x.Images.Select(x => x.Link).ToList(),
                            Videos = x.Videos.Select(x => x.Link).ToList()
                        }));
                }

                ProductId = product.ProductId;
            }
        }
    }
}
