﻿using LocalDropshipping.Web.Enums;
using System.ComponentModel.DataAnnotations;

namespace LocalDropshipping.Web.Models.ProductViewModels
{
    public class WholesaleViewModel
    {
        public WholesaleViewModel()
        {
            Fulfilment = "FOC";
            Sourcing = "Local";
        }
        [Required(ErrorMessage ="Please enter your Full Name")]
        public string Fullname { get; set; }
        [Required(ErrorMessage = "Please enter your email address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Whatsapp phone number is required")]
        [RegularExpression(@"^03\d{9}$", ErrorMessage = "Please enter a valid phone number in the format 03XXXXXXXXX.")]
        public string WhatsappPhoneNumber { get; set; }
        [Required(ErrorMessage = "Please enter your Home / City")]
        public string HomeCity { get; set; }
        public string ProductInformation { get; set; }
        [Required(ErrorMessage = "Please enter your budget.")]
        public decimal BudgetInUSD { get; set; }
        public string WebsiteURL { get; set; }
        [Required(ErrorMessage = "This feild is required")]
        public string Sourcing { get; set; } = "Local";
        [Required(ErrorMessage = "This feild is required")]
        public string Fulfilment { get; set; } = "FOC";
        public List<IFormFile> ProductImages { get; set; } 
        [Required(ErrorMessage = "Please upload your slip.")]
        public IFormFile SlipImage { get; set; }
    }
}
