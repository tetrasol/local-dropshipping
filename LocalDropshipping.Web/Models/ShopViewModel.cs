﻿using LocalDropshipping.Web.Data.Entities;

namespace LocalDropshipping.Web.Models
{
    public class ShopViewModel
    {
        public List<ShopProductViewModel> BestSeller { get; set; }
        public List<ShopProductViewModel> NewArrival { get; set; }
        public List<ShopProductViewModel> TopRated { get; set; }
        public List<OrderItem> Cart { get; set; }
    }
}
