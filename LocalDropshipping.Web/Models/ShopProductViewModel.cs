﻿namespace LocalDropshipping.Web.Models
{
    public class ShopProductViewModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal DiscountedPrice { get; set; }
        public decimal VariantPrice { get; set; }
        public string ImageLink { get; set; }
        public int VariantId { get; set; }
        public bool HasVariants { get; set; }
    }
}
